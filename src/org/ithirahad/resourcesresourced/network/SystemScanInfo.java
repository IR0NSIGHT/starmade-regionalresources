package org.ithirahad.resourcesresourced.network;

import org.apache.commons.lang3.text.WordUtils;
import org.ithirahad.resourcesresourced.RRUtils.SerializableVector3i;
import org.ithirahad.resourcesresourced.RRUtils.SpaceGridMap;
import org.ithirahad.resourcesresourced.industry.PassiveResourceSupplier;
import org.ithirahad.resourcesresourced.universe.galaxy.ZoneClass;
import org.ithirahad.resourcesresourced.universe.gasplanet.GasGiantSheet;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemClass;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemSheet;
import api.common.GameClient;
import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.ithirahad.resourcesresourced.universe.terrestrial.TerrestrialSheet;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.ithirahad.resourcesresourced.RRSConfiguration.ASTEROID_PLANET_FIRELINE;
import static org.ithirahad.resourcesresourced.RRSConfiguration.ASTEROID_PLANET_FROSTLINE;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.*;
import static org.ithirahad.resourcesresourced.industry.ResourceSourceType.GROUND;
import static org.ithirahad.resourcesresourced.listeners.BlockRemovalLogic.getStarResourceSector;
import static org.schema.game.server.data.Galaxy.*;

public class SystemScanInfo extends Packet {
    public SerializableVector3i sector;
    public SerializableVector3i system;
    public String zoneName;
    public ZoneClass zoneType;
    public SystemClass systemType;
    public String scanOverview;
    public boolean hasSecResourceInfo;
    public String sectorResourceList = "";
    public boolean hasRemoteCelestialInfo;
    public List<String> celestialEntries = new ArrayList<>();

    /**
    This packet is created on the server and sent to clients. It contains information about a system that they scan via Astrometric Scanner.
     */

    public SystemScanInfo(){}

    public SystemScanInfo(Vector3i system, Vector3i sector, boolean giveSecRscInfo, boolean giveRemotePlanetInfo) {
        //TODO: System Scanner Console that gives status on R-mouseover

        if((GameServerState.instance != null)){
            System.err.println("[MOD][Resources ReSourced] Creating new System Scan Info for sector " + sector + " in system " + system);
            hasSecResourceInfo = giveSecRscInfo;
            hasRemoteCelestialInfo = giveRemotePlanetInfo;
            ArrayList<String> celestialInfos = new ArrayList<>();
            SystemSheet thisSystem = getSystemSheet(system);
            zoneName = thisSystem.zone.name;
            zoneType = thisSystem.zone.zoneClass;
            systemType = thisSystem.systemClass;
            Galaxy galaxy = thisSystem.zone.galaxy;
            Vector3i sadj = new Vector3i(system);
            sadj.add(Galaxy.halfSize,Galaxy.halfSize,Galaxy.halfSize);
            String systemName = galaxy.getName(sadj);
            this.sector = new SerializableVector3i(sector);
            this.system = new SerializableVector3i(system);

            StringBuilder overview = new StringBuilder();
            overview.append("You are currently located in " + systemName + ", a " + thisSystem.systemClass.descriptor() + " system. \r\nThis system is within the " + zoneType.getDescriptor() + " zone known as the " + zoneName + ". \r\n");
            overview.append("Based on deep-space scans, the Traders have assigned a resource density score of " + Math.round(thisSystem.resourceDensity * 100) + " (out of 100) to this star system.\r\n\r\n");
            overview.append(thisSystem.zone.zoneClass.description() + "\r\n\r\n");
            overview.append(thisSystem.systemClass.description());
            TerrestrialSheet terrestrialSheet = thisSystem.getTerrestrial(sector);
            if(terrestrialSheet != null){
                overview.append("\r\nThe dominant celestial body in this sector");
                //TODO: if(!planetName.equals("")) overview.append(", " + planetName + ", ");
                overview.append(" is some sort of ");
                overview.append(terrestrialSheet.materials.description + ".");
            }

            SpaceGridMap<GasGiantSheet> giants = thisSystem.getGiants(system); //...or is it sadj
            GasGiantSheet gasGiantSheet = giants.get(sector);

            if(gasGiantSheet != null){
                overview.append("\r\nThe "+ gasGiantSheet.type.typeName() + " in this sector is " + gasGiantSheet.type.description());
            }
            scanOverview = overview.toString();

            if(giveSecRscInfo){
                List<PassiveResourceSupplier[]> resources = new ArrayList<>();
                resources.add(container.getAllAvailableSourcesInArea(sector,system));
                StringBuilder message = new StringBuilder();
                int i = 0;
                message.append("The following passively-harvested resources are available in your sector ").append(sector.toString()).append(": ");
                for(PassiveResourceSupplier[] wells : resources) for(PassiveResourceSupplier well : wells){
                    float currentExtractionPower = well.getTotalExPower();
                    int harvesterCount = well.getExtractorCount();
                    message.append("\r\n   ---").append(well.type.name()).append(" - ").append(ElementKeyMap.getInfo(well.resourceTypeId).name).append(" (Regeneration Per Second: ").append(well.regenRate).append("; ").append(Math.min(well.regenRate,currentExtractionPower)).append(" Currently Siphoned Per Second across ").append(harvesterCount).append(harvesterCount == 1? "extractor)" : " extractors)");
                    message.append("\r\n      Harvest this resource using a ").append(well.type == GROUND ? "Magmatic Extractor placed on the planet" : "Vapour Siphon").append(".");
                    i++;
                }
                if(i==0) message.append("\r\nNone.");
                sectorResourceList = message.toString();
            }

            if(giveRemotePlanetInfo) {
                StringBuilder information = new StringBuilder("- ");

                if(galaxy.isVoidAbs(system))information.append("No celestial bodies detected.");
                else {
                    //Star
                    String starType = "";
                    //TODO: find a way to get the colour by raw index
                    switch(galaxy.getSystemType(system)) { //or is it sadj?
                        case TYPE_SUN:
                            starType += "STAR";
                            break;
                        case TYPE_GIANT:
                            starType += "GIANT STAR";
                            break;
                        case TYPE_BLACK_HOLE:
                            starType += "WORMHOLE";
                            break;
                        case TYPE_DOUBLE_STAR:
                            starType += "DOUBLE STAR";
                            break;
                    }

                    information.append(starType);
                    information.append(", centered around sector " + getStarResourceSector(system));
                    PassiveResourceSupplier[] starRsc = container.getPassiveSources(getStarResourceSector(system));
                    if (starRsc.length != 0) {
                        information.append("\r\n Contains the following harvestable resources (system-wide): ");
                        for (PassiveResourceSupplier well : starRsc) {
                            float currentExtractionPower = well.getTotalExPower();
                            information.append("\r\n   ---").append(ElementKeyMap.getInfo(well.resourceTypeId).name).append(" (Regeneration Per Second: ").append(well.regenRate).append("; ").append(currentExtractionPower).append(" Total Siphoning Power Applied Per Cycle").append(")");
                        }
                    }
                    information.append("\r\n");
                    celestialInfos.add(information.toString());

                    //Asteroid Belts
                    int[] orbits = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
                    Vector3i locFromCorner = new Vector3i(system);
                    locFromCorner.add(halfSize, halfSize, halfSize);//corner-origin coordinates, the eternal source of galactic confusion
                    galaxy.getSystemOrbits(locFromCorner, orbits);
                    for (int i = 0; i < orbits.length; i++) {
                        int orbit = orbits[i];
                        if (orbit > 0 && !Galaxy.isPlanetOrbit(orbit)) {
                            //it's an asteroid belt! now what's in it?
                            information = new StringBuilder("- ASTEROID BELT (Radial " + i + "):");
                            information.append("\r\n Contains ");
                            float temperature = 1.0f - Math.min(1.0f, (i + 0.5f) / (float) orbits.length);
                            if (temperature < ASTEROID_PLANET_FROSTLINE) {
                                if (systemType == SystemClass.RR_FRIGID)
                                    information.append("Aegium comets, along with common metal/crystal asteroid types.");
                                else if (systemType == SystemClass.RR_CRYSTAL_RICH)
                                    information.append("Dense Crystalline Asteroids as well as common metal/crystal asteroid types.");
                                else if (systemType == SystemClass.RR_METAL_RICH)
                                    information.append("Dense Metallic Asteroids as well as common metal/crystal asteroid types.");
                                else information.append("mostly common metal/crystal asteroid types.");
                            } else if (temperature < ASTEROID_PLANET_FIRELINE) {
                                if (systemType == SystemClass.RR_CRYSTAL_RICH)
                                    information.append("Dense Crystalline Asteroids as well as common metal/crystal asteroid types.");
                                else if (systemType == SystemClass.RR_METAL_RICH)
                                    information.append("Dense Metallic Asteroids as well as common metal/crystal asteroid types.");
                                else information.append("mostly common metal/crystal asteroid types.");
                            } else { //within fireline
                                if (systemType == SystemClass.RR_FERRON)
                                    information.append("Ferron Asteroids, along with common metal/crystal asteroid types.");
                                else if (systemType == SystemClass.RR_CRYSTAL_RICH)
                                    information.append("Dense Crystalline Asteroids as well as common metal/crystal asteroid types.");
                                else if (systemType == SystemClass.RR_METAL_RICH)
                                    information.append("Dense Metallic Asteroids as well as common metal/crystal asteroid types.");
                                else information.append("mostly common metal/crystal asteroid types..");
                                information.append(" (WARNING: Solar Heat Hazard!)");
                            }
                            information.append("\r\n");
                            celestialInfos.add(information.toString());
                        }
                    }

                    //Gas Giants
                    if (thisSystem.getGiants(system).values().size() > 0) {
                        for (Vector3i loc : thisSystem.getGiants(system).keySet()) {
                            information = new StringBuilder("- GAS PLANET: ");
                            GasGiantSheet giant = thisSystem.getGiants(system).get(loc);
                            //TODO: message.append(giant.name + ": ");
                            information.append(WordUtils.capitalize(giant.type.typeName()) + " in sector " + loc);
                            PassiveResourceSupplier[] rsc = container.getPassiveSources(loc);
                            if (rsc.length != 0) {
                                information.append("\r\n Contains the following harvestable resources: ");
                                for (PassiveResourceSupplier well : rsc) {
                                    information.append("\r\n   ---").append(ElementKeyMap.getInfo(well.resourceTypeId).name).append(" (Regeneration Per Second: ").append(well.regenRate).append(")");
                                }
                            }
                            information.append("\r\n");
                            celestialInfos.add(information.toString());
                        }
                    }

                    //Planets
                    if (thisSystem.getTerrestrials(system).values().size() > 0) {
                        for (Vector3i loc : thisSystem.getTerrestrials(system).keySet()) {
                            information = new StringBuilder("- TERRESTRIAL PLANET: ");
                            TerrestrialSheet planet = thisSystem.getTerrestrial(loc);
                            //TODO: message.append(planet.name + ": ");
                            information.append(WordUtils.capitalize(planet.planetType.descName.toLowerCase()) + " in sector " + loc);
                            PassiveResourceSupplier[] rsc = container.getPassiveSources(loc);
                            if (rsc.length != 0) {
                                information.append("\r\n Contains the following harvestable resources: ");
                                for (PassiveResourceSupplier well : rsc) {
                                    information.append("\r\n   ---").append(" - ").append(ElementKeyMap.getInfo(well.resourceTypeId).name).append(" (Regeneration Per Second: ").append(well.regenRate).append(")");
                                }
                            }
                            information.append("\r\n");
                            celestialInfos.add(information.toString());
                        }
                    }
                }
            }

            //TODO: Send the raw list of data or 'datasheet' reduced transport objects. Port all of the UI logic to UI.
            //[...]
            celestialEntries = celestialInfos;
        }
        else{
            GameClient.showPopupMessage("[MOD][RRS] ERROR: Remote client attempted to create server SystemScanInfo packet!", 0);
        }
    }

    @Override
    public void readPacketData(PacketReadBuffer buf) throws IOException {
        System.err.println("[MOD][Resources ReSourced][CLIENT] Received scanner info packet.");
        sector = buf.readObject(SerializableVector3i.class);
        system = buf.readObject(SerializableVector3i.class);
        hasRemoteCelestialInfo = buf.readBoolean();
        hasSecResourceInfo = buf.readBoolean();
        zoneName = buf.readString();
        scanOverview = buf.readString();
        sectorResourceList = buf.readString();
        zoneType = ZoneClass.values()[buf.readInt()];
        systemType = SystemClass.values()[buf.readInt()];
        celestialEntries = buf.readStringList();
    }

    @Override
    public void writePacketData(PacketWriteBuffer buf) throws IOException {
        buf.writeObject(sector);
        buf.writeObject(system);
        buf.writeBoolean(hasRemoteCelestialInfo);
        buf.writeBoolean(hasSecResourceInfo);
        buf.writeString(zoneName);
        buf.writeString(scanOverview);
        buf.writeString(sectorResourceList);
        buf.writeInt(zoneType.ordinal());
        buf.writeInt(systemType.ordinal());
        buf.writeStringList(celestialEntries);
    }

    @Override
    public void processPacketOnClient() {
        astroScanControlManager.setActive(false);
        astroScanControlManager.setInfo(this);
        astroScanControlManager.setActive(true);
        if(astroScanControlManager.currentPanel != null) astroScanControlManager.currentPanel.updateText();
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) { }
}
