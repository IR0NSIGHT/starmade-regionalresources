package org.ithirahad.resourcesresourced.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.ithirahad.resourcesresourced.vfx.ScannerPulseEffect;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.io.IOException;

public class ScannerFXRemoteExecuteTemporaryPacket extends Packet {
    int sectorId;
    Vector3i sector;
    Vector3f position;
    Vector3f velocity;
    Vector4f color;
    int diameterMultiplier;

    public ScannerFXRemoteExecuteTemporaryPacket(){
        super();
    }

    public ScannerFXRemoteExecuteTemporaryPacket(int sectorID, Vector3i loc, Vector3f pos, Vector3f vel, int diameterFactor, Vector4f effectColor){
        sectorId = sectorID;
        sector = loc;
        position = pos;
        velocity = vel;
        color = effectColor;
        diameterMultiplier = diameterFactor;
    }

    @Override
    public void readPacketData(PacketReadBuffer b) throws IOException {
        sectorId = b.readInt();
        sector = b.readVector();
        position = b.readVector3f();
        velocity = b.readVector3f();
        diameterMultiplier = b.readInt();
        color = b.readVector4f();
    }

    @Override
    public void writePacketData(PacketWriteBuffer b) throws IOException {
        b.writeInt(sectorId);
        b.writeVector(sector);
        b.writeVector3f(position);
        b.writeVector3f(velocity);
        b.writeInt(diameterMultiplier);
        b.writeVector4f(color);
    }

    @Override
    public void processPacketOnClient() {
        ScannerPulseEffect.FireEffectClient(sectorId, sector, position, velocity, diameterMultiplier, color);
    }

    @Override
    public void processPacketOnServer(PlayerState playerState) {
        //why
    }
}
