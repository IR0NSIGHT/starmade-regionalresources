package org.ithirahad.resourcesresourced.network;

import api.network.Packet;
import api.network.PacketReadBuffer;
import api.network.PacketWriteBuffer;
import org.schema.game.common.data.player.PlayerState;

import java.io.IOException;

public class ClientGalaxyLayoutRequest extends Packet {
    @Override
    public void readPacketData(PacketReadBuffer packetReadBuffer) throws IOException {
        //
    }

    @Override
    public void writePacketData(PacketWriteBuffer packetWriteBuffer) throws IOException {
        //TODO: encode any relevant galaxy layout and name info, anonymizing anything the client should not have e.g. concentration map
    }

    @Override
    public void processPacketOnClient() { }

    @Override
    public void processPacketOnServer(PlayerState playerState) {

    }
}
