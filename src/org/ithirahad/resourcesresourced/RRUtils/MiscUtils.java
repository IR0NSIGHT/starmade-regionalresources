package org.ithirahad.resourcesresourced.RRUtils;

import org.lwjgl.util.vector.Quaternion;
import org.schema.game.client.data.GameClientState;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.objects.Sendable;

import javax.annotation.Nullable;
import javax.vecmath.Matrix3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class MiscUtils {

    public static void setPrivateField(String fieldName, Object targetInstance, Object newVal) {
        try {
            Class<?> cls = targetInstance.getClass();
            Field field = cls.getField(fieldName);
            field.setAccessible(true);
            field.set(targetInstance,newVal);
            field.setAccessible(false);
        } catch (NoSuchFieldException e) {
            System.err.println("[MOD][Resources ReSourced] ERROR: Provided field name \"" + targetInstance.getClass().getName() + "." + fieldName + "\" does not correspond to any extant field in the target class.");
            System.err.println("[MOD][Resources ReSourced] ISSUE CODE GRANT AETHER"); //just a quick thing to search in logs idk lol
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            System.err.println("[MOD][Resources ReSourced] ERROR: Could not access target field \"" + targetInstance.getClass().getName() + "." + fieldName + "\".");
            System.err.println("[MOD][Resources ReSourced] ISSUE CODE GRANT COULOMB"); //just a quick thing to search in logs idk lol
            e.printStackTrace();
        }
    }

    public static void setSuperclassPrivateField(Class cls, String fieldName, Object targetInstance, Object newVal) { //EdenCore compatibility measure, might be useful elsewhere
        try {
            Field field = cls.getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(targetInstance,newVal);
            field.setAccessible(false);
        } catch (NoSuchFieldException e) {
            System.err.println("[MOD][Resources ReSourced] ERROR: Provided field name \"" + targetInstance.getClass().getName() + "." + fieldName + "\" does not correspond to any extant field in the target class.");
            System.err.println("[MOD][Resources ReSourced] ISSUE CODE GRANT AETHER"); //just a quick thing to search in logs idk lol
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            System.err.println("[MOD][Resources ReSourced] ERROR: Could not access target field \"" + targetInstance.getClass().getName() + "." + fieldName + "\".");
            System.err.println("[MOD][Resources ReSourced] ISSUE CODE GRANT COULOMB"); //just a quick thing to search in logs idk lol
            e.printStackTrace();
        }
    }

    private static boolean containsField(Field[] declaredFields, String fieldName) {
        for(Field field : declaredFields) if(field.getName() == fieldName) return true;
        return false;
    }

    public static Object readPrivateField(String fieldName, Object targetInstance){
        try {
            Class<?> cls = targetInstance.getClass();
            Field field = cls.getDeclaredField(fieldName);
            field.setAccessible(true);
            Object rtn = field.get(targetInstance);
            field.setAccessible(false);
            return rtn;
        } catch (IllegalAccessException e) {
            System.err.println("[MOD][Resources ReSourced] ERROR: Could not access target field \"" + targetInstance.getClass().getName() + "." + fieldName + "\".");
            System.err.println("[MOD][Resources ReSourced] ISSUE CODE ENIGMA COULOMB"); //just a quick thing to search in logs idk lol
            e.printStackTrace();
            return null;
        } catch (NoSuchFieldException e) {
            System.err.println("[MOD][Resources ReSourced] ERROR: Provided field name \"" + targetInstance.getClass().getName() + "." + fieldName + "\" does not correspond to any extant field in the target class.");
            System.err.println("[MOD][Resources ReSourced] ISSUE CODE ENIGMA AETHER"); //just a quick thing to search in logs idk lol
            e.printStackTrace();
            return null;
        }
    }

    public static Object readSuperclassPrivateField(Class cls, String fieldName, Object targetInstance) {
        try {
            Field field = cls.getDeclaredField(fieldName);
            field.setAccessible(true);
            Object rtn = field.get(targetInstance);
            field.setAccessible(false);
            return rtn;
        } catch (IllegalAccessException e) {
            System.err.println("[MOD][Resources ReSourced] ERROR: Could not access target field \"" + targetInstance.getClass().getName() + "." + fieldName + "\".");
            System.err.println("[MOD][Resources ReSourced] ISSUE CODE ENIGMA COULOMB"); //just a quick thing to search in logs idk lol
            e.printStackTrace();
            return null;
        } catch (NoSuchFieldException e) {
            System.err.println("[MOD][Resources ReSourced] ERROR: Provided field name \"" + targetInstance.getClass().getName() + "." + fieldName + "\" does not correspond to any extant field in the target's superclasses.");
            System.err.println("[MOD][Resources ReSourced] ISSUE CODE ENIGMA AETHER"); //just a quick thing to search in logs idk lol
            e.printStackTrace();
            return null;
        }
    }

    public static final Vector3f forward = new Vector3f(0,0,-1);
    public static final Vector3f up = new Vector3f(0,1,0);

    public static Vector3f multiply(Vector3f vec, Matrix3f matrix) {
        Vector3f result = new Vector3f();
        Vector3f row = new Vector3f();
        matrix.getRow(1,row);
        result.x = vec.dot(row);
        matrix.getRow(2,row);
        result.y = vec.dot(row);
        matrix.getRow(3,row);
        result.z = vec.dot(row);
        return result;
    }

    public static Quat4f LookAt(Vector3f sourcePoint, Vector3f destPoint, boolean lookAway)
    {
        Vector3f forwardVector = new Vector3f(destPoint);
        forwardVector.sub(sourcePoint);
        if(forwardVector.lengthSquared() != 0) forwardVector.normalize();

        float dot = forward.dot(forwardVector);

        if (Math.abs(dot - (-1.0f)) < 0.000001f)
        {
            return new Quat4f(up.x, up.y, up.z, (float)Math.PI);
        }
        if (Math.abs(dot - (1.0f)) < 0.000001f)
        {
            return new Quat4f(0,0,0,1);
        }

        float rotAngle = (float)Math.acos(dot);
        Vector3f rotAxis = new Vector3f();
        rotAxis.cross(forward,forwardVector);
        if(rotAxis.lengthSquared() != 0) rotAxis.normalize();
        Quaternion qResult = CreateFromAxisAngle(rotAxis, rotAngle);
        Quat4f result = new Quat4f(qResult.x, qResult.y, qResult.z, qResult.w);
        if(lookAway) result.inverse();
        return result;
    }

    public static Quaternion CreateFromAxisAngle(Vector3f axis, float angle)
    {
        float halfAngle = angle * .5f;
        float s = (float)Math.sin(halfAngle);
        Quaternion q = new Quaternion();
        q.x = axis.x * s;
        q.y = axis.y * s;
        q.z = axis.z * s;
        q.w = (float)Math.cos(halfAngle);
        return q;
    }

    @Nullable
    public static Sendable getServerSendable(Sendable sendable) {
        int id = sendable.getId();
        boolean clientExists = GameClientState.instance != null;
        if(clientExists){
            return GameServerState.instance.getLocalAndRemoteObjectContainer().getLocalObjects().get(id);
        }
        return null;
    }

    public static <T extends Sendable> ArrayList<T> getServerSendables(ArrayList<T> sendables){
        ArrayList<T> result = new ArrayList<>();
        for(Sendable s : sendables){
            Sendable serverSendable = getServerSendable(s);
            if(serverSendable != null) result.add((T) serverSendable);
        }
        return result;
    }

    public static float smoothstep(float from, float to, float x) {
        x = clamp((x - from) / (to - from), 0.0f, 1.0f);
        return x * x * (3 - 2 * x);
    }

    public static float lerp(float from, float to, float mix) {
        return lerp(from,to,mix,true);
    }

    /**
     * Linearly interpolates between two values based on the third.
     * @param from The 'start' value
     * @param to The 'destination' value
     * @param mix The balance between the two that we are trying to find. Start is at 0.0; destination is at 1.0.
     * @param boundedAtTermini If true, do not allow overshooting/undershooting the end/start values. (i.e. clamp the mix to 0.0 and 1.0, inclusive)
     * @return
     */
    public static float lerp(float from, float to, float mix, boolean boundedAtTermini) {
        if(boundedAtTermini) mix = clamp(mix,0.0f,1.0f);
        return (from * (1f-mix)) + (to * mix);
    }

    public static Vector3f lerp(Vector3f from, Vector3f to, float mix){
        return new Vector3f(lerp(from.x,to.x,mix),lerp(from.y,to.y,mix),lerp(from.z,to.z,mix));
    }

    public static Vector4f lerp(Vector4f from, Vector4f to, float mix){
        return new Vector4f(lerp(from.x,to.x,mix),lerp(from.y,to.y,mix),lerp(from.z,to.z,mix),lerp(from.w,to.w,mix));
    }

    public static float clamp(float x, float lowerlimit, float upperlimit) {
        if (x < lowerlimit)
            x = lowerlimit;
        if (x > upperlimit)
            x = upperlimit;
        return x;
    }
}
