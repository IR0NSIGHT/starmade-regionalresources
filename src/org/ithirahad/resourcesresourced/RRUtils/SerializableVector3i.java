package org.ithirahad.resourcesresourced.RRUtils;

import org.schema.common.util.linAlg.Vector3i;

import java.io.Serializable;

/**
 * A version of StarMade's native Vector3i which supports serialization.
 */
public class SerializableVector3i extends Vector3i implements Serializable {
    //Literally just what it says on the tin

    public SerializableVector3i(){
        super();
    }

    public SerializableVector3i(int x, int y, int z){
        super(x,y,z);
    }

    public SerializableVector3i(int[] coords3d){
        super(coords3d[0],coords3d[1],coords3d[2]);
    }

    public SerializableVector3i(Vector3i v){
        super(v);
    }

    /**
    Create a SerializableVector3i with all components equal.
     @param cubeDim the value of the vector's components
     */
    public SerializableVector3i(int cubeDim){
        super(cubeDim,cubeDim,cubeDim);
    }
}
