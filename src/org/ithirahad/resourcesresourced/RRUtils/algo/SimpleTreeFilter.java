package org.ithirahad.resourcesresourced.RRUtils.algo;

import org.ithirahad.resourcesresourced.listeners.RRSGalaxyGeneration;
import org.lwjgl.Sys;
import org.schema.common.util.linAlg.Vector3i;

import java.util.Collection;
import java.util.LinkedList;

public class SimpleTreeFilter {
    private SimpleTree tree;
    public SimpleTreeFilter(SimpleTree tree) {
        this.tree = tree;
    }

    protected boolean isUnwanted(SimpleTree t) {
        return false;
    }

    public SimpleTree filterUnwanted() {
        SimpleBFS.Printer p = new SimpleBFS.Printer(){
            @Override
            public String toString(SimpleTree t) {
                return t.getValue()+(isUnwanted(t)?"V":"S") ;
            }
        };

        filterUnwanted(tree); //kills all except root
        if (isUnwanted(tree)) {//root is unwanted
        /*
            LinkedList<SimpleTree> cs = tree.getChildren();
            if (cs.isEmpty())
                return null;
            SimpleTree newRoot = cs.getFirst();
            cs.remove(newRoot);
            newRoot.addAllChildren(cs);
            tree = newRoot;

         */
        }
        return tree;
    }

    /**
     * condense the tree to only house star systems, removes voids.
     */
    private void filterUnwanted(SimpleTree t) {
        LinkedList<SimpleTree> children = new LinkedList<>(t.getChildren());
        t.getChildren().clear();
        LinkedList<SimpleTree> unwanted = new LinkedList<>();
        LinkedList<SimpleTree> grandChildren = new LinkedList<>();
        for (SimpleTree c: children) {
            filterUnwanted(c);
            assert allWanted(c.getChildren()); //invariant: c only has children that are stars.
            if (isUnwanted(c)) {
                //remove child and add grandchildren
                grandChildren.addAll(c.getChildren());
                unwanted.add(c);
            } else {
                //keep child
                t.getChildren().add(c);
            }
        }

        //remove unwanted children. links to grandchildren are stored in list
        for (SimpleTree c: unwanted) {
            children.remove(c);
        }
        //add grandchildren.
        children.addAll(grandChildren);
        assert allWanted(children);
    }

    public boolean allWanted(Collection<SimpleTree> ts) {
        for (SimpleTree t: ts) {
            if (isUnwanted(t))
                return false;
        }
        return true;
    }
}


