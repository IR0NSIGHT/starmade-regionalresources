package org.ithirahad.resourcesresourced.RRUtils.algo;

import java.util.*;

public class RandomTreeCutter {
    private int dataSize;
    private int desiredSize;
    private SimpleTree tree;

    private PriorityQueue<SimpleTree> heap;
    private HashSet<SimpleTree> visited;
    private Random random;

    /**
     * cuts down tree based on random weighting to desired size.
     * @param dataSize
     * @param desiredSize
     * @param tree
     */
    public RandomTreeCutter(int dataSize, int desiredSize, SimpleTree tree) {
        this.dataSize = dataSize;
        this.desiredSize = desiredSize;
        this.tree = tree;
    }

    private void init() {
        random = new Random();
        heap = new PriorityQueue<>(dataSize, new Comparator<SimpleTree>() {
            @Override
            public int compare(SimpleTree o1, SimpleTree o2) { //deterministic but random weight for each treenode
                random.setSeed(o1.getValue().code());
                int f1 = random.nextInt();
                random.setSeed(o2.getValue().code());
                int f2 = random.nextInt();
                return f1-f2;
            }
        });
        visited = new HashSet<>();
    }

    protected Collection<SimpleTree> run() {
        init();
        heap.add(tree);
        while (!isAbort() && !heap.isEmpty()) {
            step(heap.poll());
        }
        return visited;
    }

    private void step(SimpleTree node) {
        visited.add(node);
        //add all children to heap,
        for (SimpleTree t: node.getChildren()) {
            if (!visited.contains(t))
                heap.add(t);
        }
    }

    protected boolean isAbort() {
        return visited.size()>=desiredSize;
    }
}
