package org.ithirahad.resourcesresourced.RRUtils.algo;

public class BinarySearchTree {
    Object object; //object the node carries
    int val; //values that identifies the object. must be unique in tree
    BinarySearchTree leftChild;
    BinarySearchTree rightChild;
    public BinarySearchTree(Object object, int val) {
        this.object = object;
        this.val = val;
    }

    /**
     * will search for value in tree. if found: returns the node carrying the value, else: returns parent where object would be a child of.
     * @param val
     * @return
     */
    public BinarySearchTree find(int val) {
        if (val==this.val) //i am the searched node
           return this;
        else if (val < this.val)
            if (leftChild==null)
                return this;
            else
                return leftChild.find(val);
        else { //val > this.val

            if (rightChild == null)
                return this;
            else
                return leftChild.find(val);
        }
    }
}
