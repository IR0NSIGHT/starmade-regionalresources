package org.ithirahad.resourcesresourced.RRUtils.algo;

import org.schema.common.util.linAlg.Vector3i;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Breadth-first-search. Searches starting from "start" pos, circeling outwards, creating a tree structure of the result. O(n). Overwrite "isValid" and "abort" for custom behaviour.
 * Default: will search inside the given set, creating a tree that has shortest paths from start to all point in data set.
 * use SimpleTreeFilter to filter out unwanted nodes in the tree afterwards.
 */
public class SimpleBFS {
    SimpleTree tree;
    HashSet<Vector3i> seen = new HashSet<>();
    HashSet<Vector3i> data = new HashSet<>();
    LinkedList<SimpleTree> queue = new LinkedList<>();

    public static void main(String[] args) {
        //generate raw data (100^3 cube)
        int range = 5;
        LinkedList<Vector3i> nodes = new LinkedList<>();
        for (int x = -range; x<=range; x++)
            for (int y = -range; y<=range; y++)
                for (int z = -range; z<=range; z++) {
                    Vector3i next = new Vector3i(x,y,z);
                    nodes.add(next);
                }
        long time = System.currentTimeMillis();
        SimpleBFS bfs = new SimpleBFS(new Vector3i(0,0,0), nodes);
        bfs.run();

        long diff = System.currentTimeMillis()-time;
        System.out.println(String.format("BFS took %s seconds",diff/1000));
        print(bfs.tree,"","",5, new Printer());
    }

    /**
     *
     * @param start start of search
     * @param set data to search in
     */
    public SimpleBFS(Vector3i start,@Nullable Collection<Vector3i> set) {
        if (set != null)
            data.addAll(set);
        tree = new SimpleTree(start,null);
    }

    /**
     * run the BFS. returns the last touched datapoint when BFS aborts. if no abort, returns result tree.
     * @return
     */
    public SimpleTree run() {
        addToQueue(tree);
        seen.add(tree.getValue());
        SimpleTree node;
        while (!queue.isEmpty()) {
            node = queue.pop();
            step(node);
            if (isAbort(node.getValue(),seen,tree))
                return node;
        }
        return tree;
    }

    /**
     * test if this point is part of the data the BFS operates in. overwrite for custom behaviour
     * @param point
     * @return
     */
    protected boolean isValid(Vector3i point) {
        return data.contains(point);
    }

    /**
     * abort condition. overwrite for custom behaviour.
     * @param point current point the BFS is looking at
     * @param seen current "seen" list
     * @param tree current tree structure
     * @return default false.
     */
    protected boolean isAbort(Vector3i point, HashSet<Vector3i> seen, SimpleTree tree) {
        return false;
    }

    protected void addToQueue(SimpleTree t) {
       queue.add(t);
    }
    
    protected void step(SimpleTree node) {
        Vector3i next = new Vector3i();
        for (Vector3i o: off) {
            next.set(node.getValue());
            next.add(o);
            if (!seen.contains(next) && isValid(next)) {
                Vector3i val = new Vector3i(next);
                SimpleTree child = new SimpleTree(val,node);

                node.getChildren().add(child);

                addToQueue(child);
                seen.add(child.getValue());
            }
        }
    }

    /**
     * recursively prints the tree into the console.
     * @param t
     * @param prefix
     * @param cPrefix
     * @param cascade how many levels to print, starting at root = 0. use negative value to print all.
     */
    public static void print(SimpleTree t, String prefix, String cPrefix, int cascade, Printer p) {
        Iterator<SimpleTree> it = t.getChildren().iterator();
        System.out.println(prefix +p.toString(t));
        if (cascade == 0)
            return;
        while (it.hasNext()) {
            SimpleTree c = it.next();
            if (it.hasNext())
                print(c, prefix + "---- ", cPrefix + "|   ", cascade-1, p);
            else
                print(c, prefix + "---- ", cPrefix + "    ", cascade-1, p);
        }
    }

    private static final Vector3i[] off = new Vector3i[]{
            new Vector3i(0,0,-1),
            new Vector3i(0,0,1),

            new Vector3i(0,1,0),
            new Vector3i(0,-1,0),
            new Vector3i(1,0,0),
            new Vector3i(-1,0,0)
    };
    static class Printer{
        public String toString(SimpleTree t) {
            return t.getValue().toString();
        }
    }
}
