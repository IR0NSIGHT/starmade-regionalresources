package org.ithirahad.resourcesresourced.RRUtils.algo;

import org.ithirahad.resourcesresourced.listeners.RRSGalaxyGeneration;
import org.newdawn.slick.util.pathfinding.navmesh.Link;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.data.Galaxy;

import java.util.*;

public class RandomZoneBuilder extends SimpleBFS {
    //zone generation params
    private int zoneSize;
    private int bufferFactor = 5;
    private HashSet<Vector3i> stars;
    private Galaxy galaxy;
    private LinkedList<Vector3i> zoneStars = new LinkedList<>();

    private Vector3i minPos = new Vector3i(-Galaxy.halfSize,-Galaxy.halfSize,-Galaxy.halfSize);
    private Vector3i maxPos = new Vector3i(Galaxy.halfSize, Galaxy.halfSize, Galaxy.halfSize);

    private int starsAtStart;
    /**
     * will build a zone of desired size around the start position. does not have any sideeffects.
     * @param start center of zone, start of search, 000 = center of galaxy
     * @param zoneSize amount of stars in the zone
     * @param stars available, unzoned stars. will not be mutated
     */
    public RandomZoneBuilder(Vector3i start, int zoneSize, HashSet<Vector3i> stars, Galaxy galaxy) {
        super(start, null); //set is null bc the overwrites make it obsolete
        assert stars != null && start != null;
        this.zoneSize = zoneSize;
        this.stars = stars;
        this.starsAtStart = stars.size();
        this.galaxy = galaxy;
        System.out.println("################### RZB with "+stars.size() + " stars to build zone of size "+ zoneSize);
    }

    /**
     * generate the zone, return list of stars in that zone. first in collection is the center.
     * @return
     */
    public Collection<Vector3i> generate() {
        //find all stars in a bigger-than-needed radius, create a tree structure from the result
         run();
        System.out.println("RZB ran and has "+stars.size() +" stars left");
        System.out.println("############");
         //filter out voids
        tree = new SimpleTreeFilter(tree){
            @Override
            protected boolean isUnwanted(SimpleTree t) {
                return !stars.contains(t.getValue());
            }
        }.filterUnwanted();
        assert starsAtStart == stars.size();
        if (tree == null) {
            System.out.println("empty zone generated");
            for (Vector3i s: zoneStars)
                System.out.println("zonestar"+s+(isStar(s)?"S":"V"));
            return new LinkedList<>();
        }

        LinkedList<Vector3i> stars = tree.getAllValues();
        for (Vector3i star: stars) {
            assert RRSGalaxyGeneration.isStar(star, galaxy);
        }
        if (stars.isEmpty())
            return stars;

        //cut down oversized tree to desired size with random shape.
        zoneStars.clear();
        for (SimpleTree t: new RandomTreeCutter(stars.size(),zoneSize,tree).run()) {
            zoneStars.add(t.getValue());
        }
        return zoneStars;
    }



    @Override
    protected void step(SimpleTree node) {
        super.step(node);
    }

    @Override
    protected void addToQueue(SimpleTree t) {
        super.addToQueue(t);
        if (isStar(t.getValue())) {
            zoneStars.add(t.getValue()); //add to list of stars in this zone
            //FIXME why was that done in the first place?-> unwanted+unnecessary sideeffect stars.remove(t.getValue()); //remove from global list of available stars.
        }
    }

    @Override
    protected boolean isValid(Vector3i point) {
        return point.betweenIncl(minPos, maxPos); //between include
    }

    @Override
    protected boolean isAbort(Vector3i point, HashSet<Vector3i> seen, SimpleTree tree) {
        return zoneStars.size() >=zoneSize*bufferFactor; //stop if we have collected enough stars times multiplier.
    }

    protected boolean isStar(Vector3i systemPosAbs) {
        return stars.contains(systemPosAbs);
    }
}
