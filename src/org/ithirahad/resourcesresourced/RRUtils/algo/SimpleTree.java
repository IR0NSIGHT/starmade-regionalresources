package org.ithirahad.resourcesresourced.RRUtils.algo;

import org.schema.common.util.linAlg.Vector3i;

import javax.annotation.Nullable;
import java.util.LinkedList;
import java.util.Objects;

public class SimpleTree {
    private Vector3i value;
    private SimpleTree parent;
    private LinkedList<SimpleTree> children = new LinkedList<>();

    public SimpleTree(Vector3i value, @Nullable SimpleTree parent) {
        this.value = value;
        this.parent = parent;
    }

    public Vector3i getValue() {
        return value;
    }

    public SimpleTree getParent() {
        return parent;
    }

    public LinkedList<SimpleTree> getChildren() {
        return children;
    }

    public void setParent(SimpleTree parent) {
        this.parent = parent;
    }

    /**
     * return root (= topmost tree that has no parent)
     * @param t
     * @return
     */
    public static SimpleTree getRoot(SimpleTree t) {
        if (t.getParent() == null)
            return t;
        return getRoot(t.getParent());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleTree that = (SimpleTree) o;
        return Objects.equals(value, that.value);
    }

    public LinkedList<Vector3i> getAllValues() {
        LinkedList<Vector3i> out = new LinkedList<>();
        this.addAllValues(out);
        return out;
    }

    private void addAllValues(LinkedList<Vector3i> list) {
        list.add(getValue());
        for (SimpleTree c: getChildren()) {
            c.addAllValues(list);
        }
    }

    public void addAllChildren(LinkedList<SimpleTree> list) {
        list.add(this);
        for (SimpleTree c: getChildren()) {
            c.addAllChildren(list);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
