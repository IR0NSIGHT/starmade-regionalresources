#version 120

uniform float time;
uniform float ambientLevel;
uniform float alpha;
uniform sampler2D lavaTex;
uniform vec4 color1;
uniform vec4 color2;

varying vec4 col;
varying vec3 normal;
varying vec3 vert;

float brightness(in vec4 color){
    return (color.r+color.g+color.b)/3.0; //very visually inaccurate as a brightness function, but does not matter because we are expecting greyscale input
}

void main()
{
    ///UNLIT COLOUR

    //mostly duplicated from lava/planet core shader
    float mx = 0.25;
    float my = 0.25;

    mx += sin(time + cos(gl_TexCoord[0].y*1.0)*0.1)/166.0;
    my += cos(time + sin(gl_TexCoord[0].x*1.0)*0.1)/166.0;

    float dx = mx;
    float dy = my;

    vec4 tex = texture2D(lavaTex,gl_TexCoord[0].xy*7.0);
    vec4 distort = texture2D(lavaTex,(gl_TexCoord[0].xy * 0.2) + vec2(dx,dy));

    dx += time/60.0;
    dy -= time/60.0;

    vec4 distort2 = texture2D(lavaTex,(gl_TexCoord[0].xy * 0.8) + vec2(dx,dy));

    float omx = sin(time/80.0 + (distort.r + distort2.g)/5.0)/2.0;
    float omy = cos(time/80.0 + (distort.g + distort2.r)/5.0)/2.0;

    vec4 tex2 = texture2D(lavaTex,(gl_TexCoord[0].xy * 2.0) + vec2(mx+omx,my+omy));

    gl_FragColor = mix(tex2 * col,tex,0.1);

    gl_FragColor.r = mix(gl_FragColor.r,1.0,1.0 - tex.a);
    gl_FragColor.g = mix(gl_FragColor.g,1.0,1.0 - tex.a);
    gl_FragColor.b = mix(gl_FragColor.b,1.0,1.0 - tex.a);
    //gl_FragColor.a = mix(gl_FragColor.a,1.0,1.0 - tex.a);

    gl_FragColor.rgb -= mx*0.6 *(distort.r+distort.g)*0.5;
    gl_FragColor.rgb -= my*0.6 *(distort.r+distort.b)*0.5;
    //gl_FragColor.rgb += pow(gl_FragColor.r, 1.1);
    gl_FragColor.a = tex.a;

    gl_FragColor *= col;

    //gl_FragColor.r += 1.0 - tex.a*1.2;
    //gl_FragColor.g += 1.0 - tex.a*1.2;
    //gl_FragColor.b += 1.0 - tex.a*1.2;
    gl_FragColor.r = mix(color1.r,color2.r,brightness(gl_FragColor));
    gl_FragColor.g = mix(color1.g,color2.g,brightness(gl_FragColor));
    gl_FragColor.b = mix(color1.b,color2.b,brightness(gl_FragColor));
    ///

    vec3 light = gl_LightSource[0].position.xyz;
    vec3 lightAngle = normalize(light-vert);
    vec3 nrm = normalize(normal);
    float diff = (max(dot(lightAngle, nrm), 0.0)/(1-ambientLevel)) + ambientLevel;

    gl_FragColor = gl_FragColor * diff;
    gl_FragColor.a = alpha;
}
