package org.ithirahad.resourcesresourced;

import org.ithirahad.resourcesresourced.industry.PassiveResourceSupplier;
import org.ithirahad.resourcesresourced.universe.gasplanet.GasGiantSheet;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemSheet;
import org.ithirahad.resourcesresourced.universe.terrestrial.TerrestrialSheet;
import api.common.GameServer;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.utils.game.PlayerUtils;
import api.utils.game.chat.CommandInterface;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.server.data.Galaxy;
import org.schema.schine.common.language.Lng;
import org.schema.schine.network.server.ServerMessage;

import javax.annotation.Nullable;
import javax.vecmath.Color4f;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.ithirahad.resourcesresourced.industry.ResourceSourceType.GROUND;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.*;
import static org.ithirahad.resourcesresourced.RRSConfiguration.ASTEROID_PLANET_FROSTLINE;
import static org.ithirahad.resourcesresourced.RRSConfiguration.ASTEROID_PLANET_FIRELINE;
import static org.ithirahad.resourcesresourced.listeners.SkyboxColoursListener.*;

public class RRSCommandRegistrar {
    public static void registerCommands(){
        StarLoader.registerCommand(new CommandInterface() {
            @Override
            public String getCommand() {
                return "rrs_location_info";
            }

            @Override
            public String[] getAliases() {
                return new String[0];
            }

            @Override
            public String getDescription() {
                return "Provides information about your current system and zone.";
            }

            @Override
            public boolean isAdminOnly() {
                return true;
            }

            @Override
            public boolean onCommand(PlayerState sender, String[] args) {
                try {
                    StringBuilder message = new StringBuilder();
                    Vector3i playerpos = sender.getCurrentSystem();
                    SystemSheet sheet = getSystemSheet(playerpos);
                    Vector3i sadj = new Vector3i(playerpos);
                    sadj.add(Galaxy.halfSize,Galaxy.halfSize,Galaxy.halfSize);
                    message.append("<TEMPORARY FEATURE; WILL BE REPLACED WITH SCANNER SYSTEM>\r\nYou are currently located in ")
                            .append(sheet.zone.galaxy.getName(sadj))
                            .append(", a ")
                            .append(sheet.systemClass.descriptor())
                            .append(" system. \r\nThis system is within the ")
                            .append(sheet.zone.zoneClass.getDescriptor())
                            .append(" zone known as the ")
                            .append(sheet.zone.name)
                            .append(".\r\n");

                    message.append(sheet.zone.zoneClass.description())
                            .append("\r\n\r\n");
                    message.append(sheet.systemClass.description());
                    TerrestrialSheet terrestrialSheet = sheet.getTerrestrial(sender.getCurrentSector());
                    if(terrestrialSheet != null){
                        message.append("\r\nThe dominant celestial body in this sector");
                        //TODO: planet name goes here if present
                        message.append(" is some sort of ");
                        message.append(terrestrialSheet.materials.description + ".");
                    }
                    GasGiantSheet gasGiantSheet = sheet.getGiants(playerpos).get(sender.getCurrentSector());
                    if(gasGiantSheet != null){
                        message.append("\r\nThe ")
                                .append(gasGiantSheet.type.typeName())
                                .append(" in this sector is ")
                                .append(gasGiantSheet.type.description());
                    }
                    sender.sendServerMessage(Lng.astr(message.toString()), ServerMessage.MESSAGE_TYPE_DIALOG);
                    return true;
                } catch(Exception e) {
                    e.printStackTrace();
                    PlayerUtils.sendMessage(sender,"[ERROR] Error retrieving RRS information for your location.");
                    return false;
                }
            }

            @Override
            public void serverAction(@Nullable PlayerState sender, String[] strings) {

            }

            @Override
            public StarMod getMod() {
                return modInstance;
            }
        });

        StarLoader.registerCommand(new CommandInterface() {
            @Override
            public String getCommand() {
                return "rrs_temperature";
            }

            @Override
            public String[] getAliases() {
                return new String[0];
            }

            @Override
            public String getDescription() {
                return "Returns the black-body equilibrium temperature at your location, in degrees Skoomanheit.";
            }

            @Override
            public boolean isAdminOnly() {
                return false;
            }

            @Override
            public boolean onCommand(PlayerState sender, String[] args) {
                try {
                    float temp = GameServer.getUniverse().getStellarSystemFromSecPos(sender.getCurrentSector()).getTemperature(sender.getCurrentSector());
                    PlayerUtils.sendMessage(sender,"Current equilibrium temperature at your location: " + temp + " degrees Skoomanheit.");
                    if(temp < ASTEROID_PLANET_FROSTLINE) PlayerUtils.sendMessage(sender,"This is beyond the frostline! Don't freeze out there.");
                    if(temp > ASTEROID_PLANET_FIRELINE) PlayerUtils.sendMessage(sender,"This is within the heat advisory radius! Be careful not to melt.");
                    return true;
                } catch (IOException e) {
                    PlayerUtils.sendMessage(sender,"[ERROR] Error getting star system instance: IOException");
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public void serverAction(@Nullable PlayerState playerState, String[] strings) {

            }

            @Override
            public StarMod getMod() {
                return modInstance;
            }
        });

        StarLoader.registerCommand(new CommandInterface() {
            @Override
            public String getCommand() {
                return "rrs_resources";
            }

            @Override
            public String[] getAliases() {
                return new String[0];
            }

            @Override
            public String getDescription() {
                return "Returns the resources available in your sector.";
            }

            @Override
            public boolean isAdminOnly() {
                return true;
            }

            @Override
            public boolean onCommand(PlayerState sender, String[] args) {
                try {
                    List<PassiveResourceSupplier[]> resources = new ArrayList<>();
                    resources.add(container.getAllAvailableSourcesInArea(sender.getCurrentSector(),sender.getCurrentSystem()));
                    StringBuilder message = new StringBuilder();
                    int i = 0;
                    message.append("The following passively-harvested resources are available in your sector ").append(sender.getCurrentSector().toString()).append(": ");
                    for(PassiveResourceSupplier[] wells : resources) for(PassiveResourceSupplier well : wells){
                        message.append("\r\n   ---").append(well.type.name()).append(" - ").append(ElementKeyMap.getInfo(well.resourceTypeId).name).append(" (Regen: ").append(well.regenRate).append(")");
                        message.append("\r\n      Harvest this resource using a ").append(well.type == GROUND ? "Magmatic Extractor" : "Vapour Siphon").append(".");
                        i++;
                    }
                    if(i==0) message.append("\r\nNone.");
                    PlayerUtils.sendMessage(sender, message.toString());
                    sender.sendServerMessage(Lng.astr(message.toString()), ServerMessage.MESSAGE_TYPE_DIALOG);
                } catch (Exception e) {
                    PlayerUtils.sendMessage(sender,"[ERROR] Error getting resource info");
                    e.printStackTrace();
                }
                return true;
            }

            @Override
            public void serverAction(@Nullable PlayerState sender, String[] strings) {}

            @Override
            public StarMod getMod() {
                return modInstance;
            }
        });

        StarLoader.registerCommand(new CommandInterface() {
            @Override
            public String getCommand() {
                return "rrs_save";
            }

            @Override
            public String[] getAliases() {
                return new String[0];
            }

            @Override
            public String getDescription() {
                return "Cleans up any orphaned passive extractor records and saves extractor persistence information.";
            }

            @Override
            public boolean isAdminOnly() {
                return true;
            }

            @Override
            public boolean onCommand(PlayerState sender, String[] args) {
                modInstance.saveExtractorData();
                return true;
            }

            @Override
            public void serverAction(@Nullable PlayerState sender, String[] strings) {}

            @Override
            public StarMod getMod() {
                return modInstance;
            }
        });

        StarLoader.registerCommand(new CommandInterface() {
            @Override
            public String getCommand() {
                return "rrs_skybox_color";
            }

            @Override
            public String[] getAliases() {
                return new String[0];
            }

            @Override
            public String getDescription() {
                return "Debug command; singleplayer only. Changes RGBA skybox color 1 or 2. If no arguments, toggles debug skybox colors on or off.";
            }

            @Override
            public boolean isAdminOnly() {
                return true;
            }

            @Override
            public boolean onCommand(PlayerState sender, String[] args) {
                if((GameClientState.instance != null)) {
                    if (args.length == 0) debugColors = !debugColors;
                    if (args.length == 1) debugColors = Boolean.parseBoolean(args[0]);
                    else if (args.length == 5) {
                        Color4f color = new Color4f(Float.parseFloat(args[1]), Float.parseFloat(args[2]), Float.parseFloat(args[3]), Float.parseFloat(args[4]));
                        switch (args[0]) {
                            case "1": dc1.set(color);
                                break;
                            case "2": dc2.set(color);
                                break;
                            default:
                                PlayerUtils.sendMessage(sender,"[ERROR] Invalid Color Slot: " + args[0]);
                                sender.sendServerMessage(Lng.astr("[ERROR] Invalid Color Slot: " + args[0]), ServerMessage.MESSAGE_TYPE_ERROR);
                        }
                    } else {
                        PlayerUtils.sendMessage(sender,"[ERROR] Invalid Arguments");
                        sender.sendServerMessage(Lng.astr("[ERROR] Invalid Arguments"), ServerMessage.MESSAGE_TYPE_ERROR);
                    }
                }
                return false;
            }

            @Override
            public void serverAction(@Nullable PlayerState playerState, String[] strings) {

            }

            @Override
            public StarMod getMod() {
                return modInstance;
            }
        });

    }
}
