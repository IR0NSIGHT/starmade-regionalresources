package org.ithirahad.resourcesresourced.listeners;

import org.ithirahad.resourcesresourced.universe.galaxy.ResourceZone;
import org.ithirahad.resourcesresourced.universe.galaxy.ZoneClass;
import org.ithirahad.resourcesresourced.universe.galaxy.ZoneColorInformation;
import api.listener.EventPriority;
import api.listener.Listener;
import api.listener.events.world.ProceduralSkyboxColorEvent;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.server.data.Galaxy;

import javax.vecmath.Color4f;
import java.util.Random;

import static org.ithirahad.resourcesresourced.ResourcesReSourced.zoneMaps;

public class SkyboxColoursListener extends Listener<ProceduralSkyboxColorEvent> {
    public static boolean debugColors = false;
    public static Color4f dc1 = new Color4f(); //debug colors
    public static Color4f dc2 = new Color4f();

    public SkyboxColoursListener(){
        super(EventPriority.HIGHEST);
    }

    @Override
    public void onEvent(ProceduralSkyboxColorEvent p) {

        if(debugColors){
            p.setColor1(dc1.x,dc1.y,dc1.z,dc1.w);
            p.setColor2(dc2.x,dc2.y,dc2.z,dc2.w);
            return;
        }

        Vector3i sys = p.getSystemCoordinates();
        Galaxy gx = GameClientState.instance.getCurrentGalaxy();
        Random rand = new Random(gx.getSeed() * sys.hashCode());

        ResourceZone zone = zoneMaps.get(gx.galaxyPos).get(sys);
        if(zone.zoneClass != ZoneClass.NONE) {
            ZoneColorInformation colourRange = zone.zoneClass.colors();

            float r;
            float g;
            float b;
            float a;

            if (colourRange.clampHueAndSaturation) {
                float s = rand.nextFloat(); //scale factor
                r = colourRange.minValues.x + ((colourRange.maxValues.x - colourRange.minValues.x) * s);
                g = colourRange.minValues.y + ((colourRange.maxValues.y - colourRange.minValues.y) * s);
                b = colourRange.minValues.z + ((colourRange.maxValues.z - colourRange.minValues.z) * s);
            } else {
                r = colourRange.minValues.x + ((colourRange.maxValues.x - colourRange.minValues.x) * rand.nextFloat());
                g = colourRange.minValues.y + ((colourRange.maxValues.y - colourRange.minValues.y) * rand.nextFloat());
                b = colourRange.minValues.z + ((colourRange.maxValues.z - colourRange.minValues.z) * rand.nextFloat());
            }
            a = colourRange.minValues.w + ((colourRange.maxValues.w - colourRange.minValues.w) * rand.nextFloat());
            p.setColor1(r, g, b, a);

            if (colourRange.clampHueAndSaturation) {
                float s = rand.nextFloat();
                r = colourRange.minValues.x + ((colourRange.maxValues.x - colourRange.minValues.x) * s);
                g = colourRange.minValues.y + ((colourRange.maxValues.y - colourRange.minValues.y) * s);
                b = colourRange.minValues.z + ((colourRange.maxValues.z - colourRange.minValues.z) * s);
            } else {
                r = colourRange.minValues.x + ((colourRange.maxValues.x - colourRange.minValues.x) * rand.nextFloat());
                g = colourRange.minValues.y + ((colourRange.maxValues.y - colourRange.minValues.y) * rand.nextFloat());
                b = colourRange.minValues.z + ((colourRange.maxValues.z - colourRange.minValues.z) * rand.nextFloat());
            }
            a = colourRange.minValues.w + ((colourRange.maxValues.w - colourRange.minValues.w) * rand.nextFloat());
            p.setColor2(r, g, b, a);
        }
    }
}
