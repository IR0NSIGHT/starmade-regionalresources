package org.ithirahad.resourcesresourced.listeners;

import org.ithirahad.resourcesresourced.industry.Extractor;
import org.ithirahad.resourcesresourced.industry.PassiveResourceSupplier;
import api.listener.fastevents.FactoryManufactureListener;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import org.ithirahad.resourcesresourced.industry.ResourceSourceType;
import org.ithirahad.resourcesresourced.vfx.ExtractorPulseEffect;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.factory.FactoryCollectionManager;
import org.schema.game.common.controller.elements.factory.FactoryProducerInterface;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.data.element.meta.RecipeInterface;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.ServerConfig;
import org.schema.schine.graphicsengine.movie.craterstudio.data.tuples.Pair;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import java.util.List;

import static org.ithirahad.resourcesresourced.RRSConfiguration.EXTRACTION_RATE_PER_SECOND_PER_BLOCK;
import static org.ithirahad.resourcesresourced.industry.ResourceSourceType.GROUND;
import static org.ithirahad.resourcesresourced.industry.ResourceSourceType.SPACE;
import static org.ithirahad.resourcesresourced.listeners.BlockRemovalLogic.isExtractor;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.container;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType.*;

public class ExtractorTickFastListener implements FactoryManufactureListener {
    @Override
    public boolean onPreManufacture(FactoryCollectionManager factoryCollectionManager, Inventory inventory, LongOpenHashSet[] longOpenHashSets) {
        if (GameServerState.instance == null) return true;

        SegmentPiece block = inventory.getBlockIfPossible();
        if (block == null || !isExtractor(block.getType()))
            return true;

        SegmentController entity = factoryCollectionManager.getSegmentController();
        if(entity.getType() == SHIP) return true;

        if ((block.getType() == elementEntries.get("Magmatic Extractor").id && (entity.getType() != PLANET_SEGMENT && entity.getType() != PLANET_ICO)))
            return false;
        //A magmatic extractor not placed on a planet is not (or should not be) doing anything. No need to waste any more time.

        boolean isPowered = factoryCollectionManager.consumePower(factoryCollectionManager.getElementManager());
        boolean hasCapacity = true;
        PassiveResourceSupplier[] localWells;
        Vector3i sector = entity.getSector(new Vector3i());
        ResourceSourceType type;
        if (block.getType() == elementEntries.get("Magmatic Extractor").id){
            localWells = container.getPassiveSources(sector);
            type = GROUND;
        }
        else {
            localWells = container.getAllAvailableSourcesInArea(entity);
            type = SPACE;
        }

        String uid = Extractor.generateUID(entity.getUniqueIdentifier(),sector,factoryCollectionManager.getControllerPos());
        System.out.println("################Ticking extractor-factory "+uid);
        for (PassiveResourceSupplier well : localWells) { //update all wells: collect stuff and create harvesters for unloaded colletion
            well.updateLoaded();
            //ABORT IF: remove harvester if unpowered or not matching farmed ore type
            if(!isPowered || well.type != type) {
                //TODO: Also on any other conditions for removal
                well.removeExtractor(uid);
                continue;
            }

            Extractor harvester;
                //add harvester if doenst exist yet
            if(!well.contains(uid)) {
                harvester = well.addExtractor(entity.getUniqueIdentifier(),factoryCollectionManager.getControllerPos(), type, sector);
                harvester.setCapacityVolume(inventory.getCapacity());
            } else  //get existing
                harvester = well.getExtractor(uid);
            //update extracting power
            harvester.setStrength(factoryCollectionManager.getFactoryCapability() * EXTRACTION_RATE_PER_SECOND_PER_BLOCK);

            //collected farmed stuff from the harvester, put into physical factory
            int materials = well.claimCollectedResources(uid, inventory.getCapacity());
            int miningBonus = getMiningBonus(entity.getFactionId(), entity.getSectorId());
            entity.getConfigManager().apply(StatusEffectType.MINING_BONUS_ACTIVE, miningBonus);
            //System.out.println("claimed "+ materials+ " units of ore wiht mining bonus="+ miningBonus);
            materials *= miningBonus;
            float volPerUnit = Math.max(ElementKeyMap.getInfo(well.resourceTypeId).volume,0.00000000001f); //no dividing by zero, even if something's misconfigured...
            if(((materials * volPerUnit) + inventory.getVolume()) > inventory.getCapacity()){ //overflow
                hasCapacity = false; //TODO: notify
                materials = (int)Math.floor((inventory.getCapacity() - inventory.getVolume())/volPerUnit);
            }
            int inventoryChange = inventory.incExistingOrNextFreeSlotWithoutException(well.resourceTypeId, materials);
            inventory.sendInventoryModification(inventoryChange);

            //update remaining inventory size on harvester
            harvester.setCapacityVolume(Math.max(0,inventory.getCapacity() - inventory.getVolume()));
            //System.out.println(well);
            System.out.println(harvester);
        }

        if(block.getType() == elementEntries.get("Vapor Siphon").id && isPowered && hasCapacity) { //do vfx
            //Vector3f endpoint = new Vector3f(entity.getWorldTransform().origin);
            Vector3f endpoint = block.getWorldPos(new Vector3f(), entity.getSectorId());
            Vector3f startpoint;
            String currentSectorSize = ServerConfig.SECTOR_SIZE.getCurrentState().toString();
            //Vector3f blockOffset = new Vector3i(block.x,block.y,block.z).toVector3f();
            //endpoint.add(blockOffset);

            List<Pair<Vector3i, Vector4f>> fx = container.getAvailableSpaceSourcesForDisplay(entity);
            for (Pair<Vector3i, Vector4f> entry : fx){
                if(entry.first() == sector) startpoint = new Vector3f(0,0,0); //same sector, so assume centre of sector for now
                else{
                    startpoint = new Vector3f(entry.first().toVector3f());
                    startpoint.scale(Float.parseFloat(currentSectorSize));
                }
                ExtractorPulseEffect.fireEffectServer(entity.getSectorId(), sector, startpoint, endpoint, entry.second());
            }
        } //TODO: else magma extraction effect, whatever that would look like
        return false;
    }

    @Override
    public void onProduceItem(RecipeInterface recipeInterface, Inventory inventory, FactoryProducerInterface factoryProducerInterface, FactoryResource factoryResource, int i, IntCollection intCollection) {
        //nothing ever happens
        //and i wonder.
    }

    static int getMiningBonus(int factionId, int sectorId) {
        return GameServerState.instance.getUniverse().getSystemOwnerShipType(sectorId, factionId).getMiningBonusMult() * (Integer) ServerConfig.MINING_BONUS.getCurrentState();
    }
}