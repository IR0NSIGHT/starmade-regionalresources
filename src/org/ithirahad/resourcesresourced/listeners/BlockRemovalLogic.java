package org.ithirahad.resourcesresourced.listeners;

import api.listener.events.block.SegmentPieceAddByMetadataEvent;
import api.listener.events.block.SegmentPieceAddEvent;
import org.ithirahad.resourcesresourced.industry.PassiveResourceSupplier;
import org.ithirahad.resourcesresourced.industry.ResourceSourceType;
import org.ithirahad.resourcesresourced.ResourcesReSourced;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemSheet;
import api.listener.Listener;
import api.listener.events.block.SegmentPieceRemoveEvent;
import api.listener.events.entity.SegmentControllerOverheatEvent;
import api.mod.StarLoader;
import org.schema.common.util.linAlg.Vector3b;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.movie.craterstudio.data.tuples.Pair;

import javax.annotation.Nullable;

import static api.mod.StarLoader.registerListener;
import static org.ithirahad.resourcesresourced.industry.ResourceSourceType.*;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.container;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.getSystemSheet;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType.*;

/**
 * Created by Jake on 2/24/2021.
 * Modified by Ithirahad in Mar-Apr 2021.
 */
public class BlockRemovalLogic {

    public static void initListeners(){
        StarLoader.registerListener(SegmentPieceRemoveEvent.class, new Listener<SegmentPieceRemoveEvent>() {
            @Override
            public void onEvent(SegmentPieceRemoveEvent event) {
                if((GameServerState.instance != null) && isExtractor(event.getType())) //no need to do this stuff on dedicated clients
                {
                    SimpleTransformableSendableObject.EntityType entityType = event.getSegment().getSegmentController().getType();
                    ResourceSourceType thisExtractorType = event.getType() == elementEntries.get("Magmatic Extractor").id ? GROUND : SPACE;
                    if ((entityType != PLANET_SEGMENT && entityType != PLANET_ICO) && thisExtractorType == GROUND)
                        return; //magmatic extractor not on planet; didn't do anything to begin with.

                    String thisEntityUID = event.getSegment().getSegmentController().getUniqueIdentifier();

                    Vector3i sys = event.getSegment().getSegmentController().getSystem(new Vector3i());
                    SystemSheet sheet = getSystemSheet(Galaxy.getContainingGalaxyFromSystemPos(sys, new Vector3i()), sys);
                    assert sheet != null;

                    PassiveResourceSupplier[] passiveResourceSupplies = container.getAllAvailableSourcesInArea(event.getSegment().getSegmentController());

                    for (PassiveResourceSupplier well : passiveResourceSupplies) if (well != null) {

                        well.removeExtractor(thisEntityUID);
                    }
                }
            }
        }, ResourcesReSourced.modInstance);

        StarLoader.registerListener(SegmentControllerOverheatEvent.class, new Listener<SegmentControllerOverheatEvent>() {
            @Override
            public void onEvent(SegmentControllerOverheatEvent event) {
                if(event.getEntity().getType() != SHIP) {
                    String uid = event.getEntity().getUniqueIdentifier();
                    PassiveResourceSupplier[] wells = container.getAllAvailableSourcesInArea(event.getEntity());
                    for (PassiveResourceSupplier well : wells)
                        if (well != null && well.contains(uid)) {
                            well.removeExtractor(uid);
                        }
                }
            }
        }, ResourcesReSourced.modInstance);


        registerListener(SegmentPieceAddEvent.class, new Listener<SegmentPieceAddEvent>() {
            @Override
            public void onEvent(SegmentPieceAddEvent e) {
                short b = e.getNewType();
                SimpleTransformableSendableObject.EntityType etype = e.getSegmentController().getType();
                if (GameServerState.instance != null && ((ElementKeyMap.getInfo(b).factory != null && etype == ASTEROID_MANAGED) || (isExtractor(b) && etype == SHIP))) {
                    SegmentPieceCleanupRunner.cleanupQueue.enqueue(new Pair<>(new Vector3b(e.getX(), e.getY(), e.getZ()), e.getSegment()));
                }
            }
        }, ResourcesReSourced.modInstance);

        registerListener(SegmentPieceAddByMetadataEvent.class, new Listener<SegmentPieceAddByMetadataEvent>() {
            @Override
            public void onEvent(SegmentPieceAddByMetadataEvent e) {
                short b = e.getType();
                SimpleTransformableSendableObject.EntityType etype = e.getSegment().getSegmentController().getType();
                if (GameServerState.instance != null && ((ElementKeyMap.getInfo(b).factory != null && etype == ASTEROID_MANAGED) || (isExtractor(b) && etype == SHIP))) {
                    SegmentPieceCleanupRunner.cleanupQueue.enqueue(new Pair<>(new Vector3b(e.getX(), e.getY(), e.getZ()), e.getSegment()));
                }
            }
        }, ResourcesReSourced.modInstance);
    }
    /*
    public static void tryPlaceExtractor(SegmentPieceAddInfo info, boolean playerPlacement) {
        if(GameServerState.instance != null && info.segment.getSegmentController().getType() != SHIP) {
            SegmentController controller = info.segment.getSegmentController();
            ResourceSourceType thisExtractorType = info.type == elementEntries.get("Magmatic Extractor").id ? GROUND : SPACE;
            if (!(controller.getType() == PLANET_SEGMENT || controller.getType() == PLANET_ICO) && thisExtractorType == GROUND)
                return;
            //magma extractor not on a planet; can't possibly do anything.

            long absIndex = info.absIndex;
            Vector3i sys = controller.getSystem(new Vector3i());
            SystemSheet sheet = getSystemSheet(Galaxy.getContainingGalaxyFromSystemPos(sys, new Vector3i()), sys);
            assert sheet != null;

            addExtractorIfAbsent(controller.getSector(new Vector3i()), controller, thisExtractorType, absIndex);
            //There was a second type check here to filter out magmatic extractors on stations/asteroids/etc., but it was kind of redundant.
        }
    }
     */

    /**
     * Checks for the presence of extractor blocks of a given type.
     * @param entity The entity to check
     * @param type Whether to look for ground or space extractors
     */
    static int numberOfExtractors(SegmentController entity, ResourceSourceType type) {
        ElementCountMap allBlocksOnEntity = entity.getElementClassCountMap();
        int extractorCount;
        if(type == SPACE) extractorCount = allBlocksOnEntity.get(elementEntries.get("Vapor Siphon").id);
        else extractorCount = allBlocksOnEntity.get(elementEntries.get("Magmatic Extractor").id);
        return extractorCount;
    }

    public static boolean isExtractor(short id){
        return (id == elementEntries.get("Vapor Siphon").id || id == elementEntries.get("Magmatic Extractor").id);
    }

    public static SegmentPiece getBlockFromSegment(Segment segment, Vector3b tileLoc, @Nullable SegmentPiece segmentPieceTmp) {
        if (segmentPieceTmp == null) {
            segmentPieceTmp = new SegmentPiece();
        }

        segmentPieceTmp.setByReference(segment, tileLoc.x, tileLoc.y, tileLoc.z);
        return segmentPieceTmp;
    }

    public static Vector3i getStarResourceSector(Vector3i system){
        Vector3i sector = new Vector3i(system);
        sector.scale(VoidSystem.SYSTEM_SIZE);
        sector.add(8,8,8); //center of system(?) Doesn't really matter tbh; could use the corner
        return sector;
    }

    public static class SegmentPieceAddInfo {
        //public PlayerState playerState;
        public short type;
        public Segment segment;
        public byte x;
        public byte y;
        public byte z;
        public long absIndex;

        public SegmentPieceAddInfo(){}

        public SegmentPieceAddInfo(short type, long absIndex, byte x, byte y, byte z, Segment segment) {
            this.type = type;
            this.absIndex = absIndex;
            this.x = x;
            this.y = y;
            this.z = z;
            this.segment = segment;
        }

        public SegmentPieceAddInfo(SegmentPieceAddEvent e) {
            this(e.getNewType(), e.getAbsIndex(), e.getX(), e.getY(), e.getZ(), e.getSegment());
        }
    }
}

