package org.ithirahad.resourcesresourced.listeners;

import it.unimi.dsi.fastutil.Hash;
import org.ithirahad.resourcesresourced.RRUtils.SerializableVector3i;
import org.ithirahad.resourcesresourced.RRUtils.SpaceGridMap;
import org.ithirahad.resourcesresourced.RRUtils.algo.RandomZoneBuilder;
import org.ithirahad.resourcesresourced.ResourcesReSourced;
import org.ithirahad.resourcesresourced.industry.ResourceSourceType;
import org.ithirahad.resourcesresourced.universe.galaxy.ResourceZone;
import org.ithirahad.resourcesresourced.universe.galaxy.ZoneClass;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemClass;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemSheet;
import api.listener.EventPriority;
import api.listener.Listener;
import api.listener.events.world.GalaxyFinishedGeneratingEvent;
import org.newdawn.slick.util.pathfinding.navmesh.Link;
import org.schema.common.FastMath;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;

import java.util.*;

import static org.ithirahad.resourcesresourced.RRSConfiguration.*;
import static org.ithirahad.resourcesresourced.RRUtils.MiscUtils.lerp;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.*;
import static org.ithirahad.resourcesresourced.universe.galaxy.ZoneClass.RRClasses;
import static org.ithirahad.resourcesresourced.universe.galaxy.ZoneClass.RR_CORE;

public class RRSGalaxyGeneration extends Listener<GalaxyFinishedGeneratingEvent>{

    public RRSGalaxyGeneration(){
        super(EventPriority.MONITOR); //In reality, probably no mod should be trying to change the galaxy AFTER it generates, but just in case one does, we'll give it time to do so.
    }

    //debug
    public static Galaxy currentG;

    @Override
    public void onEvent(GalaxyFinishedGeneratingEvent e) {
        currentG = e.getGalaxy();
        if(galaxiesDoneGenerating.contains(e.getGalaxy().galaxyPos)){
            System.err.println("[MOD][RRS][WARNING] RRS information for galaxy at " + e.getGalaxy().galaxyPos + " already exists in this session! Aborting generation.");
            return;
        }

        System.err.println("[MOD][RRS] Galaxy generation complete. Preparing to create galaxy regions...");
        ArrayList<Vector3i> allStars = new ArrayList<>(ResourcesReSourced.availableStarsLists.get(e.getGalaxy().galaxyPos));
        Random random_GALACTIC = new Random(e.getGalaxy().getSeed());
        long galaxySeed = seeds.get(e.getGalaxy().galaxyPos);

        SerializableVector3i thisGalaxy = new SerializableVector3i(e.getGalaxy().galaxyPos);

        SpaceGridMap<ResourceZone> zoneMap = new SpaceGridMap<>(); //map of zones to stars in this galaxy
        LinkedList<ResourceZone> zoneList = new LinkedList<>();

        //Generate zones to fill area
        System.err.println("[MOD][Resources ReSourced] Beginning zone generation for galaxy " + e.getGalaxy().galaxyPos.toString() + "; going for " + ((ZoneClass.RRClasses().length * RESOURCE_ZONES_PER_TYPE) + (USE_GALAXY_CORE_REGION? 1 : 0)) + " zones.");

        //generate inner, mid and outer star lists
        Vector3i furthestStar = new Vector3i();
        for(Vector3i star : allStars) if(star.lengthSquared() > furthestStar.lengthSquared()) furthestStar.set(star);
        float gc;
        if(USE_GALAXY_CORE_REGION) gc = GALAXY_CORE_RADIUS;
        else gc = 0;
        float range = furthestStar.length() - gc;
        float midZoneTypeStart = (range*MID_ZONE_TYPE_START) + gc; //one-third mark
        int midZoneRadiusSquared = (int) (midZoneTypeStart * midZoneTypeStart);
        float outerZoneTypeStart = (range*FAR_ZONE_TYPE_START) + gc; //two-thirds mark
        int outerZoneRadiusSquared = (int) (outerZoneTypeStart * outerZoneTypeStart);

        LinkedList<Vector3i> innerTemp = new LinkedList<>(),midTemp = new LinkedList<>(),outerTemp = new LinkedList<>();
        for(Vector3i star : allStars)
            if(star.lengthSquared() > outerZoneRadiusSquared) outerTemp.add(star);
            else if(star.lengthSquared() > midZoneRadiusSquared) midTemp.add(star);
            else if(star.lengthSquared() > gc) innerTemp.add(star);

        //inner mid outer lists done
        final ArrayList<Vector3i> innerStars = new ArrayList<>(innerTemp);
        final ArrayList<Vector3i> midStars = new ArrayList<>(midTemp);
        final ArrayList<Vector3i> outerStars = new ArrayList<>(outerTemp);


    //generate zones now

        //set of stars, looses stars to zones.
        HashSet<Vector3i> availableStars = new HashSet<>(allStars);
        assert availableStars.size()>0;

        int allStarCount = allStars.size();
        //generate list of zones with size etc
        LinkedList<ZoneClass> types = new LinkedList<>();
        LinkedList<Integer> sizes = new LinkedList<>();
        int size = 10; //size of a zone
        int zones = 3; //amount of zones per type
        for (ZoneClass type: RRClasses()) {
            for (int i = 0; i < zones*type.getFrequencyMultiplier();i++) {
                types.add(type);
                int s = (int) (size*type.getRadiusMultiplier());
                sizes.add(s);
                System.out.println("Zone "+ type.getDescriptor()+ " size"+ s);
            }
        }
        Iterator<ZoneClass> itZ = types.iterator();
        Iterator<Integer> itS = sizes.iterator();
        Random r = new Random(galaxySeed);
        while (itZ.hasNext()) {
            ZoneClass type = itZ.next();
            size = itS.next();
            if (size<=0)
                continue;
            //choose a star to start at depending on the zone placement: inner mid or outer belt (or any)
            ArrayList<Vector3i> belt;
            switch (type.getPlacement()) {
                case INNER: belt = innerStars; break;
                case MID: belt = midStars; break;
                case OUTER: belt = outerStars; break;
                default: belt = allStars;
            }
            Vector3i center = getRandomAvailableStar(belt, r.nextLong());
            System.out.println("Build zone " + type.getDescriptor() +" size " + size + " around " + center);

            //handle core type
            if (type.equals(RR_CORE)) {
                if (USE_GALAXY_CORE_REGION)
                    center.set(0,0,0);
                else
                    continue;
            }
            assert center != null;

            //generate zone
            ResourceZone z = generateZone(center, e.getGalaxy(), galaxySeed, type, availableStars, (int) (size*type.getRadiusMultiplier()));
            for (Vector3i star: z.getStars()) { //FIXME contains non star systems.
                assert isStar(star,e.getGalaxy());
            }
            zoneList.add(z);
            //remove used stars from global list of unused stars.
            for (Vector3i star: z.getStars()) {
                availableStars.remove(star);
            }
            System.out.println("zone added: " + type.getDescriptor() + " with " + z.getStars().size() + " stars. availalbe remaining: " + availableStars.size());
        }

        System.err.println("[MOD][Resources ReSourced] Finished generating resource zones.");
        assert  allStarCount == allStars.size();

        //add all unused/remaining stars to "barrens" "region" //TODO questionable to even need that zone, any star thats not in the map is a "barren zone" star by default.
        //TODO manipulate global zoneMap star->zone that it returns "barrens" for any star it doesnt know.
        ResourceZone barrens = new ResourceZone(e.getGalaxy(),"Galactic Barrens", ZoneClass.NONE, availableStars);
        zoneList.add(barrens);

        //map all stars for star->zone
        for (ResourceZone zone: zoneList) {
            for (Vector3i star: zone.getStars()) {
                zoneMap.put(star,zone);
            }
        }
        //assert zoneMap.count() == allStarCount;

        SystemSheet voidPlaceholder = new SystemSheet(SystemClass.NORMAL_VOID,0.0f, barrens, 0.0f);
        GALACTIC_VOIDS.put(thisGalaxy, voidPlaceholder);

        zoneMaps.put(thisGalaxy, zoneMap);

        galaxiesDoneGenerating.add(new Vector3i(e.getGalaxy().galaxyPos));
    }

    /**
     * standard pos, 0 0 0 is galaxy center
     * @param system
     * @return
     */
    public static boolean isStar(Vector3i system, Galaxy g) {
        return g.isStellarSystem(Galaxy.getRelPosInGalaxyFromAbsSystem(system,new Vector3i()));
    }

    protected Vector3i getRandomAvailableStar(ArrayList<Vector3i> list, long seed) {
        Random r = new Random(seed);
        assert list.size()>0;
        int n = r.nextInt(list.size()); //safety abort
        Vector3i rVec = list.get(n);
        assert rVec != null;
        return rVec;
    }

    /**
     * generate zone from parameters, automatically removes used stars.
     * @param start start and center of zone, can be void.
     * @param g galaxy t pass to zone
     * @param seed seed to pass to the zone (unused for generation)
     * @param type Zone type
     * @param stars available star list
     * @param starCount amount of stars the zone will have (precise)
     * @return
     */
    private ResourceZone generateZone(Vector3i start, Galaxy g, long seed, ZoneClass type, HashSet<Vector3i> stars, int starCount) {
        //generate list of stars inside the zone
        RandomZoneBuilder rzb = new RandomZoneBuilder(start, starCount, stars, g);
        Collection<Vector3i> zoneStars = rzb.generate();

        //instantiate and fill zone object
        ResourceZone zone = new ResourceZone(g, seed, type);
        zone.addAll(zoneStars);

        System.out.println("Generate zone " +zone);
        return zone;
    }
}
