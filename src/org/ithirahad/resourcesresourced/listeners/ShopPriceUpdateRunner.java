package org.ithirahad.resourcesresourced.listeners;

import api.utils.StarRunnable;
import it.unimi.dsi.fastutil.objects.ObjectArrayFIFOQueue;
import org.schema.game.common.controller.SegmentController;
import static org.ithirahad.resourcesresourced.industry.ShopPricingManager.setNPShopPricesIfApplicable;

public class ShopPriceUpdateRunner extends StarRunnable {
    public static final ObjectArrayFIFOQueue<SegmentController> shopQueue = new ObjectArrayFIFOQueue<>();

    @Override
    public void run() {
        while(!shopQueue.isEmpty()){
            SegmentController entity = shopQueue.dequeue();
            setNPShopPricesIfApplicable(entity);
        }
    }
}
