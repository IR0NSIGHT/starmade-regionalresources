package org.ithirahad.resourcesresourced.listeners;

import org.ithirahad.resourcesresourced.universe.asteroid.RRAsteroidType;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemSheet;
import api.listener.Listener;
import api.listener.events.controller.asteroid.AsteroidSegmentGenerateEvent;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.FloatingRock;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.server.data.Galaxy;

import static org.ithirahad.resourcesresourced.ResourcesReSourced.getSystemSheet;
import static org.schema.game.common.data.world.SectorInformation.SectorType.VOID;

public class AsteroidGenerationListener extends Listener<AsteroidSegmentGenerateEvent> {
    @Override
    public void onEvent(AsteroidSegmentGenerateEvent e) {
        FloatingRock asteroid = (FloatingRock) e.getAsteroidCreatorThread().getSegmentController();
        try {
            long seed = asteroid.getCreatorThread().getSegmentController().getSeed();

            Vector3i hotPotato = new Vector3i(); //just a vector3i to pass around and return sometimes; be careful not to catch it byref or you lose.
            Vector3i systemCoords = new Vector3i(e.getAsteroidCreatorThread().getSegmentController().getSystem(hotPotato)); //I hope this arg is correct - it seems to just be a write target that gets hot-potatoed a whole lot and then finally returned
            //TODO: nullpointer
            StellarSystem sys = asteroid.getRemoteSector().getServerSector()._getSystem();

            if (sys.getCenterSectorType() != VOID) {
                Vector3i galaxyCoords = Galaxy.getContainingGalaxyFromSystemPos(sys.getPos(), hotPotato);

                SystemSheet systemSheet;

                systemSheet = getSystemSheet(galaxyCoords, systemCoords); //get sheet for this zone

                float temperature = sys.getTemperature(asteroid.getSector(new Vector3i()));
                //basically a reimplementation of a Sector method that is for some reason private

                RRAsteroidType rockType = systemSheet.pickRockType(temperature, seed);

                //if(debug) System.err.println("[MOD][Resources ReSourced] Selected type " + rockType.name() + " for rock in class " + systemSheet.systemClass.classLetter() + " system " + systemCoords.toString() + ", of the " + systemSheet.zone.name + ".");
                //if(debug) System.err.println("Temperature:" + temperature + "; Outside frostline of " + frostLine + ": " + (temperature < frostLine) + "; Inside burn radius: " + (isInBurnRadius));
                //Set the creator id to our rock type, so we know what string to draw on CustomAsteroidNameListener
                asteroid.setModCustomNameServer(rockType.getNiceName());
                e.setWorldCreatorFloatingRockFactory(rockType.getFactory(seed));
            } else {
                //Set the creator id to our rock type, so we know what string to draw on CustomAsteroidNameListener
                asteroid.setModCustomNameServer(RRAsteroidType.TEMPERATE.getNiceName());
                e.setWorldCreatorFloatingRockFactory(RRAsteroidType.TEMPERATE.getFactory(seed)); //voids get generic rocks
            }
        } catch (NullPointerException ex){
            /*if(ConfigValues.debug) {
                System.err.println("[MOD][Resources ReSourced][ERROR] Nullpointer error in asteroid generation! Printing stack trace and exiting.");
                ex.printStackTrace();
                System.exit(-1);
            }
            else {
             */
                long seed = asteroid.getCreatorThread().getSegmentController().getSeed();
                asteroid.setModCustomNameServer("Asteroid machine broke");
                e.setWorldCreatorFloatingRockFactory(RRAsteroidType.TEMPERATE.getFactory(seed)); //fallback
            //}
        }
    }
}
