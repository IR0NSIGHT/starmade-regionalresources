package org.ithirahad.resourcesresourced;

import api.DebugFile;
import api.listener.events.controller.ClientInitializeEvent;
import api.listener.events.controller.shop.ShopGenerateEvent;
import api.listener.events.player.PlayerChangeSectorEvent;
import api.listener.events.player.PlayerSpawnEvent;
import api.listener.events.register.RegisterAddonsEvent;
import api.listener.fastevents.BlockConfigLoadListener;
import api.utils.gui.ModGUIHandler;
import glossar.GlossarCategory;
import glossar.GlossarEntry;
import glossar.GlossarInit;
import it.unimi.dsi.fastutil.objects.Object2LongOpenHashMap;
import org.ithirahad.resourcesresourced.RRUtils.DebugUI;
import org.ithirahad.resourcesresourced.gui.AstrometricScanControlManager;
import org.ithirahad.resourcesresourced.gui.GasPlanetMapDrawer;
import org.ithirahad.resourcesresourced.gui.ZoneMapDrawer;
import org.ithirahad.resourcesresourced.industry.*;
import org.ithirahad.resourcesresourced.listeners.*;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.ithirahad.resourcesresourced.RRUtils.SpaceGridMap;
import org.ithirahad.resourcesresourced.network.ExtractorFXRemoteExecuteTemporaryPacket;
import org.ithirahad.resourcesresourced.network.ScannerFXRemoteExecuteTemporaryPacket;
import org.ithirahad.resourcesresourced.network.SystemScanInfo;
import org.ithirahad.resourcesresourced.shipsystems.ReconAstrometricScannerAddOn;
import org.ithirahad.resourcesresourced.vfx.ParticleEffectsManager;
import org.ithirahad.resourcesresourced.universe.galaxy.ResourceZone;
import org.ithirahad.resourcesresourced.universe.gasplanet.GasPlanetDrawer;
import org.ithirahad.resourcesresourced.universe.gasplanet.GasPlanetInnerAtmosphereDrawer;
import org.ithirahad.resourcesresourced.universe.gasplanet.GasPlanetOuterAtmosphereDrawer;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemSheet;
import org.ithirahad.resourcesresourced.universe.terrestrial.TerrestrialSheet;
import api.common.GameCommon;
import api.common.GameServer;
import api.config.BlockConfig;
import api.listener.EventPriority;
import api.listener.Listener;
import api.listener.events.controller.ServerInitializeEvent;
import api.listener.events.controller.asteroid.AsteroidSegmentGenerateEvent;
import api.listener.events.controller.planet.PlanetSegmentGenerateEvent;
import api.listener.events.draw.RegisterWorldDrawersEvent;
import api.listener.events.gui.HudCreateEvent;
import api.listener.events.inventory.metaobject.InventoryPutMetaItemEvent;
import api.listener.events.network.ClientLoginEvent;
import api.listener.events.state.ShipyardEnterStateEvent;
import api.listener.events.world.*;
import api.listener.fastevents.FactoryManufactureListener;
import api.listener.fastevents.FastListenerCommon;
import api.mod.ModSkeleton;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.mod.config.FileConfiguration;
import api.mod.config.PersistentObjectUtil;
import api.mod.config.SyncedConfigReceiveEvent;
import api.mod.config.SyncedConfigUtil;
import api.utils.StarRunnable;
import api.utils.particle.ModParticleUtil;
import api.utils.textures.StarLoaderTexture;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.ElementCountMap;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.elements.shipyard.orders.states.Constructing;
import org.schema.game.common.controller.elements.shipyard.orders.states.Deconstructing;
import org.schema.game.common.controller.generator.PlanetCreatorThread;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.meta.BlueprintMetaItem;
import org.schema.game.common.data.element.meta.MetaObjectManager;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.world.*;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.controller.EntityNotFountException;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;
import org.schema.game.server.data.PlayerNotFountException;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.network.RegisteredClientOnServer;
import org.schema.schine.network.server.ServerMessage;
import org.schema.schine.resource.MeshLoader;
import org.schema.schine.resource.ResourceLoader;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.util.*;

import static api.common.GameClient.getClientState;
import static api.network.packets.PacketUtil.registerPacket;
import static org.ithirahad.resourcesresourced.industry.RRSRecipeManager.getConstituentComponents;
import static org.ithirahad.resourcesresourced.RRSCommandRegistrar.registerCommands;
import static org.ithirahad.resourcesresourced.RRSConfiguration.*;
import static org.ithirahad.resourcesresourced.universe.gasplanet.GasPlanetDrawer.displayGiants;
import static api.mod.StarLoader.registerListener;
import static org.schema.schine.network.server.ServerMessage.MESSAGE_TYPE_ERROR;

public class ResourcesReSourced extends StarMod {
    public static ResourcesReSourced modInstance;
    public static boolean bastionInitiativeIsPresentAndActive = false;

    //------CLIENTSIDE DATA UPDATE INFO
    public static Vector3i clientSystem;
    public static SystemSheet clientSystemSheet; //todo: optimize this stuff
    public static StarRunnable systemUpdate = null;

    //what is this list for? seems like galaxy->star in that galaxy map??
    public static HashMap<Vector3i, List<Vector3i>> availableStarsLists; //todo: spacegridmap? shouldn't matter too much either way
    public static boolean displayBrokenBlocksWarning = false;

    Vector3i newPos;
    public static boolean clientSpawned = false;
    public static HashSet<Vector3i> galaxiesDoneGenerating = null;

    //-------UNIVERSE GEN STUFF
    public static Object2LongOpenHashMap<Vector3i> seeds = new Object2LongOpenHashMap<>(); //galaxy to seed

    public static SpaceGridMap<SpaceGridMap<ResourceZone>> zoneMaps = null; //map of galactic dictionaries of zone maps. Might be able to do arraylists rather than hashmaps.

    public static final SpaceGridMap<SystemSheet> GALACTIC_VOIDS = new SpaceGridMap<>(); //galaxy to generic void grid map; determinism does not matter much her :D

    //EXTRACTOR STUFF

    public static PassiveResourceSourcesContainer container;
    public static PassiveResourceSourcesContainer persistenceContainer; //'lazy' backup of container for save/load, containing only active containers (others can be freely regenerated)
    FactoryManufactureListener extractorTickListener;
    FactoryManufactureListener productionTickListener;
    //TODO: Maybe have one per system or per galaxy or something; one giga-hashmap seems... well, huge (and makes the container class pretty redundant)

    ///

    //ASSET STUFF TODO: move to world asset manager; this is getting out of hand :D

    public static final ArrayList<Sprite> gasGiantTextures = new ArrayList<>();
    public static Sprite gasGiantMapSprite;
    public static Shader gasGiantSurfaceShader;
    public static Shader gasGiantInteriorShader;

    public static Sprite gasGiantMapBackLayerSprite;
    public static Sprite gasGiantMapFrontLayerSprite;

    public static Sprite genericFlareSprite;
    //TODO: DOCUMENT EVERYTHING.

    //UI
    public static AstrometricScanControlManager astroScanControlManager;
    ///

    private ZoneMapDrawer zoneMapDrawer;
    @Override
    public void onResourceLoad(ResourceLoader loader){
        try {
            //map drawers
            zoneMapDrawer = new ZoneMapDrawer(this);
            zoneMapDrawer.loadSprite(this); //loads predefined sprite into zonemapdrawer.

            new GasPlanetMapDrawer(this).loadSprites();
            //!map drawers

            loader.getAudioLoader().loadSound("mods/Resources ReSourced.jar/org/ithirahad/resourcesresourced/assets/sound/scanner-ping.wav", "astro scanner ping", "wav");

            gasGiantTextures.add(StarLoaderTexture.newSprite(ImageIO.read(getJarResource("org/ithirahad/resourcesresourced/assets/image/world/Gas_Giant_1.png")), this, "Gas Giant 1"));
            gasGiantTextures.add(StarLoaderTexture.newSprite(ImageIO.read(getJarResource("org/ithirahad/resourcesresourced/assets/image/world/Gas_Giant_2.png")), this, "Gas Giant 2"));

            gasGiantMapSprite = StarLoaderTexture.newSprite(ImageIO.read(getJarResource("org/ithirahad/resourcesresourced/assets/image/map/gasGiant-1x1-c-.png")), this, "gasGiant-1x1-c-");
            gasGiantMapSprite.setPositionCenter(true);
            gasGiantMapSprite.setWidth(128);
            gasGiantMapSprite.setHeight(128);

            gasGiantMapBackLayerSprite = StarLoaderTexture.newSprite(ImageIO.read(getJarResource("org/ithirahad/resourcesresourced/assets/image/map/gasGiantBacking-1x1-c-.png")), this, "gasGiantBacking-1x1-c-");
            gasGiantMapFrontLayerSprite = StarLoaderTexture.newSprite(ImageIO.read(getJarResource("org/ithirahad/resourcesresourced/assets/image/map/gasGiantBall-1x1-c-.png")), this, "gasGiantBall-1x1-c-");

            gasGiantMapBackLayerSprite.setPositionCenter(true);
            gasGiantMapBackLayerSprite.setWidth(128);
            gasGiantMapBackLayerSprite.setHeight(128);

            gasGiantMapFrontLayerSprite.setPositionCenter(true);
            gasGiantMapFrontLayerSprite.setWidth(128);
            gasGiantMapFrontLayerSprite.setHeight(128);
        } catch (IOException| ResourceException ex) {
            ex.printStackTrace();
        }
        MeshLoader mloader = loader.getMeshLoader();
        try {
            mloader.loadModMesh(this,"planet_sphere", getJarResource("org/ithirahad/resourcesresourced/assets/model/planet_sphere.zip"), null);
            mloader.loadModMesh(this,"Sphere", getJarResource("org/ithirahad/resourcesresourced/assets/model/Sphere.zip"), null);
        } catch (ResourceException | IOException ex) {
            DebugFile.log("RRS sphere and planet sphereIO exception (expected)");
        }
        System.err.println("[MOD][Resources ReSourced] Finished loading models!");
        try {

            gasGiantSurfaceShader = Shader.newModShader(getSkeleton(), "GasGiantShader",
                    getJarResource("org/ithirahad/resourcesresourced/assets/shader/temp_gasgiant.vert"),
                    getJarResource("org/ithirahad/resourcesresourced/assets/shader/temp_gasgiant.frag"));
            gasGiantInteriorShader = Shader.newModShader(getSkeleton(), "GasGiantInnerShader",
                    getJarResource("org/ithirahad/resourcesresourced/assets/shader/gasgiant_interior.vert"),
                    getJarResource("org/ithirahad/resourcesresourced/assets/shader/gasgiant_interior.frag"));
            System.err.println("[MOD][Resources ReSourced] Finished loading shaders!");
        } catch (IOException | ResourceException ex) {
            System.err.println("[MOD][Resources ReSourced][ERROR] Error loading mod resources!");
            ex.printStackTrace();
        }
    }
    ///

    @Override
    public void onLoad(){modInstance = this;}

    public void initStatics(){
        if(zoneMaps == null) zoneMaps = new SpaceGridMap<>();
        if(galaxiesDoneGenerating == null) galaxiesDoneGenerating = new HashSet<>();
        if(availableStarsLists == null) availableStarsLists = new HashMap<>();
    }

    @Override
    public void onUniversalRegistryLoad() {
        System.err.println("[MOD][Resources ReSourced] Registering mod URVs.");
        RRSElementInfoManager.registerURVs(this);
    }

    @Override
    public void onBlockConfigLoad(BlockConfig config) {
        System.err.println("[MOD][Resources ReSourced] Beginning element initialization.");
        try {
            RRSElementInfoManager.loadElements(this, config);
        } catch (Exception ex) {
            DebugFile.logError(ex,this);
            ex.printStackTrace();
        }
    }

    @Override
    public void onLoadModParticles(ModParticleUtil.LoadEvent event) {
        ParticleEffectsManager.init(event, this);
    }

    @Override
    public void onServerCreated(ServerInitializeEvent event) {
        initStatics();
    }

    @Override
    public void onClientCreated(ClientInitializeEvent e) {
        addModGlossarEntries();
        initStatics();
    }

    @Override
    public void onEnable() {

        //register debug stuff
        StarLoader.registerCommand(new DebugUI());
        //-

        registerPacket(SystemScanInfo.class);
        registerPacket(ExtractorFXRemoteExecuteTemporaryPacket.class);
        registerPacket(ScannerFXRemoteExecuteTemporaryPacket.class);
        //only needed because of strange in-built StarLoader particle method behaviour
        //TODO: map/FoW packets

        FastListenerCommon.blockConfigLoadListeners.add(new BlockConfigLoadListener() {
            @Override
            public void preBlockConfigLoad(){}
            @Override
            public void postBlockConfigLoad(){
                RRSRecipeManager.addModRecipes();
            }
            @Override
            public void onModLoadBlockConfig_POST(StarMod starMod) {}
            @Override
            public void onModLoadBlockConfig_PRE(StarMod starMod){}
        });

        final FileConfiguration config = getConfig(CONFIG_NAME);
        updateConfiguration(config,true); //just in case

        StarLoader.registerListener(ClientLoginEvent.class, new Listener<ClientLoginEvent>() {
            @Override
            public void onEvent(ClientLoginEvent event) {
                if(GameServerState.instance != null) SyncedConfigUtil.sendConfigToClient(event.getServerProcessor(), config); //TODO: send FoW profile
            }
        }, this);

        StarLoader.registerListener(RegisterAddonsEvent.class, new Listener<RegisterAddonsEvent>() {
            @Override
            public void onEvent(RegisterAddonsEvent event) {
                System.err.println("[MOD][Resources ReSourced] Registering scanner add-ons...");
                event.addModule(new ReconAstrometricScannerAddOn(event.getContainer(), modInstance));
            }
        }, modInstance);


        StarLoader.registerListener(SyncedConfigReceiveEvent.class, new Listener<SyncedConfigReceiveEvent>() {
            @Override
            public void onEvent(SyncedConfigReceiveEvent event) {
                FileConfiguration recievedConfig = event.getConfig();
                ModSkeleton spookyScary = recievedConfig.getMod().getSkeleton();
                if(spookyScary.getName().equals(this.getMod().getSkeleton().getName())) { //There's only one config for this mod at the moment anyway; everything else is persistentobject stuff.
                    for (String key : event.getConfig().getKeys()) {
                        String val = event.getConfig().getString(key);
                        config.set(key, val);
                        System.err.println("[MOD][Resources ReSourced] Received value for " + key + ": " + val);
                    }
                    updateConfiguration(config,false);
                }
            }
        }, this);
        //TODO: Does this even work?

        ArrayList<Object> resSourceObjects = PersistentObjectUtil.getObjects(modInstance.getSkeleton(), PassiveResourceSourcesContainer.class);
        if (resSourceObjects.isEmpty()) {
            PersistentObjectUtil.addObject(modInstance.getSkeleton(), new PassiveResourceSourcesContainer());
        }

        //load resource well container from POU
        container = new PassiveResourceSourcesContainer();
        try {
            persistenceContainer = (PassiveResourceSourcesContainer) resSourceObjects.get(0);
            if (persistenceContainer == null)
                persistenceContainer = new PassiveResourceSourcesContainer();
            else {
                container.copyFrom(persistenceContainer);
                container.afterDeserialize();
            }
        } catch(Exception ignore){
            System.err.println("[MOD][Resources ReSourced] Detected bad persistence file! All passive extractors will have to be reloaded in order to function");
        }

        extractorTickListener = new ExtractorTickFastListener();
        productionTickListener = new ProductionTickFastListener();
        FastListenerCommon.factoryManufactureListeners.add(0,extractorTickListener);
        FastListenerCommon.factoryManufactureListeners.add(0,productionTickListener);

        registerListener(HudCreateEvent.class, new CustomAsteroidNameListener(), this);

        registerListener(HudCreateEvent.class, new Listener<HudCreateEvent>() {
            @Override
            public void onEvent(HudCreateEvent hudCreateEvent) {
                if(GameClientState.instance != null) {
                    astroScanControlManager = new AstrometricScanControlManager(getClientState());
                    ModGUIHandler.registerNewControlManager(getSkeleton(), astroScanControlManager); //TODO: right place?
                }
            }
        },this);

        /*
        registerListener(PlanetTypeSelectEvent.class, new Listener<PlanetTypeSelectEvent>(){
            @Override
            public void onEvent(PlanetTypeSelectEvent e) {
                StellarSystem sys = e.getStellarSystem();
                SystemSheet sheet = getSystemSheet(sys.getPos());
                if(!sheet.containsTerrestrialAt(e.getSectorCoordinates()))
                    sheet.addTerrestrial(sys, e.getSectorCoordinates());
                e.setPlanetType(sheet.getTerrestrial(e.getSectorCoordinates()).planetType.vanillaEquiv()); //mostly for map, but also fallback for any other missing behaviours
                //seeing as the new planet segments don't know where there are, is there any way to make the sheet data hitch a ride on the planet type or something here?
            }
        },this);
        //Never fired on its own and ultimately created infinite loops...
        //redundant anyway as any attempt to access the planet type will call the relevant method regardless
         */

        registerListener(PlayerChangeSectorEvent.class, new Listener<PlayerChangeSectorEvent>() {
            @Override
            public void onEvent(PlayerChangeSectorEvent playerChangeSectorEvent) {
                if(GameClientState.instance != null){
                    updateLocalInfo();
                }
            }
        }, this);

        registerListener(PlayerSpawnEvent.class, new Listener<PlayerSpawnEvent>() {
            @Override
            public void onEvent(PlayerSpawnEvent playerSpawnEvent) {
                if(GameClientState.instance != null){
                    updateLocalInfo();
                    if(displayBrokenBlocksWarning) playerSpawnEvent.getPlayer().getOwnerState().sendClientMessage("WARNING: RRS has detected that your session's block config is broken. The game may crash or not behave as expected. Closing and restarting StarMade should fix this in most cases.", ServerMessage.MESSAGE_TYPE_DIALOG);
                }
            }
        }, this);

        registerListener(PlanetCreateEvent.class, new Listener<PlanetCreateEvent>() {
            @Override
            public void onEvent(PlanetCreateEvent e) {
                if(DEBUG_LOGGING) System.err.println("[MOD][Resources ReSourced] Fired planet create event!");
                Vector3i systemPos = e.getSector()._getSystem().getPos();
                SystemSheet sheet = getSystemSheet(systemPos);
                assert sheet != null;
                if(!sheet.containsTerrestrialAt(e.getSector().pos)) {
                    System.err.println("[MOD][Resources ReSourced][WARNING] Late planet insertion; should already have been generated!");
                    sheet.addTerrestrial(e.getSector()._getSystem(), e.getSector().pos);
                }
                TerrestrialSheet planetInfo = sheet.getTerrestrial(e.getSector().pos);
                if(planetInfo != null){
                    for(Planet seg : e.getPlanetSegments()){
                        PlanetCreatorThread thread;
                        if(seg.getCreatorThread() != null){
                            thread = (PlanetCreatorThread) seg.getCreatorThread();
                            thread.creator = planetInfo.getCreatorFactory(seg);
                            if(DEBUG_LOGGING) System.err.println("[MOD][Resources ReSourced] Successfully replaced planet segment creator!");
                        }
                        else {
                            thread = new PlanetCreatorThread(seg, SectorInformation.PlanetType.MARS); //Doesn't actually matter
                            thread.creator = planetInfo.getCreatorFactory(seg);
                            seg.setCreatorThread(thread);
                            if(DEBUG_LOGGING) System.err.println("[MOD][Resources ReSourced] Successfully created new planet gen thread & replaced segment creator!");
                        }
                    }
                }
                else System.err.println("[MOD][Resources ReSourced][WARNING] Unable to find or create planet info sheet at " + e.getSectorPos());
            }
        }, this);

        registerListener(PlanetSegmentGenerateEvent.class, new Listener<PlanetSegmentGenerateEvent>() {
            @Override
            public void onEvent(PlanetSegmentGenerateEvent e) {
                Planet entity = (Planet) e.getSegment().getSegmentController();
                if(DEBUG_LOGGING) System.err.println("[MOD][Resources ReSourced] Fired planet create event!");
                Vector3i systemPos = e.getSegment().getSegmentController().getSystem(new Vector3i());
                Vector3i sectorPos = e.getSegment().getSegmentController().getSector(new Vector3i());
                SystemSheet sheet = getSystemSheet(systemPos);
                if(sheet != null) {
                    if (!sheet.containsTerrestrialAt(sectorPos)) {
                        StellarSystem system = entity.getRemoteSector().getServerSector()._getSystem();
                        sheet.addTerrestrial(system, sectorPos);
                        System.err.println("[MOD][Resources ReSourced] Did not find planet on system sheet at " + sectorPos + ". Created new planet sheet.");
                    }
                    TerrestrialSheet planetInfo = sheet.getTerrestrial(sectorPos);
                    if (planetInfo != null) {
                        PlanetCreatorThread thread = e.getCreatorThread();
                        thread.creator = planetInfo.getCreatorFactory(entity);
                        if(DEBUG_LOGGING) System.err.println("[MOD][Resources ReSourced] Successfully replaced planet segment creator!");
                    }
                }
                else{
                    PlanetCreatorThread thread = e.getCreatorThread();
                    thread.creator = TerrestrialSheet.getFallbackPlanetCreator(entity);
                    System.err.println("[MOD][Resources ReSourced][WARNING] Unable to retrieve system sheet. Using generic fallback planet profile (barren lifeless rock).");
                }
                //TODO: else generate generic barren planet (bad for debug, good for release closure)
            }
        },this);

        registerListener(ShipyardEnterStateEvent.class,
            new Listener<ShipyardEnterStateEvent>() {
                @Override
                public void onEvent(ShipyardEnterStateEvent event) {
                    if(event.getEnteredState() instanceof Constructing){
                        Constructing newState = (Constructing) event.getEnteredState() ;
                        ElementCountMap resources = getConstituentComponents(newState.getEntityState().currentMapFrom);
                        newState.getEntityState().currentMapFrom.resetAll();
                        newState.getEntityState().currentMapFrom = resources;
                        MiscUtils.setPrivateField("currentFill", newState, newState.getEntityState().currentMapTo.getTotalAmount());
                    } else if(event.getEnteredState()  instanceof Deconstructing){
                        Deconstructing newState = (Deconstructing) event.getEnteredState();
                        ElementCountMap resources = getConstituentComponents(newState.getEntityState().currentMapFrom);
                        newState.getEntityState().currentMapFrom.resetAll();
                        newState.getEntityState().currentMapFrom = resources;
                    }
                }
            },this); //TODO: (POST RELEASE) Remove this in favor of a proper new feature. Design schematics should accommodate blocks OR their constituent components.

        registerListener(InventoryPutMetaItemEvent.class, new Listener<InventoryPutMetaItemEvent>() {
            @Override
            public void onEvent(InventoryPutMetaItemEvent event) {
                if(event.getTypeId() == MetaObjectManager.MetaObjectType.BLUEPRINT.type){

                    BlueprintMetaItem item = (BlueprintMetaItem) event.getMetaObject();
                    BluePrintController bpct = BluePrintController.active;
                    try {
                        BlueprintEntry en = bpct.getBlueprint(item.blueprintName, bpct.readBluePrints());
                        ElementCountMap orig = en.getElementCountMapWithChilds();
                        MiscUtils.setSuperclassPrivateField(BlueprintEntry.class,"countWithChilds", en, getConstituentComponents(orig));
                        System.err.println("[MOD][Resources ReSourced");
                    } catch (EntityNotFountException e) {
                        System.err.println("[MOD][Resources ReSourced][ERROR] Unable to locate blueprint entry for ingredient list replacement! Blueprint item will not be functional.");
                        e.printStackTrace();
                    } catch (Exception e) {
                        System.err.println("[MOD][Resources ReSourced][ERROR] Unable to replace blueprint ingredients for unknown reason! Blueprint item will not be functional.");
                        e.printStackTrace();
                    }

                    ElementCountMap originalGoal = item.goal;
                    item.goal = getConstituentComponents(originalGoal);

                    boolean done = true;
                    for(short i = 0; i < ElementKeyMap.highestType + 1; i++){
                        int goal = item.goal.get(i);
                        int progress = item.progress.get(i);
                        if(progress > goal){
                            item.progress.reset(i);
                            if(goal != 0) item.progress.put(i,goal);
                        }
                        else if(goal > 0 && progress < goal) done = false;
                    }
                    if(done) item.progress = item.goal;
                }
            }
        }, this); //TODO: (POST RELEASE) Remove this in favor of a proper new feature. Blueprints should accommodate blocks OR their constituent components.

        registerListener(GalaxyGenerationEvent.class, new Listener<GalaxyGenerationEvent>(EventPriority.POST) {
            @Override
            public void onEvent(GalaxyGenerationEvent e) {
                System.err.println("[MOD][Resources ReSourced] Initializing RRS for generation... Galaxy: " + e.getGalaxyCoordinates());
                //init
                Vector3i coord = e.getGalaxyCoordinates();
                if (!availableStarsLists.containsKey(coord)) availableStarsLists.put(coord, new ArrayList<Vector3i>());
                seeds.put(e.getGalaxyCoordinates(),e.getSeed());
            }
        }, this);

        registerListener(StarCreationAttemptEvent.class, new Listener<StarCreationAttemptEvent>(EventPriority.POST) {
            @Override
            public void onEvent(StarCreationAttemptEvent e) {
                Vector3i galaxyCoord = e.getGalaxy().galaxyPos;
                List<Vector3i> availableStarsHere;
                if (!availableStarsLists.containsKey(galaxyCoord)){
                    availableStarsHere = new LinkedList<>();
                    availableStarsLists.put(galaxyCoord, availableStarsHere);
                    System.err.println("[MOD][Resources ReSourced][WARNING] LATE INSERTION OF GALAXY INTO AVAILABLE STAR LISTINGS");
                }
                else availableStarsHere = availableStarsLists.get(galaxyCoord);
                //Add star to list of stars if weight is correct
                Vector3i pos = e.getPosition();
                Vector3i posAdjusted = new Vector3i(pos);
                posAdjusted.add(-64,-64,-64);
                //boolean isStar = e.getGalaxy().isStellarSystem(pos);//Origin-centered version. (I wasted an entire day due to this.)
                if (!availableStarsHere.contains(posAdjusted) && e.getGalaxy().isStellarSystem(pos)) { //"contains?" check to eliminate any chance of duplicates for debug purposes
                    availableStarsHere.add(posAdjusted); //Yeah... I really, really, really hope that the coordinate systems match up and we aren't deep in the black void or out of bounds.
                }
            }
        }, this);

        registerListener(GalaxyFinishedGeneratingEvent.class, new RRSGalaxyGeneration(), this);

        registerListener(AsteroidSegmentGenerateEvent.class, new AsteroidGenerationListener(), this);

        registerListener(AsteroidNormalPopulateEvent.class, new Listener<AsteroidNormalPopulateEvent>(EventPriority.POST) { //ideally overrides all other mods
            final Vector3i sector = new Vector3i();
            final Vector3i system = new Vector3i();
            @Override
            public void onEvent(AsteroidNormalPopulateEvent e) {
                Galaxy g = GameServerState.instance.getUniverse().getGalaxyFromSystemPos(e.getSystem().getPos());
                sector.set(e.getSector().pos);
                VoidSystem.getPosFromSector(sector, system);
                SystemSheet sheet = getSystemSheet(g.galaxyPos, system); //TODO: SHOULD NEVER N E V E R BE NULL!!!
                if(sheet != null && sheet.getGiants(system).containsLocation(sector)) {
                    e.overridePopulation(true);
                    e.setForcedAsteroidCount(0); //yes, a lot of gas-giant moons are asteroids. I can't be arsed to simulate that at the moment.
                    //TODO: simulate that, or add moons. At least try to put asteroids outside of gas giants rather than yeeting them :P
                }
            }
        }, this);

       registerListener(RegisterWorldDrawersEvent.class, new Listener<RegisterWorldDrawersEvent>() {
           @Override
           public void onEvent(RegisterWorldDrawersEvent ev) {
               ev.getModDrawables().add(new GasPlanetDrawer());
               ev.getModDrawables().add(new GasPlanetOuterAtmosphereDrawer());
               ev.getModDrawables().add(new GasPlanetInnerAtmosphereDrawer());
           }
       },this);

        BlockRemovalLogic.initListeners();

        registerListener(ShopGenerateEvent.class, new Listener<ShopGenerateEvent>() {
            //TODO: Does this fire upon every loading of a shop? Or just the first-ever spawn?
            // If it's every loading, we'll need those moddy mcmodface modder module madness monkey machines.
            // In this case, a 'monkey' on the back of every loaded shop that makes sure it keeps consistent prices.
            // For now we'll just set all the prices based on the absolute price of the blocks, but that's boring...
            // ...also, maybe change shop colour in zones with custom generators (maybe even dinged-up ones in barrens, adv armour in ferron, etc.)
            @Override
            public void onEvent(ShopGenerateEvent e) {
                if(GameServerState.instance != null)
                    ShopPriceUpdateRunner.shopQueue.enqueue(e.getStation());
            }
        }, this);

        /*
        registerListener(SegmentControllerFullyLoadedEvent.class, new Listener<SegmentControllerFullyLoadedEvent>() {
            @Override
            public void onEvent(SegmentControllerFullyLoadedEvent event) {
                if(event.getController().getType() == SPACE_STATION) try {
                    SegmentController entity = event.getController();
                    SerializableVector3i sector = new SerializableVector3i(entity.getSector(new Vector3i())); //ew
                    SystemSheet sheet = getSystemSheet(event.getController().getSystem(new Vector3i()));
                    if (sheet.getGiants(entity.getSystem(new Vector3i())).keySet().contains(sector)) {
                        Vector3f planetCentre = new Vector3f(sheet.getGiants(entity.getSystem(new Vector3i())).get(sector).inSectorOffset); //The gas giant's centre
                        Transform oldt = entity.getWorldTransform();
                        System.err.println("[MOD]ResourcesReSourced] Current world transform rotation: " + '\n' + entity.getWorldTransform().basis.toString());
                        System.err.println("[MOD][ResourcesReSourced] Forcing change to station transforms' rotation.");
                        entity.getWorldTransform().setRotation(LookAt(entity.getWorldTransform().origin, planetCentre, true));
                        System.err.println("[MOD][ResourcesReSourced] Current world transform rotation is finally: " + '\n' + entity.getWorldTransform().basis.toString());

                        Transform newt = new Transform(entity.getWorldTransform());
                        entity.getWorldTransform().set(oldt);
                        entity.warpTransformable(newt,true,true, null);
                        //no idea what this sorcery does, but the anti collision teleport does it this way, so it should work, right?
                    }
                }
                catch(Exception ex){
                    System.err.println("[MOD][Resources ReSourced] Unable to reorient station to match gas giant...");
                    ex.printStackTrace();
                    System.err.println();
                }
            }
        }, this);
        //TODO: FIX ORIENTATION
         */

        new StarRunnable(){
            @Override
            public void run(){
                //Client-side local world info check
                if(GameClientState.instance != null){
                    updateLocalInfo();
                }
            }
        }.runTimer(this,5);

        new PassiveResourceUpdater().runTimer(this, 1);

        new SegmentPieceCleanupRunner().runTimer(this,1);

        new ShopPriceUpdateRunner().runTimer(this,1);

        new StarRunnable(){ //Remote clients do not need this. They should simply see stuff going into inventories magically.
            boolean firstRun = true;
            boolean doTick = false;
            final Timer timer = new Timer();
            @Override
            public void run() {
                if(GameServerState.instance == null)return;
                if (firstRun) {
                    timer.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            doTick = true;
                        }
                    }, 0, PASSIVE_EXTRACTOR_STATE_SAVE_INTERVAL_MS);
                }
                firstRun = false;

                if (doTick && (GameServerState.instance != null)) { //do not do this on MP clients
                    saveExtractorData();
                    doTick = false;
                }
            }
        }.runTimer(this, 1);

        registerListener(ProceduralSkyboxColorEvent.class, new SkyboxColoursListener(), this);
        FastListenerCommon.sectorUpdateListeners.add(new GasPlanetPhysicsListener());

        registerCommands();
    }

    @Override
    public void onDisable(){
        if(GameServerState.instance != null) saveExtractorData();
    }

    public static SystemSheet getSystemSheet(Vector3i starSystem){
        SystemSheet result;
        Vector3i galaxy;
        if(GameServerState.instance != null) galaxy = Galaxy.getContainingGalaxyFromSystemPos(starSystem, new Vector3i());
        else galaxy = GameClientState.instance.getCurrentGalaxy().galaxyPos; //sussy option but should be fine in the limited cases where this happens on client
        result = getSystemSheet(galaxy, starSystem);
        return result;
    }

    public static SystemSheet getSystemSheet(Vector3i galaxy, Vector3i starSystem){
        if(galaxiesDoneGenerating.contains(galaxy)) try {
            //System.err.print("Attempting sheet retrieval from system " + starSystem.toString() + " in galaxy " + galaxy.toString());
            SpaceGridMap<ResourceZone> zoneDictionary;
            zoneDictionary = zoneMaps.get(galaxy);
            if (zoneDictionary != null && zoneDictionary.containsLocation(starSystem)) {
                //System.err.println("[MOD][Resources ReSourced] Found corresponding zone: " + zoneDictionary.get(starSystem).name);
                return zoneDictionary.get(starSystem).getSheetFor(starSystem);
            } else {
                try {
                    Galaxy gx;
                    if(GameServerState.instance != null) gx = GameServer.getServerState().getUniverse().getGalaxy(galaxy);
                    else gx = getClientState().getCurrentGalaxy();
                    if(DEBUG_LOGGING) System.err.println("[MOD][StarLoader][WARNING] Using current local galaxy as fallback for void sheet retrieval; may produce erroneous results if mismatched");
                    if(gx.isVoid(Galaxy.getRelPosInGalaxyFromAbsSystem(starSystem,new Vector3i()))) return GALACTIC_VOIDS.get(galaxy);
                }
                catch(Exception ignore){
                    if(DEBUG_LOGGING) System.err.println("[MOD][StarLoader][WARNING] Could not retrieve generic void sheet");
                }
                if(DEBUG_LOGGING) System.err.println("[MOD][StarLoader][WARNING] Could not retrieve system sheet:");
                if(DEBUG_LOGGING) System.err.print("Galaxy " + galaxy.toString() + "; System " + starSystem.toString());
                return null; //...should we just generate a new one? This should never be a problem, but somehow it is.
            }
        }
        catch (Exception e){
            e.printStackTrace();
            if(DEBUG_LOGGING) System.err.println("[MOD][StarLoader][ERROR] Could not retrieve system sheet:");
            if(DEBUG_LOGGING) System.err.print("Galaxy " + galaxy.toString() + "; System " + starSystem.toString());
            if(GameServerState.instance != null) for (RegisteredClientOnServer client : GameServer.getServerState().getClients().values()) {
                try {
                    PlayerState player =  GameServer.getServerState().getPlayerFromName(client.getPlayerName());
                    if(player.isAdmin()) player.sendServerMessage(Lng.astr("[SERVER][ERROR] Could not retrieve star system information when requested."), MESSAGE_TYPE_ERROR); //TODO: MESSAGE_TYPE_DIALOG can be used for the sensor readouts for now.
                } catch (PlayerNotFountException ignore) {}
            }
            return null;
        }
        else{
            if(DEBUG_LOGGING) {
                System.err.println("[MOD][StarLoader][ERROR] Attempted to retrieve system sheet before any galaxy finished generating...");
                System.err.print(Arrays.toString(Thread.currentThread().getStackTrace()));
            }
            return null;
        }
    }

    public void updateLocalInfo(){
        try {
            PlayerState player = GameClientState.instance.getPlayer();
            Galaxy curr = null;
            try{
                curr = GameClientState.instance.getCurrentGalaxy();
            } catch (Exception ignore){}

            if (    galaxiesDoneGenerating != null
                    && curr != null
                    && galaxiesDoneGenerating.contains(GameClientState.instance.getCurrentGalaxy().galaxyPos)
                    && GameClientState.instance != null
                    && player != null
            ) {
                try {
                    newPos = player.getCurrentSystem();
                    if (newPos != null && !newPos.equals(clientSystem)) {
                        if (clientSystem == null) clientSystem = new Vector3i();
                        clientSystem.set(newPos);
                        Vector3i galaxy = Galaxy.getContainingGalaxyFromSystemPos(newPos, new Vector3i());
                        clientSystemSheet = getSystemSheet(galaxy, newPos);
                        if (clientSystemSheet != null)
                            displayGiants = clientSystemSheet.getGiants(clientSystem).toHashMap();
                        else displayGiants.clear();
                    }
                } catch(Exception ignore) {
                    // seems like the proper clientside event doesn't exist to start doing this error-free,
                    // so we are doing this cop-out try catch instead. Thankfully, very much not performance-critical.
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
            System.err.println("[MOD][ResourcesReSourced][FATAL ERROR] Unable to update local info. This would cause major errors; shutting down preemptively. Please report this error, and send logs.");
            System.exit(-1);
        }
    }

        /*
    > Logic:
      For every system:
        For every block:
          Add const/blockcount
      For every system:
        For every station/block:
          If its loaded, add `resources` to its inventory

    > Structure:
     [System Pos]:
         [Station + Pos of block] + resources

    > Place logic:
      On Special block place:
         - Remove station from array if it exists
         - Add [Station+Pos of block to array]
      On special block remove:
         - Remove station from the array
      On station overheat:
         - Remove station from array

    > Possible flaws:
     - Station is removed by some other means, could accumilate resources in the background.
       Possible fix: Check if a station still exists in the database every shutdown

     */
    public void saveExtractorData(){

        if(DEBUG_LOGGING) for (RegisteredClientOnServer client : GameServer.getServerState().getClients().values()) {
            try {
                PlayerState player =  GameServer.getServerState().getPlayerFromName(client.getPlayerName());
                if(player.isAdmin()) player.sendServerMessage(Lng.astr("[SERVER] \r\n Saving passive resource extractor persistence data..."), ServerMessage.MESSAGE_TYPE_WARNING); //TODO: MESSAGE_TYPE_DIALOG can be used for the sensor readouts for now.
            } catch (PlayerNotFountException ignore) {}
        }
        persistenceContainer.beforeSerialize();
        persistenceContainer.copyOnlyActiveFrom(container);
        if(DEBUG_LOGGING) {
            Set<Vector3i> locations = persistenceContainer.getMap().keySet();
            System.err.println("[MOD][Resources ReSourced] Saving passive resource extractor persistence data for sources at the following locations: ");
            if(locations.size() == 0) System.err.println("None.");
            for (Vector3i location : locations) {
                System.err.println(location + " - " + location.hashCode());
            }
        } else System.err.println("[MOD][Resources ReSourced] Saving passive resource extractor persistence data for " + persistenceContainer.getMap().keySet().size() + " active sources...");
        PersistentObjectUtil.save(modInstance.getSkeleton());
    }

    public void updateConfiguration(FileConfiguration config, boolean writeToFile){
            USE_GALAXY_CORE_REGION = Boolean.parseBoolean(config.getConfigurableValue("UseGalacticCoreZone", "true"));
            GALAXY_CORE_RADIUS = Float.parseFloat(config.getConfigurableValue("GalacticCoreZoneRadiusInSystems", "4"));

            MID_ZONE_TYPE_START = Float.parseFloat(config.getConfigurableValue("MidZoneTypeStartRadius", "0.25"));
            FAR_ZONE_TYPE_START = Float.parseFloat(config.getConfigurableValue("OuterZoneTypeStartRadius", "0.55"));

            RESOURCE_ZONES_PER_TYPE = Integer.parseInt(config.getConfigurableValue("NumberOfZonesPerType", "3"));
            RESOURCE_ZONE_MIN_STARS = Integer.parseInt(config.getConfigurableValue("MinimumStarCountForValidZone", "5")); //irrelevant tbh; the game can't neatly handle filling any reasonable percentage of the grid squares
            ZONE_STAR_LOOK_RADIUS_INITIAL = Float.parseFloat(config.getConfigurableValue("ZoneFirstPassSearchRadiusInSystems", "1.5"));
            ZONE_STAR_LOOK_RADIUS_SECONDARY = Float.parseFloat(config.getConfigurableValue("ZoneLaterPassSearchRadiusInSystems", "1.35"));
            SEARCH_RADIUS_MODIFIER_FOR_DISTANCE_FROM_GALACTIC_CENTRE = Float.parseFloat(config.getConfigurableValue("SearchRadiusDistanceModifier","2"));
            ZONE_SEARCH_BLOBS_MIN = Integer.parseInt(config.getConfigurableValue("ZoneSearchStepsMin","2"));
            ZONE_SEARCH_BLOBS_MAX = Integer.parseInt(config.getConfigurableValue("ZoneSearchStepsMax","4"));

            NAME_GENERIC_ADJECTIVE_CHANCE = Float.parseFloat(config.getConfigurableValue("ZoneNamingGenericAdjectiveChance","0.75"));
            NAME_PREFIX_CHANCE = Float.parseFloat(config.getConfigurableValue("ZoneNamingPrefixChance","0.1"));

            ASTEROID_PLANET_FROSTLINE = Float.parseFloat(config.getConfigurableValue("CelestialGenFrostLine","0.4"));
            ASTEROID_PLANET_FIRELINE = Float.parseFloat(config.getConfigurableValue("CelestialGenLavaLine","0.7"));

            MAX_SYSTEM_RESOURCE_DENSITY = Float.parseFloat(config.getConfigurableValue("SystemResourceDensityMax","1.0")); //ideally should not exceed 1
            MIN_SYSTEM_RESOURCE_DENSITY = Float.parseFloat(config.getConfigurableValue("SystemResourceDensityMin","0.1"));
            
            GAUSSIAN_PASSIVE_RESOURCE_BONUS_CAP = Float.parseFloat(config.getConfigurableValue("GaussianPassiveResourceBonusCap", "9.0"));
            GAUSSIAN_PASSIVE_RESOURCE_BONUS_SIGMA = Float.parseFloat(config.getConfigurableValue("GaussianPassiveResourceSigma", "0.5"));

            DENSE_ASTEROID_RESOURCE_CHANCE = Float.parseFloat(config.getConfigurableValue("DenseAsteroidOreFrequency","0.005"));
            STANDARD_ASTEROID_RESOURCE_CHANCE = Float.parseFloat(config.getConfigurableValue("StandardAsteroidOreFrequency","0.0025"));

            OFFTYPE_ASTEROID_CHANCE_MIN = Float.parseFloat(config.getConfigurableValue("OffTypeAsteroidChanceMin","0.01"));
            OFFTYPE_ASTEROID_CHANCE_MAX = Float.parseFloat(config.getConfigurableValue("OffTypeAsteroidChanceMax","0.075"));
            OFFTYPE_CHROMA_CHANCE = Float.parseFloat(config.getConfigurableValue("ChromaAsteroidFrequency","0.005f"));

            GAS_GIANT_MIN_RADIUS = Float.parseFloat(config.getConfigurableValue("GasGiantMinRadius","0.6"));
            GAS_GIANT_MAX_RADIUS = Float.parseFloat(config.getConfigurableValue("GasGiantMaxRadius","1.25f"));
            GAS_GIANT_MIN_ROTATION = Float.parseFloat(config.getConfigurableValue("GasGiantMinRotationRate","0.8"));
            GAS_GIANT_MAX_ROTATION = Float.parseFloat(config.getConfigurableValue("GasGiantMaxRotationRate","0.8"));

            GIANT_GRAVITY_FACTOR = Float.parseFloat(config.getConfigurableValue("GasGiantPhysicsGravityStrength","2.5"));
            FLOOR_REPEL_FORCE_FACTOR = Float.parseFloat(config.getConfigurableValue("GasGiantPhysicsFloorBuoyancyStrength","100.0"));
            DRAG_FORCE_MULTIPLIER = Float.parseFloat(config.getConfigurableValue("GasGiantPhysicsDragFactor","0.4"));

            RSC_ANBARIC_MIN_REGEN_RATE_SEC = Float.parseFloat(config.getConfigurableValue("PassiveAnbaricRegenRateMin","0.2"));
            RSC_ANBARIC_MAX_REGEN_RATE_SEC = Float.parseFloat(config.getConfigurableValue("PassiveAnbaricRegenRateMax", "1.5"));

            RSC_PARSYNE_MIN_REGEN_RATE_SEC = Float.parseFloat(config.getConfigurableValue("PassiveParsyneRegenRateMin", "0.5"));
            RSC_PARSYNE_MAX_REGEN_RATE_SEC = Float.parseFloat(config.getConfigurableValue("PassiveParsyneRegenRateMax", "5"));
            RSC_PARSYNE_PLANET_MOD = Float.parseFloat(config.getConfigurableValue("PassiveParsynePlanetModifier","0.05"));

            RSC_THERMYN_MIN_REGEN_RATE_SEC = Float.parseFloat(config.getConfigurableValue("PassiveThermynRegenRateMin","0.2"));
            RSC_THERMYN_MAX_REGEN_RATE_SEC = Float.parseFloat(config.getConfigurableValue("PassiveThermynRegenRateMax", "1.5"));

            EXTRACTION_RATE_PER_SECOND_PER_BLOCK = Float.parseFloat(config.getConfigurableValue("ExtractorBaseExtractionPower", "0.1"));
            PASSIVE_POOL_UPDATE_INTERVAL_MS = Integer.parseInt(config.getConfigurableValue("PassivePoolUpdateTickRateMS","5000"));
            PASSIVE_EXTRACTOR_STATE_SAVE_INTERVAL_MS = Integer.parseInt(config.getConfigurableValue("PassiveExtractorStateSaveIntervalMS","240000"));

            REFINING_MACETINE_BYPRODUCT_RATE = Float.parseFloat(config.getConfigurableValue("RefiningMacetineByproductChance","0.05"));
            COMPONENT_RECYCLE_FAIL_RATE = Float.parseFloat(config.getConfigurableValue("ComponentRecyclerFailRate","0.5"));

            ASTROMETRIC_SCANNER_CHARGE_TIME = Float.parseFloat(config.getConfigurableValue("RRSScannerBaseChargeTime","5"));
            ASTROMETRIC_SCANNER_CHARGE_POWER_PER_MASS = Float.parseFloat(config.getConfigurableValue("RRSScannerChargePowerReqPerMass", "5"));
        if(GameCommon.isOnSinglePlayer() || GameCommon.isDedicatedServer()){
            if (writeToFile) config.saveConfig();
        }
    }
    public static void main(String[] args) {
        //meh //muh
        new ResourcesReSourced().addModGlossarEntries();

    }

    private void addModGlossarEntries() {
        StringBuilder b = new StringBuilder("Galactic zones are marked on the map by colored circles, click on the circle to display the complete zone. Every zone type offers a single, unique resource that can be harvested through different means of Resource mining. There is at least one zone for every zone type available in the spawn galaxy." +
                "\n\nZone types:");

        String am = "Asteroid mining";
        String ggs = "Gas Giant siphon extraction";
        String pme = "Planet magmatic extraction";
        String sse = "Star siphon extraction";
        String[][] zones = new String[][]{
                {"Green","Core","All resources",am+", "+ggs+", "+pme,"","The Core zone type is unique. As the name suggests, only one Galactic Core exists in the centre of each galaxy. It contains small amounts of all resources available in RRS. Occasional Aegium comets, Ferron asteroids, and Thermyn-bearing terrestrial planets can be found here. There are also special Exotic Gas Giants, which may provide small amounts of Parsyne and Anbaric available via the Vapor Siphon."},
                {"White","Misty","Parsyne Plasma",ggs+", "+sse,"beam array systems","The stars and gas giant planets in Misty systems emit useful Parsyne Plasma. Place a Vapor Siphon on a station anywhere in the system to harvest from the star, or in/adjacent to a Parsyne gas giant sector to harvest from the gas giant."},
                {"Cyan","Frigid","Aegium Shards",am+", "+pme,"shielding and other energy field systems","In Frigid systems, most asteroids and planets are frozen over, except very close to the star. In the outermost belts, Aegium Comets can be found, containing valuable Aegium Shards. Some planets in these systems also provide a trickle of Aegium if a Magmatic Extractor is placed on them."},
                {"Blue","Energetic","Thermyn Amalgam",pme,"power and explosive systems","Energized Planets can be found In Energetic systems. Placing a Magmatic Extractor on an Energized Planet will yield volatile Thermyn Amalgam, used in power and explosives systems."},
                {"Magenta","Crystal rich","Crystalline ore",am,"base material","Crystalline systems don't contain any special resources, but their asteroid belts contain Dense Crystal asteroids, with large amounts of common Crystalline Ore to fuel your large building projects."},
                {"Rust","Ferric","Ferron Ore",am+", "+pme,"scanner and stealth","Ferron-rich star systems contain large amounts of Ferron asteroids in asteroid belts close to the star. As the name suggests, these asteroids contain Ferron Ore, used in remote sensing and stealth systems. Some planets in these systems also provide a trickle of Ferron if a Magmatic Extractor is placed on them."},
                {"Grey","Metallic","Metal Mesh",am,"most important base material","Metallic systems don't contain any special resources, but their asteroid belts contain Dense Metal asteroids, with large amounts of common Metallic Ore to fuel your large building projects."},
                {"Red","Flux","Anbaric Vapor",ggs,"FTL systems and other exotic technology","Flux systems contain special dark red gas giants. Placing a Vapor Siphon on a station in/adjacent to the gas giant sector will allow you to harvest Anbaric Vapor from these gas giants."},
                {"Unmarked","Empty Space","Common Metal and Crystal",am+", "+ggs+", "+pme,"","Systems not within a particular resource zone will contain mostly normal Crystalline Ore and Metallic Ore asteroids, and generic planet types. However, in ANY star system in any (or no) zone, there is a small chance for special asteroid or planet types to spawn."}
        };
        for (String[] arr: zones) {
            b.append("\n\n").append(getZoneEntry(arr[0],arr[1],arr[2],arr[3],arr[4],arr[5]));
        }
        b.append("\n" +
                "Each zone type contains unique resources used in manufacturing. Some of these are found in stars, planets, or asteroids. Consult the Astrometric Scanner chambers, available in the Recon category, for more information.\n" +
                "\n" +
                "There is no zone or naturally-occurring source of Macetine Aggregate. However, it is produced as a byproduct of refining any other special zone resource mentioned above (i.e. resources besides Metallic Ore, Crystalline Ore, and Macetine Aggregate itself)\n");

        System.out.println(b.toString());


       GlossarInit.initGlossar(this);
       GlossarCategory cat = new GlossarCategory("Regional Resources");
       cat.addEntry(new GlossarEntry("Introduction","Resources ReSourced (RRS) overhauls aspects of the universe, industrial resources, factories, and recipe chains, with the goal of creating scarcity, incentivicing trade, and driving content in StarMade via unequal resource distribution.\nmeow." +
               "\n" +
               "Simultaneously, it reduces the tedium and pitfalls involved in building ships via blueprints or shipyards, by introducing the Components system, block recycling, Omnichrome Paint, and a reduced set of distinctive resources."));

        cat.addEntry(new GlossarEntry("Resource Zones",b.toString()));

        cat.addEntry(new GlossarEntry("Astrometric Scanning", "The Astrometric Scanner system is a new Recon Chamber tree added by RRS. Its chambers provide various information about your current solar system and location.\n\n" +
                "Astrometric Scanner Base:\nProvides general information about your zone, system, and sector (if applicable), including zone type and resource density score.\n\n"+
                "Planetary Scanner: \nProvides detailed information about a planet while in the same sector.\n\n"+
                "Stellar Cartography System: \nAllows your Astrometric Scanner to uncover your current system on the map.\n\n"+
                "Sector Prospecting Scanner: \nProvides information about what passively-harvested resources are available within extraction range of your current sector."));

        cat.addEntry(new GlossarEntry("Resource Mining","Asteroid Mining:\nAsteroids are found in the yellow belts around a star and mined with a salvage beam. It consists of a computer and multiple beam blocks connected to the computer. It functions similar to a beam weapon, breaking down the blocks it hits. The blocks are turned into raw ore/resources and go into the pilots inventory or a slaved cargo.\n\n" +
                "Siphon station:\nSome resources can only be harvested from gas giants or stars. Place a station in a adjacent sector to the gas giant, and place a 'Vapor Siphon' on it. (for Parsyne stars, the siphon can be placed anywhere in space in the same solar system.) Set the resource extraction recipe and activate the siphon (similar to a factory), and it will start collecting vapour, outputting raw ore. The base extraction rate is 0.1 resource per second, but this can be increased by using factory enhancers.\n\n Keep in mind that every source for passive resource collection (planet, Parsyne star, etc) has a maximal production rate, after which further enhancers are not useful. Multiple siphons will split the outcome among them. A siphon also works in an unloaded sector, stopping when the cargo volume it was directly assigned is full.\n\n" +
                "Magmatic Extractor:\nSome resources require to be extracted from a terrestrial planet. Planets are found in purple belts around a star. To extract the ore, place a 'Magmatic Extractor' block directly on the surface. It functions similarly to a vapor siphon, extracting ore from the planet over time.\n\n" +
                "Factory Enhancers:\nJust as with Factory blocks, Extractor blocks can be connected to Factory Enhancers to amplify their harvesting capabilities. However, that sources for extraction can only yield a certain amount of material per second, regardless of the number of extractors or enhancer blocks attached to them.\n\n" +
                "NOTE: For technical and gameplay reasons, RRS does not add any types of ore to planet blocks. Instead, planet mining is strictly passive, via the Magmatic Extractor block."));

        cat.addEntry(new GlossarEntry("Crafting","Vanilla StarMade's complicated recipe trees have been removed in favor of a simple, straightforward system of Components and Blocks.\n\n" +
                "Blocks:\nBlocks are crafted inside a Block Assembler. The Assembler exclusively uses a collection of Components to construct the block, and produces output every 4 ticks. All blocks can be broken down into their constituent Components without loss in a Block Disassembler.\n\n" +
                "Components:\nComponents are produced in a Component Fabricator, exclusively using resource capsules. Components can be disassembled in a Component Recycler, though this sometimes fails and yields scrap instead. 18 different types of Components exist: 7 basic components crafted from standard crystal or metal, and 11 advanced components manufactured from special resources found in their respective zones.\n\n" +
                "Capsules:\nCapsules are gained by refining ore in a capsule refinery. Once refined, there is no way to gain back the raw ore from a capsule. There are 9 types of resource capsule. The 24 vanilla capsule types are craftable as decoration, but no longer useful in production.\n\n" +
                "Blueprints:\nBlueprints are filled directly with Components, not blocks. No more hours spent crafting every single block type for your ship or station in the correct amount - get out there and fly!\n\"" +
                "Shipyards:\nShipyards use Components for construction of ships, and produce Components upon deconstruction. Just like Blueprints, they do not require manual crafting of blocks. Once you have created a sufficient stock of Components to build your ship, the shipyard will take care of the rest."));


        GlossarInit.addCategory(cat);
    }

    private String getZoneEntry(String color, String name, String resources, String extraction,String usedIn,String additionalInfo) {
        String s = String.format("%s - %s\n\tResources Found:   %s\n\tExtraction Mode:   %s\n\tUsed In:   %s\n\tAdditional Information:\n%s",name,color,resources,extraction,usedIn,additionalInfo);
        return s.replace("\t","-");
    }
}
