package org.ithirahad.resourcesresourced.universe.gasplanet;

import org.ithirahad.resourcesresourced.industry.PassiveResourceSupplier;

import javax.vecmath.Color4f;
import javax.vecmath.Vector3f;

public class GasGiantSheet {
    public float size; //multiple of some default value - probably 0.5f * sector size (* conversion ratio with rendering)
    public PassiveResourceSupplier[] resourcePools;
    public Vector3f inSectorOffset;
    public Color4f darkColor;
    public Color4f brightColor;
    public float axialTiltX;
    public float axialTiltY;
    public float rotationRate; // per minute
    public static float BASE_ROTATION_RATE = 0.1f;
    public static float BASE_GIANT_SIZE = 0.35f; //model scale, relative to sector size
    public GasGiantType type;
    public int textureID = 0;

    public GasGiantSheet(GasGiantType type, float size, float rotationRate, float tiltx, float tilty, Vector3f offset, PassiveResourceSupplier[] resources, long seed, int textureID){
        this.inSectorOffset = offset;
        this.size = size;
        this.resourcePools = resources;
        this.darkColor = darkColor;
        this.brightColor = brightColor;
        this.axialTiltX = tiltx;
        this.axialTiltY = tilty;
        this.rotationRate = rotationRate;
        this.textureID = textureID;
    }

    public GasGiantSheet(GasGiantType type, float size, float rotationRate, float tiltx, float tilty, Vector3f offset, PassiveResourceSupplier[] resources, long seed){
        this.type = type;
        this.inSectorOffset = offset;
        this.size = size;
        this.resourcePools = resources;
        this.darkColor = type.getGasGiantColorDark(seed);
        this.brightColor = type.getGasGiantColorLight(seed);
        this.axialTiltX = tiltx;
        this.axialTiltY = tilty;
        this.rotationRate = rotationRate;
    }

    public GasGiantSheet(GasGiantType type, float size, float rotationRate, float tiltx, float tilty, PassiveResourceSupplier[] resources, long seed){
        this(type, size, rotationRate, tiltx, tilty, new Vector3f(0,0,0), resources, seed);
    }

    public GasGiantSheet(GasGiantType type, float size, float rotationRate, float tiltx, float tilty, PassiveResourceSupplier[] resources, long seed, int textureID){
        this(type, size, rotationRate, tiltx, tilty, new Vector3f(0,0,0), resources, seed);
        this.textureID = textureID;
    }

    public GasGiantSheet(GasGiantType type, float size, float tiltx, float tilty, PassiveResourceSupplier[] resources, long seed){
        this(type, size, 1f, tiltx, tilty, resources, seed);
    };

    public GasGiantSheet(GasGiantType type, float size, float tiltx, float tilty, PassiveResourceSupplier[] resources, long seed, int textureID){
        this(type, size, 1f, tiltx, tilty, resources, seed);
        this.textureID = textureID;
    };

    public GasGiantSheet(GasGiantType type, float size, PassiveResourceSupplier[] resources, long seed){
        this(type, size, 0f, 0f, resources, seed);
    };

    public GasGiantSheet(GasGiantType type, PassiveResourceSupplier[] resources, long seed){
        this(type, 1f, resources, seed);
    };

    //TODO: have a separate drawinfo mini class that can be provided upon request, rather than shipping the entire gasgiantsheet around for rendering?
}
