package org.ithirahad.resourcesresourced.universe.asteroid.rockfactory;

import org.schema.common.FastMath;
import org.schema.game.server.controller.world.factory.WorldCreatorFloatingRockFactory;
import org.schema.game.server.controller.world.factory.planet.structures.TerrainStructure;
import org.schema.game.server.controller.world.factory.planet.structures.TerrainStructureList;
import org.schema.game.server.controller.world.factory.terrain.GeneratorResourcePlugin;
import org.schema.game.server.controller.world.factory.terrain.TerrainDeco;

import java.util.Random;

import static org.ithirahad.resourcesresourced.RRSConfiguration.DENSE_ASTEROID_RESOURCE_CHANCE;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.oreEntries;

public class DenseMetalRockFactory extends WorldCreatorFloatingRockFactory {

    private final short CRUST_MATERIAL_TYPE = 203; //tekt
    private final short CORE_MATERIAL_TYPE = 139; //red planet rock
    private final short SCATTER_MATERIAL_TYPE = 151; //chabaz

    private final int CRUST_INDEX;
    private final int CORE_INDEX;
    private final int SCATTER_INDEX;

    private final short ORE_TYPE;

    public DenseMetalRockFactory(long l) {
        super(l);
        CRUST_INDEX = registerBlock(CRUST_MATERIAL_TYPE);
        CORE_INDEX = registerBlock(CORE_MATERIAL_TYPE);
        SCATTER_INDEX = registerBlock(SCATTER_MATERIAL_TYPE);
        ORE_TYPE = oreEntries.get("Metallic Ore").left.id;
    }

    @Override
    protected void terrainStructurePlacement(byte x, byte y, byte z, float v, TerrainStructureList terrainStructureList, Random random) {
        if (FastMath.rand.nextFloat() <= (DENSE_ASTEROID_RESOURCE_CHANCE))
            terrainStructureList.add(x, y, z, TerrainStructure.Type.ResourceBlob, ORE_TYPE, CORE_MATERIAL_TYPE, defaultResourceSize);
        if (FastMath.rand.nextFloat() <= (DENSE_ASTEROID_RESOURCE_CHANCE))
            terrainStructureList.add(x, y, z, TerrainStructure.Type.ResourceBlob, ORE_TYPE,CRUST_MATERIAL_TYPE, defaultResourceSize);
    }

    @Override
    protected int getRandomSolidType(float v, Random random) {
        if(random.nextFloat() < 0.025) return SCATTER_INDEX;
        return v < 0.07F ? CRUST_INDEX : CORE_INDEX;
    }

    @Override
    public void setMinable(Random random) {
        this.minable = new TerrainDeco[2];
        minable[0] = new GeneratorResourcePlugin(18,elementEntries.get("Metallic Ore").id, CRUST_MATERIAL_TYPE);
        minable[1] = new GeneratorResourcePlugin(18,elementEntries.get("Metallic Ore").id, CORE_MATERIAL_TYPE);
    }
}
