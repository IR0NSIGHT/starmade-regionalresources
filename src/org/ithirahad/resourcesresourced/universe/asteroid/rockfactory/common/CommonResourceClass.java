package org.ithirahad.resourcesresourced.universe.asteroid.rockfactory.common;

public enum CommonResourceClass {
    METAL,
    CRYSTAL
}
