package org.ithirahad.resourcesresourced.universe.asteroid.rockfactory.common;

import org.schema.game.common.data.element.ElementKeyMap;

public class CommonRockStoneFactory extends CommonTypeFactory {
    public CommonRockStoneFactory(long seed, CommonResourceClass type) {
        super(seed, type, CRUST_TERRAN, ElementKeyMap.TERRAIN_ROCK_YELLOW, ElementKeyMap.TERRAIN_ROCK_GREEN);
    }
}