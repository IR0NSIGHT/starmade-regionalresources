package org.ithirahad.resourcesresourced.universe.asteroid.rockfactory.common;

import org.schema.game.common.data.element.ElementKeyMap;

import java.util.Random;

public class CommonRockLavaFactory extends CommonTypeFactory {
    public final int TERRAIN_LAVA_INDEX;
    public CommonRockLavaFactory(long seed, CommonResourceClass type) {
        super(seed, type, CRUST_MARTIAN, ElementKeyMap.TERRAIN_ROCK_RED, ElementKeyMap.TERRAIN_ROCK_ORANGE, ElementKeyMap.TERRAIN_ROCK_BLACK);
        TERRAIN_LAVA_INDEX = registerBlock(ElementKeyMap.TERRAIN_LAVA_ID);
    }

    @Override
    protected int getRandomSolidType(float v, Random random) {
        return v < 0.07F ? (rand.nextBoolean() ? CORE_MATERIAL_INDEX : TERRAIN_LAVA_INDEX) : CRUST_MATERIAL_INDEX;
    }
}