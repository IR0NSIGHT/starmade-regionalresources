package org.ithirahad.resourcesresourced.universe.asteroid.rockfactory;

import org.schema.common.FastMath;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.server.controller.world.factory.WorldCreatorFloatingRockFactory;
import org.schema.game.server.controller.world.factory.planet.structures.TerrainStructure;
import org.schema.game.server.controller.world.factory.planet.structures.TerrainStructureList;
import org.schema.game.server.controller.world.factory.terrain.GeneratorResourcePlugin;
import org.schema.game.server.controller.world.factory.terrain.TerrainDeco;

import java.util.Random;

import static org.ithirahad.resourcesresourced.RRSConfiguration.DENSE_ASTEROID_RESOURCE_CHANCE;
import static org.ithirahad.resourcesresourced.RRSConfiguration.STANDARD_ASTEROID_RESOURCE_CHANCE;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.oreEntries;

public class ChromaRockFactory extends WorldCreatorFloatingRockFactory {
    private static final int[] crystalIDs = {
            286, //ice crystal
            452, //purple crystal
            455, //yellow crystal?
            456, //red crystal
            457, //orange crystal
            458, //green crystal
            459, //blue crystal
    };

    private final int[] crystalIndices;

    private final short PRIMARY_CRYSTAL_TYPE = 453; //nocx crystal
    private final short CRUST_MATERIAL_TYPE = 139; //blue rock
    private final short INNER_CORE_MATERIAL_TYPE = ElementKeyMap.TERRAIN_ROCK_PURPLE;

    private final int PRIMARY_CRYSTAL_INDEX;
    private final int CRUST_MATERIAL_INDEX;
    private final int INNER_CORE_MATERIAL_INDEX;

    private final Random rand;
    private final short CRYSTAL_ORE_ELEMENT_TYPE;
    private final short METAL_ORE_ELEMENT_TYPE;
    private final short FERRON_ORE_ELEMENT_TYPE;
    private final short AEGIUM_ORE_ELEMENT_TYPE;

    public ChromaRockFactory(long seed) {
        super(seed);
        rand = new Random(seed);
        CRYSTAL_ORE_ELEMENT_TYPE = oreEntries.get("Crystalline Ore").left.id;
        METAL_ORE_ELEMENT_TYPE = oreEntries.get("Metallic Ore").left.id;
        FERRON_ORE_ELEMENT_TYPE = oreEntries.get("Ferron Ore").left.id;
        AEGIUM_ORE_ELEMENT_TYPE = oreEntries.get("Aegium Ore").left.id;

        crystalIndices = new int[crystalIDs.length];
        for(int i = 0; i < crystalIDs.length; i++) crystalIndices[i] = registerBlock((short) crystalIDs[i]);
        PRIMARY_CRYSTAL_INDEX = registerBlock(PRIMARY_CRYSTAL_TYPE);
        CRUST_MATERIAL_INDEX = registerBlock(CRUST_MATERIAL_TYPE);
        INNER_CORE_MATERIAL_INDEX = registerBlock(INNER_CORE_MATERIAL_TYPE);
    }

    private short nextCrystal(){
        return (short) crystalIDs[rand.nextInt(crystalIDs.length)];
    }

    private int nextCrystalIndex(){
        return crystalIndices[rand.nextInt(crystalIndices.length)];
    }

    @Override
    protected void terrainStructurePlacement(byte x, byte y, byte z, float v, TerrainStructureList terrainStructureList, Random random) {
        if (FastMath.rand.nextFloat() <= (DENSE_ASTEROID_RESOURCE_CHANCE))
            terrainStructureList.add(x, y, z, TerrainStructure.Type.ResourceBlob, CRYSTAL_ORE_ELEMENT_TYPE, INNER_CORE_MATERIAL_TYPE, defaultResourceSize);
        if (FastMath.rand.nextFloat() <= (DENSE_ASTEROID_RESOURCE_CHANCE))
            terrainStructureList.add(x, y, z, TerrainStructure.Type.ResourceBlob, CRYSTAL_ORE_ELEMENT_TYPE, CRUST_MATERIAL_TYPE, defaultResourceSize);

        if (FastMath.rand.nextFloat() <= (DENSE_ASTEROID_RESOURCE_CHANCE))
            terrainStructureList.add(x, y, z, TerrainStructure.Type.ResourceBlob, METAL_ORE_ELEMENT_TYPE, INNER_CORE_MATERIAL_TYPE, defaultResourceSize);
        if (FastMath.rand.nextFloat() <= (DENSE_ASTEROID_RESOURCE_CHANCE))
            terrainStructureList.add(x, y, z, TerrainStructure.Type.ResourceBlob, METAL_ORE_ELEMENT_TYPE, CRUST_MATERIAL_TYPE, defaultResourceSize);

        if (FastMath.rand.nextFloat() <= (DENSE_ASTEROID_RESOURCE_CHANCE))
            terrainStructureList.add(x, y, z, TerrainStructure.Type.ResourceBlob, FERRON_ORE_ELEMENT_TYPE, CRUST_MATERIAL_TYPE, defaultResourceSize);

        if (FastMath.rand.nextFloat() <= (DENSE_ASTEROID_RESOURCE_CHANCE))
            terrainStructureList.add(x, y, z, TerrainStructure.Type.ResourceBlob, AEGIUM_ORE_ELEMENT_TYPE, CRUST_MATERIAL_TYPE, defaultResourceSize);

        if (FastMath.rand.nextFloat() <= (STANDARD_ASTEROID_RESOURCE_CHANCE))
            terrainStructureList.add(x, y, z, TerrainStructure.Type.Rock, nextCrystal(), TerrainStructure.toHalfFloat(1), (short) 0);
    }

    @Override
    protected int getRandomSolidType(float v, Random random) {
        short type;
        if (v < 0.07F) type = (short) (rand.nextFloat() > 0.98f ? nextCrystalIndex() : CRUST_MATERIAL_INDEX);
        else if (v < 0.1f) type = (short) nextCrystalIndex();
        else if (v < 0.25f) type = (short) PRIMARY_CRYSTAL_INDEX; //outer core is made of solid nocx
        else type = (short) INNER_CORE_MATERIAL_INDEX;

        return type;
    }

    @Override
    public void setMinable(Random random) {
        this.minable = new TerrainDeco[6];
        this.minable[0] = new GeneratorResourcePlugin(4 , CRYSTAL_ORE_ELEMENT_TYPE, CRUST_MATERIAL_TYPE);
        this.minable[1] = new GeneratorResourcePlugin(4 , CRYSTAL_ORE_ELEMENT_TYPE, INNER_CORE_MATERIAL_TYPE);
        this.minable[2] = new GeneratorResourcePlugin(4 , METAL_ORE_ELEMENT_TYPE, CRUST_MATERIAL_TYPE);
        this.minable[3] = new GeneratorResourcePlugin(4 , METAL_ORE_ELEMENT_TYPE, INNER_CORE_MATERIAL_TYPE);
        this.minable[4] = new GeneratorResourcePlugin(4 , FERRON_ORE_ELEMENT_TYPE, CRUST_MATERIAL_TYPE);
        this.minable[5] = new GeneratorResourcePlugin(4 , AEGIUM_ORE_ELEMENT_TYPE, CRUST_MATERIAL_TYPE);
    }
}