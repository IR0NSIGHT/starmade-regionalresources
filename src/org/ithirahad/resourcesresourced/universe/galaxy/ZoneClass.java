package org.ithirahad.resourcesresourced.universe.galaxy;

import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

import javax.vecmath.Vector4f;

import static org.ithirahad.resourcesresourced.RRSConfiguration.MAP_ZONE_COLOUR_ALPHA;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.bastionInitiativeIsPresentAndActive;
import static org.ithirahad.resourcesresourced.universe.galaxy.ZoneClass.PlacementMode.*;

public enum ZoneClass {
    NONE("nondescript"),

    //RR PATHFINDER TYPES
    PF_PYROCLASTIC("scalding"), //black 90%, red 10%
    PF_COMETARY("icy"), //white 90%, blue 10%
    PF_EXOTIC("energetic"), //purple 90%, red dirt (yellow) 10%
    PF_FLUX("plasmic flux-filled"), //blue 90%, green 510%
    PF_DUSTY("dusty"), //red dirt (yellow) 90%, black 10%
    PF_TEMPERATE("temperate"), //green 90%, orange 10%
    PF_FRIGID("frigid"), //orange 90%, white 10%
    PF_RUDDY("rusty"), //red 90%, red dirt (yellow) 5%, purple 5%

    //REGIONAL RESOURCES (FULL)
    RR_MISTY("misty", MID, 1.0f, 1.0f), //Allows parsyne star harvesting
    RR_FRIGID("frigid", MID, 0.9f, 1.0f), //Allows sertise ore spawns
    RR_FERRIC("ferron-rich", INNER, 1.0f, 1.0f), //Allows ferron ore spawns
    RR_ENERGETIC("energetic", MID, 1.0f, 1.0f), //Allows thermyn planet spawns
    RR_ANBARIC("anbaric flux-filled", INNER, 0.7f, 1.0f), //Allows anbaric gas giant spawns
    RR_METAL_DENSE("metal-rich", OUTER, 1.75f, 1), //lots of common metal
    RR_CRYSTAL_DENSE("crystal-rich", OUTER, 1.25f, 1), //lots of common crystal
    RR_PARAMETRIC("unusual"), //random params
    RR_CORE("central", SPECIAL, 1.0f, -1), //middle of galaxy
    BA_REMNANT("ancient remnants", MID, 0.5f, 2), //Bastion Initiative
    RR_EXTRADIMENSIONAL("dimensionally-displaced"), //Reserved :DD
    UNKNOWN("unknown"); //not yet scanned on client TODO: FoW

    /**
     * Adjective or brief description of the zone classification.
     */
    private final String descriptor;
    /**
     * How zones of this type should be placed.
     */
    private final PlacementMode placement;
    /**
     * Modifier on the size of zones of this class.
     */
    private final float radiusMultiplier;
    /**
     * Modifier on the number of zones of this type that will generate.
     */
    private final float frequencyMultiplier;
    //TODO: config

    ZoneClass(String descriptor, PlacementMode placementMode, float radius, float frequencyMultiplier) {
        this.descriptor = descriptor;
        this.placement = placementMode;
        this.radiusMultiplier = radius;
        this.frequencyMultiplier = frequencyMultiplier;
    }

    ZoneClass(String descriptor) {
        this.descriptor = descriptor;
        this.placement = ANY;
        this.radiusMultiplier = 1.0f;
        this.frequencyMultiplier = 1.0f;
    }

    public String getDescriptor() {
        return descriptor;
    }

    public String[] adjective() { //zone descriptions
        switch (this) {
            case RR_CORE:
                return new String[]{"Center", "Central", "Pinwheel", "Whirlpool", "Maelstrom", "Middle", "Convergent"}; //unused
            case RR_EXTRADIMENSIONAL:
                return new String[]{"Enigmatic", "Paradox", "Ineffable", "Anathema"};
            case RR_PARAMETRIC:
                return new String[]{"Uncharted", "Extraordinary"};
            case RR_ANBARIC:
                return new String[]{"Fluctuating", "Multispatial", "Vaporous", "Churning", "Red", "Crimson", "Ruddy", "Sanguine"};
            case RR_MISTY:
                return new String[]{"Cloud", "Misting", "Plasmic", "Wisp", "Filament", "Roiling", "Smoky", "Ivory", "Pearl"};
            case RR_METAL_DENSE:
                return new String[]{"Dense", "Gravitic", "Cornucopian", "Abundance", "Providence", "Motherlode", "Lodestone", "Bountiful", "Alloy", "Tailing" /*will produce literally cubic kilometers of tailings, so why not*/};
            case RR_ENERGETIC:
                return new String[]{"Energetic", "Quantum", "Resonant", "Argon", "Xenon", "Cerulean", "Blueshift", "Galvanic", "Fulminous", "Crackling", "Thunder", "Thunderous"};
            case RR_FRIGID:
                return new String[]{"Frigid", "Sub-Zero", "Cryos", "Cryogen", "Shivering", "Frosted", "Winter's", "Frostbite", "Permafrost", "Polaric", "Aegis", "Shield", "Warding"};
            case RR_CRYSTAL_DENSE:
                return new String[]{"Glittering", "Sparkling", "Glimmering", "Gleaming", "Shattered", "Broken" /*hopefully not :D*/, "Cabochon", "Brilliant", "Gemstone", "Sapphire", "Tourmaline", "Fluorite", "Pyritic", "Carnelian", "Topaz", "Amethyst", "Celestite"};
            case RR_FERRIC:
                return new String[]{"Metallic", "Rust", "Ferrous", "Iron", "Steel","Orichalcum","Mithril","Titanian","Bronze"};
            case BA_REMNANT:
                return new String[]{"Shattered","Ruined","Broken","Lost","Fallen","Carcass","Doomed","Faded","Trace","Archaean"};
            case NONE:
                return new String[]{"Nondescript"}; //probably never used
            case UNKNOWN:
                return new String[]{"[<BUG> DESCRIPTION UNKNOWN]"};
            case PF_PYROCLASTIC:
                return new String[]{"Scalding"};
            case PF_COMETARY:
                return new String[]{"Cryogenic"};
            case PF_EXOTIC:
                return new String[]{"Energetic"};
            case PF_FLUX:
                return new String[]{"Flux-ridden"};
            case PF_DUSTY:
                return new String[]{"Dusty"};
            case PF_TEMPERATE:
                return new String[]{"Temperate"};
            case PF_FRIGID:
                return new String[]{"Frigid"};
            case PF_RUDDY:
                return new String[]{"Rusted"};
            default:
                return new String[]{"Barren", "Bleak", "Fruitless"};
        }
    }

    public String description() {
        switch (this) {
            case RR_CORE:
                return "This region comprises the heart of the galaxy. Here, at the placid eye of the storm, begins the journey of many enterprising spacefarers.\r\nIn this region, advanced manufacturing resources are relatively scarce. However, there is a tenuous swirl of parsyne vapor coursing through space, and one can occasionally find a strange rogue planet or wayward asteroid washed out of the chaos of the surrounding galaxy, bearing exotic materials.";
            //------------------------------------------------------------------------
            case PF_PYROCLASTIC:
                return "This region contains many hot, chaotic star systems full of high-temperature asteroids and planets, formed by frequent collisions and strong heat radiation.\r\nAs a result, it is rich in resources that only form in extreme heat.";
            case PF_COMETARY:
                return "In this frigid expanse of space, an unknown phenomenon is lowering the heat output of the stars.\r\nIn this region, one can find an abundance of resources that only condense in the extreme cold.";
            case PF_EXOTIC:
                return "This region contains large amounts of unusual forms of matter.\r\nMany of its asteroids are composed of strange purple stone, rich with precious metals and unique crystals.";
            case PF_FLUX:
                return "This region's asteroid belts course with a subtle energy flux.\r\nThe asteroids are a strange shade of blue, and contain energized ores useful in the creation of energy shield modules.";
            case PF_DUSTY:
                return "This area of space once contained unusual amounts of free-floating dust, sand, and rock.\r\nThis suspended material has condensed into dusty red asteroids around many of the stars here.";
            case PF_TEMPERATE:
                return "This region's stars are extremely stable and unusually inactive, allowing the formation of large amounts of minerals that would normally be mostly destroyed in the harsh environment of space.\r\nAsteroids with a curious green hue can be found here in multitudes.";
            case PF_FRIGID:
                return "The asteroids in this region seem to have formed very far from their stars, but contain relatively low amounts of ice.";
            case PF_RUDDY:
                return "The asteroids in this region appear to bear large amounts of various metal oxides.";
            //-------------------------------------------------------------------------
            case RR_MISTY:
                return "This region of space is filled with trace amounts of a mysterious substance with energy-refracting properties.\r\nThe substance appears to be emitted by almost every star in the area.";
            case RR_FRIGID:
                return "In this frigid expanse, an unknown phenomenon is lowering the heat output of the stars.\r\nThere is an abundance of icy asteroids and minerals that can only form in this cold environment.";
            case RR_FERRIC:
                return "This region of space once contained a large metal-rich nebula. As a result, certain asteroids here contain rare heavy metal ores.";
            case RR_ENERGETIC:
                return "This area of the galaxy contains faint, drifting exotic matter clouds.\r\nCertain planets here somehow collect this exotic matter over time, concentrating it beneath their surface as a bizarre energetic substance that the Trading Guild designates as \"Thermyn\".";
            case RR_ANBARIC:
                return "At some point in the distant past, this area experienced a surge of hyperspatial particles.\r\nAs a result, some gas giant planets in this region of space churn with a mysterious reddish vapor that seems to defy the conventional laws of physics.";
            case RR_METAL_DENSE:
                return "This region's stars are characterized by an unusually high metallicity.\r\nAccordingly, planets and asteroids found here are very rich in common metal and crystal ores.";
            case RR_CRYSTAL_DENSE:
                return "While otherwise unremarkable, this region's glittering asteroid fields and planets contain exceptional amounts of common crystals.";
            case RR_PARAMETRIC:
                return "The characteristics of this region of space appear to defy current scientific models.\r\nWho knows what interesting materials and strange new worlds may be found here...";
            case BA_REMNANT:
                return "While sensor readings are unclear, this region appears to be the vast graveyard of a galaxy-spanning, advanced empire. \r\nThe very material of the asteroids and planets here seems to have been transfigured by advanced picotechnology beyond our understanding.\r\n While the civilization is long gone, whatever energy or substance allowed its traces to persist for so long could be extremely useful to a modern-day star empire.";
            case RR_EXTRADIMENSIONAL:
                return "This area contains seemingly impossible forms of matter. Likely circumstances of formation: <<<UNABLE TO CALCULATE... UNABLE T 01001110 01001111 00100000 01001110 01001111 00100000 01001110 01001111 00100000 01001110 01001111 00100000 01001110 01001111 00100000 01010000 01001100 01000101 01000001 01010011 01000101 01010100 01000101 01010010 01001101 01001001 01001110 01000001 01010100 01000101>>>";
            case UNKNOWN:
                return "This unexplored zone remains a mystery to you. No information is currently available.\r\nUse your Astrometric Scanner in the region to learn more...";
            default:
                return "With an ordinary chemical composition and poor resource density, this region of space appears to be largely uninteresting.";
        }
    }

    public Character classLetter() {
        switch (this) {
            case PF_PYROCLASTIC:
                return 'H'; //hot
            case PF_COMETARY:
                return 'C'; //cold
            case PF_DUSTY:
                return 'D'; //desert
            case PF_EXOTIC:
                return 'X'; //Xotic :P
            case PF_FLUX:
                return 'Z'; //more xtreme kool letterz idk
            case PF_FRIGID:
                return 'K'; //idk
            case PF_RUDDY:
                return 'F'; //ferrous
            case PF_TEMPERATE:
                return 'G'; //green/Goldilocks
            //-----------------------------------------------------------
            case RR_CORE:
                return 'G'; //Galactic core
            case RR_METAL_DENSE:
                return 'M'; //Metal
            case RR_CRYSTAL_DENSE:
                return 'C'; //Crystal
            case RR_MISTY:
                return 'P'; //Parsyne
            case RR_FRIGID:
                return 'S'; //Subzero/Shields (Aegium)
            case RR_FERRIC:
                return 'F'; //Ferron
            case RR_ANBARIC:
                return 'A'; //Anbaric
            case RR_ENERGETIC:
                return 'T'; //Thermyn
            case BA_REMNANT:
                return 'X'; //ayylmao
            case RR_PARAMETRIC:
                return 'U'; //Unknown
            case RR_EXTRADIMENSIONAL:
                return '?';
            default:
                return 'N'; //Not Applicable
        }
    }

    public ZoneColorInformation colors() { //TODO: tune some of these
        switch (this) {
            case RR_METAL_DENSE:
                return new ZoneColorInformation(false, 0.3f, 0.75f, 0.1f, 0.6f, 0f, 0.3f, 0.1f, 0.66f);
            case RR_CRYSTAL_DENSE:
                return new ZoneColorInformation(false, 0.5f, 0.9f, 0.05f, 0.35f, 0.5f, 0.9f, 0.2f, 1.0f);
            case RR_ENERGETIC:
                return new ZoneColorInformation(false, 0f, 0.35f, 0f, 0.35f, 0.5f, 1f, 0.3f, 1f);
            case RR_FERRIC:
                return new ZoneColorInformation(false, 0.5f, 1f, 0.1f, 0.5f, 0f, 0.2f, 0.05f, 1f);
            case RR_MISTY:
                return new ZoneColorInformation(true, 0.45f, 0.74f, 0.45f, 0.9f, 0.5f, 0.8f, 0.3f, 0.95f); //entire shader seems a bit red-biased, and we're going for neutral...
            case RR_FRIGID:
                return new ZoneColorInformation(false, 0.2f, 0.4f, 0.4f, 0.75f, 0.75f, 1f, 0.2f, 1f);
            case RR_ANBARIC:
                return new ZoneColorInformation(false, 0.75f, 1.25f, 0.1f, 0.4f, 0f, 0.2f, 0.3f, 1f);
            case RR_CORE:
                return new ZoneColorInformation(false, 0f, 0.7f, 0f, 0.6f, 0f, 0.7f, 0f, 1f);
            case BA_REMNANT:
                return new ZoneColorInformation(false, 0.05f,0.15f,0.6f,0.9f,0.05f,0.4f,0.1f,0.8f);
            case UNKNOWN:
                //this is an error situation
            default:
                return new ZoneColorInformation(false, 0, 1, 0, 1, 0, 1, 0, 1);
        }
    }

    public Vector4f ZoneMapColor(){
        switch(this)
        {
            case RR_MISTY:
                return new Vector4f(1f, 1f, 1f, MAP_ZONE_COLOUR_ALPHA);
            case RR_FRIGID:
                return new Vector4f(0.3f, 0.9f, 1f, MAP_ZONE_COLOUR_ALPHA);
            case RR_FERRIC:
                return new Vector4f(1.0f, 0.5f, 0.1f, MAP_ZONE_COLOUR_ALPHA);
            case RR_ENERGETIC:
                return new Vector4f(0.2f, 0.2f, 1f, MAP_ZONE_COLOUR_ALPHA);
            case RR_ANBARIC:
                return new Vector4f(1f, 0.1f, 0.1f, MAP_ZONE_COLOUR_ALPHA);
            case RR_METAL_DENSE:
                return new Vector4f(0.401f, 0.4f, 0.46f, MAP_ZONE_COLOUR_ALPHA);
            case RR_CRYSTAL_DENSE:
                return new Vector4f(1f, 0f, 1f, MAP_ZONE_COLOUR_ALPHA);
            case RR_PARAMETRIC:
                return new Vector4f(0.5f, 0.75f, 1f, MAP_ZONE_COLOUR_ALPHA);
            case RR_CORE:
                return new Vector4f(0.4f, 1f, 0.55f, MAP_ZONE_COLOUR_ALPHA);
            case RR_EXTRADIMENSIONAL:
                return new Vector4f(0.6f, 0.2f, 0.6f, MAP_ZONE_COLOUR_ALPHA);
            case BA_REMNANT:
                return new Vector4f(0.13f, 0.9f, 0.35f, MAP_ZONE_COLOUR_ALPHA);
            case UNKNOWN:
                return new Vector4f(0.6f,0.6f,0.6f,MAP_ZONE_COLOUR_ALPHA);
            default:
                return new Vector4f(1,1,1, MAP_ZONE_COLOUR_ALPHA);
        }
    }

    public ElementInformation[] principalResources(){
        String[] s = {};
        switch(this)
        {
            case NONE:
            case RR_PARAMETRIC:
            case RR_CORE:
            case RR_EXTRADIMENSIONAL:
                s = new String[]{};
            case UNKNOWN:
                break;
            case RR_MISTY:
                s = new String[]{"Parsyne Plasma", "Macetine Aggregate"};
                break;
            case RR_FRIGID:
                s = new String[]{"Aegium Ore", "Macetine Aggregate"};
                break;
            case RR_FERRIC:
                s = new String[]{"Ferron Ore", "Macetine Aggregate"};
                break;
            case RR_ENERGETIC:
                s = new String[]{"Thermyn Amalgam", "Macetine Aggregate"};
                break;
            case RR_ANBARIC:
                s = new String[]{"Anbaric Vapor", "Macetine Aggregate"};
                break;
            case RR_METAL_DENSE:
                s = new String[]{"Metallic Ore"};
                break;
            case RR_CRYSTAL_DENSE:
                s = new String[]{"Crystalline Ore"};
                break;
            case BA_REMNANT:
                s = new String[]{"Castellium Condensate"};
        }
        ElementInformation[] result = new ElementInformation[s.length];
        for(int i = 0; i < s.length; i++){
            result[i] = elementEntries.get(s[i]);
        }
        return result;
    }

    public ElementInformation[] principalRefinedResource(){
        String[] s = {};
        switch(this)
        {
            case NONE:
            case RR_PARAMETRIC:
            case RR_CORE:
            case RR_EXTRADIMENSIONAL:
                s = new String[]{};
            case UNKNOWN:
                break;
            case RR_MISTY:
                s = new String[]{"Parsyne Capsule"};
                break;
            case RR_FRIGID:
                s = new String[]{"Aegium Capsule"};
                break;
            case RR_FERRIC:
                s = new String[]{"Ferron Capsule"};
                break;
            case RR_ENERGETIC:
                s = new String[]{"Thermyn Capsule"};
                break;
            case RR_ANBARIC:
                s = new String[]{"Anbaric Capsule"};
                break;
            case RR_METAL_DENSE:
                return new ElementInformation[]{ElementKeyMap.getInfo(ElementKeyMap.METAL_MESH)};
            case RR_CRYSTAL_DENSE:
                return new ElementInformation[]{ElementKeyMap.getInfo(ElementKeyMap.CRYSTAL_CRIRCUITS)};
            case BA_REMNANT:
                s = new String[]{"Castellium Condensate"};
        }
        ElementInformation[] result = new ElementInformation[s.length];
        for(int i = 0; i < s.length; i++){
            result[i] = elementEntries.get(s[i]);
        }
        return result;
    }

    public static ZoneClass[] pathfinderClasses(){
        return new ZoneClass[]{PF_COMETARY, PF_DUSTY, PF_EXOTIC, PF_FLUX, PF_FRIGID, PF_PYROCLASTIC, PF_RUDDY, PF_TEMPERATE};
    }

    public static ZoneClass[] RRClasses(){
        if(bastionInitiativeIsPresentAndActive) return new ZoneClass[]{RR_METAL_DENSE, RR_ENERGETIC, RR_ANBARIC, RR_FRIGID, RR_CRYSTAL_DENSE, RR_FERRIC, RR_MISTY, BA_REMNANT};
        else return new ZoneClass[]{RR_METAL_DENSE, RR_ENERGETIC, RR_ANBARIC, RR_FRIGID, RR_CRYSTAL_DENSE, RR_FERRIC, RR_MISTY};
        //Maybe just return all with matching abbreviations tbh. Extradimensional, if used, wouldn't need to have the abbr. header anyway.
    }

    public PlacementMode getPlacement() {
        return placement;
    }

    public float getRadiusMultiplier() {
        return radiusMultiplier;
    }

    public float getFrequencyMultiplier() {
        return frequencyMultiplier;
    }

    public enum PlacementMode{
        INNER,
        MID,
        OUTER,
        ANY,
        SPECIAL
    }
}
