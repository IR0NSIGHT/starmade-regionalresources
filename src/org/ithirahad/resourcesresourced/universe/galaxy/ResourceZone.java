package org.ithirahad.resourcesresourced.universe.galaxy;

import org.ithirahad.resourcesresourced.ResourcesReSourced;
import org.ithirahad.resourcesresourced.industry.PassiveResourceSupplier;
import org.ithirahad.resourcesresourced.industry.ResourceSourceType;
import org.ithirahad.resourcesresourced.RRUtils.SpaceGridMap;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemClass;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemSheet;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.server.data.Galaxy;
import org.schema.game.server.data.GameServerState;

import javax.validation.constraints.NotNull;
import java.util.*;

import static org.ithirahad.resourcesresourced.RRUtils.MiscUtils.lerp;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.*;
import static org.ithirahad.resourcesresourced.RRSConfiguration.*;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.ithirahad.resourcesresourced.listeners.BlockRemovalLogic.getStarResourceSector;
import static org.ithirahad.resourcesresourced.universe.starsystem.SystemClass.*;

public class ResourceZone {
    public final String name;
    public final long seed;
    public ZoneClass zoneClass;
    private final SpaceGridMap<SystemSheet> systemDictionary; //TODO: make this PRIVATE once done debugging
    private final LinkedList<Vector3i> stars = new LinkedList<>();

    public Vector3i getCenterSystem() {
        return stars.getFirst();
    }
    /**
     * returns a collection of all stars contained in this zone/belonging to zone
     * @return list
     */
    public LinkedList<Vector3i> getStars() {
        return stars;
    }
    private final Random rng;
    public final Galaxy galaxy;
    public boolean mapLinesGenerated = false;

    public ResourceZone(Galaxy galaxy, long seed, ZoneClass type){
        this.rng = new Random(seed);
        this.seed = seed;
        this.galaxy = galaxy;
        this.name = (new ZoneNamer()).generateZoneName(galaxy.galaxyPos, type, seed);
        zoneClass = type;
        systemDictionary = new SpaceGridMap<>(/*GridMapScale.GALAXY, false*/);
    }

    public ResourceZone(Galaxy galaxy, String name, ZoneClass type, Collection<Vector3i> systemList){
        this.seed = ResourcesReSourced.seeds.get(galaxy.galaxyPos);
        this.rng = new Random(seed);
        this.name = name;
        this.galaxy = galaxy;
        zoneClass = type;
        systemDictionary = new SpaceGridMap<>(/*GridMapScale.GALAXY, false*/);
        System.err.println("[MOD][Resources ReSourced] Bulk adding " + systemList.size() + " stars to the " + this.name + "!");
        for(Vector3i star : systemList){
            //System.err.println("[MOD][Resources ReSourced][WARNING] Adding star " + star.toString() + " to " + zoneName + "!");
            add(star);
        }
        System.err.println("[MOD][Resources ReSourced] Finished adding stars. Zone now contains " + systemDictionary.count() + " stars.");
    }

    public void add(Vector3i system){
        boolean overwrite = false;
        if(system == null){
            System.err.println("[MOD][Resources ReSourced][ERROR] Attempting to add a null Vector3i to " + name + "!");
            System.exit(-1); //(...yes, it's that bad)
        }

        overwrite = systemDictionary.containsLocation(system);
        int debugPreSize = systemDictionary.count();
        Vector3i systemCornerOrigin = new Vector3i(system);
        systemCornerOrigin.add(64,64,64);
        boolean isVoid = galaxy.isVoid(systemCornerOrigin);
        SystemSheet sheet = generateSystemSheet(system, isVoid); //was and should be inline; made it separate for debug purposes
        systemDictionary.put(system,sheet);

        if(isVoid) {
            //placeholder
        }
        else {
            SystemClass cls = systemDictionary.get(system).systemClass;
            if (cls == RR_MISTY) {
                Random lrand = new Random(galaxy.getSeed() + system.hashCode());
                Vector3i sector = getStarResourceSector(system);
                float sourceRate = lerp(RSC_PARSYNE_MIN_REGEN_RATE_SEC, RSC_PARSYNE_MAX_REGEN_RATE_SEC, lrand.nextFloat()) * sheet.resourceDensity;
                if ((GameServerState.instance != null)) container.setPassiveSources(sector,
                        new PassiveResourceSupplier[]{
                                new PassiveResourceSupplier(sourceRate,
                                        elementEntries.get("Parsyne Plasma").id, ResourceSourceType.SPACE, sector)
                        });
            }
        }

        if(overwrite){
            System.err.println("[MOD][Resources ReSourced][WARNING] Zone "+ name +" overwrote a system sheet at ("+system.toString()+")!");
        }
        else if(systemDictionary.count() == debugPreSize){
            System.err.println("[MOD][Resources ReSourced][ERROR] Failed to add a system ("+system.toString()+") to " + name + "!");
            System.exit(-1);
        }
    }

    public void addAll(@NotNull Collection<Vector3i> starSystems){
        assert starSystems != null;
        stars.addAll(starSystems);
    }

    public void remove(Vector3i starSystem){
        systemDictionary.remove(starSystem); //hopefully we don't need this; that would have strange implications.
        //if this does get called, don't forget to add the system to the unassociated systems zone.
    }

    public SystemSheet getSheetFor(Vector3i system) throws IllegalArgumentException{
        if(!systemDictionary.containsLocation(system)) throw new IllegalArgumentException("[MOD][Resources ReSourced] Cannot provide system sheet: zone does not contain the specified system!");
        return systemDictionary.get(system);
    }

    public boolean containsStar(Vector3i star){
        return systemDictionary.containsLocation(star);
    };

    public int population(){
        return systemDictionary.count();
    }

    private SystemSheet generateSystemSheet(Vector3i loc, boolean isVoid){
        //TODO: move from external 'generate' to internal 'populate' for most info?
        Random lrand = new Random(galaxy.getSystemSeed(loc));

        float offType = OFFTYPE_ASTEROID_CHANCE_MIN + (lrand.nextFloat() * OFFTYPE_ASTEROID_CHANCE_MAX);
        float rscDensity;
        if(isVoid) rscDensity = 0f;
        else rscDensity = lerp(MIN_SYSTEM_RESOURCE_DENSITY,MAX_SYSTEM_RESOURCE_DENSITY, rng.nextFloat());
        SystemClass cls = pickSystemType(isVoid);

        if (this.zoneClass == ZoneClass.RR_EXTRADIMENSIONAL) {
            offType = 0.0f; //no asteroids from this universe
        } else if (this.zoneClass == ZoneClass.RR_PARAMETRIC) {
            //would have to pass a value or range of values to this later.
        }

        return new SystemSheet(cls, offType, this, rscDensity);
    }

    public SystemClass pickSystemType(boolean isVoid){
        SystemClass result;
        if(isVoid) switch(this.zoneClass){
            case RR_FERRIC:
            case RR_METAL_DENSE:
                result = RR_METAL_VOID;
                break;
            case RR_CRYSTAL_DENSE:
                result = RR_CRYSTAL_VOID;
                break;
            case RR_ANBARIC:
                result = RR_ANBARIC_VOID;
                break;
            case RR_MISTY:
                result = RR_MISTY_VOID;
                break;
            case RR_ENERGETIC:
                result = RR_ENERGETIC_VOID;
                break;
            case RR_EXTRADIMENSIONAL:
                result = RR_EXTRADIMENSIONAL_VOID;
                break;
            default:
                result = NORMAL_VOID;
        }
        else switch(this.zoneClass){
            //TODO: Any special rules for random systems in zones being off-types go here
            //or if we want some kind of super dense jackpot versions of the systems... hmmmmmm
            case NONE:
                result = SystemClass.NORMAL;
                break;
            case RR_CORE:
                result = SystemClass.RR_CORE;
                break;
            case RR_METAL_DENSE:
                int m = rng.nextInt(20);
                switch(m){
                    case 0: result = SystemClass.NORMAL; break;
                    case 1: result = SystemClass.RR_FERRON; break;
                    case 2: result = SystemClass.RR_CRYSTAL_RICH; break;
                    default: result = SystemClass.RR_METAL_RICH; break;
                }
                break;
            case RR_MISTY:
                result = SystemClass.RR_MISTY;
                break;
            case RR_ANBARIC:
                result = SystemClass.RR_ANBARIC;
                break;
            case RR_ENERGETIC:
                result = SystemClass.RR_ENERGETIC;
                break;
            case RR_EXTRADIMENSIONAL:
                result = SystemClass.RR_EXTRADIMENSIONAL;
                break;
            case RR_FRIGID:
                result = SystemClass.RR_FRIGID;
                break;
            case RR_CRYSTAL_DENSE:
                int n = rng.nextInt(50);
                switch(n){
                    case 0: result = SystemClass.RR_FRIGID; break;
                    case 1: result = SystemClass.RR_CRYSTAL_RICH; break;
                    case 2: case 3: case 4: case 5: case 6: result = SystemClass.NORMAL; break;
                    default: result = SystemClass.RR_CRYSTAL_RICH; break;
                }
                break;
            case RR_FERRIC:
                result = SystemClass.RR_FERRON;
                break;
            case RR_PARAMETRIC:
                result = SystemClass.RR_PARAMETRIC;
                break;
            //-------PATHFINDER CRAP-------
            case PF_COMETARY:
                result = SystemClass.PF_COMETARY;
                break;
            case PF_DUSTY:
                result = SystemClass.PF_DUSTY;
                break;
            case PF_EXOTIC:
                result = SystemClass.PF_EXOTIC;
                break;
            case PF_FLUX:
                result = SystemClass.PF_FLUX;
                break;
            case PF_FRIGID:
                result = SystemClass.PF_FRIGID;
                break;
            case PF_PYROCLASTIC:
                result = SystemClass.PF_PYROCLASTIC;
                break;
            case PF_RUDDY:
                result = SystemClass.PF_RUDDY;
                break;
            case PF_TEMPERATE:
                result = SystemClass.PF_TEMPERATE;
                break;
            default:
                result = NORMAL;
        }
        return result;
    }

    @Override
    public String toString() {
        return "ResourceZone{" +
                "name='" + name + '\'' +
                ", zoneClass=" + zoneClass +
                ", center='" + stars.getFirst()+"'"+
                ", stars='"+stars.size()+"'"+
                '}';
    }

    public static class ZoneOrigin {
        public final Vector3i location;
        public final float radius;
        public final float height;
        public final List<Vector3i> attachedStars = new ArrayList<Vector3i>();

        public ZoneOrigin(Vector3i location, float radius, float height){
            this.location = new Vector3i(location);
            this.radius = radius;
            this.height = height;
        }
    }
}
