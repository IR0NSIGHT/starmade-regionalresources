package org.ithirahad.resourcesresourced.universe.galaxy;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import org.schema.common.util.linAlg.Vector3i;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import static org.ithirahad.resourcesresourced.RRSConfiguration.NAME_GENERIC_ADJECTIVE_CHANCE;
import static org.ithirahad.resourcesresourced.RRSConfiguration.NAME_PREFIX_CHANCE;


public class ZoneNamer {
    public static String[] suffixes = {"Stars","Expanse","Stretch","Isles Region","Belt","Reach","Strand","Cluster","Veil","Vale","Sea Region","Star Sea","Ocean Region","Star Ocean","Mantle","Thicket","Star Formation","Cascade","Falls","Verge","Shore","Fade","Pass","Passage","River","Creek","Drift","Solitude","Domain","Wilds","Wildlands","Archipelago","Frontier","Impasse","Span","Vestige","Remnant","Shards","Pocket","Meadows Region","Fade","Heath","Branch","Cache","Immensity","Ether","Stain","Scar","Steps","Den","Redoubt","Labyrinth"}; //TODO: configurable
    public static String[] prefixes = {"Greater","Great","Outer","Inner","High","Grand","Alpha"}; //TODO: STOP USING THESE SO MUCH
    public static String[] zoneAdjectives = {
            //Arranged so they can be removed in thematic lines or clusters as easily as possible
            //TODO: configurable
            "Drifting","Reaching","Creeping",
            "Drowned","Shadowed","Fallen","Dim","Marred",
            "Harmonious","Harmony",
            "Arcadian","Utopian","Halcyon",
            "Grand",
            "Brilliant", "Beacon", "Luminous", "Luminiferous", "Lighthouse", "Gleaming",
            "Corona", "Crown", "Pinnacle",
            "Elysian", "Empyrean", "Divinity's", "Heaven's", "Providence",
            "Archangel","Angel","Seraph","Cherub","Valkyrie",
            "Forsaken","Tenebrous","Shrouded", "Shadowy", "Wisp", "Echo",
            "Lone", "Lonely", "Forlorn",
            "Intrepid","Brave","Heedless","Fool's",
            "Protean","Shifting","Grey","Purgatory",
            "Orphean","Tartarous","Hadean","Charonine",
            "Stygian", "Abyssal", "Infernal", "Nadir", "Grim",
            "Phantom","Ghoul","Ghast","Spectre","Demon",
            "Tormentor's",
            "Widow's","Widower's",
            "Arrowhead","Bullet","Blade","Bayonet","Sword","Katana","Scimitar","Sabre","Dagger","Thorned","Spearhead",
            "Sage","Rose","Mandrake","Amaranth","Nightshade","Silphium","Foxglove","Lily","Asphodel",
            "Aura","Miasmic", "Nebulous", "Fogborne",
            "Cloudborne","Cumulus","Stratus","Nimbus","Cirrus",
            "Archer's","Hunter's", "Seeker's","Treader's","Chaser's",
            "Mender's", "Oracle's", "Seer's", "Augur's","Mystic's",
            "Mason's","Fletcher's",
            "Dreamer's","Drifter's","Wanderer's","Vagabond's","Peregrine","Eidolon",
            "Heir's", "Baron's", "Ascendant's","Noble's",
            "Barbarian","Savage",
            "Rogue","Thief's","Thieves'","Robber's","Scoundrel","Liar's","Wicked",
            "Edda","Legend's","Kalevala","Odyssey","Gilgamesh",
            "Thor's","Odin's","Tyr's","Idunn's","Freya's","Loki's","Zeus's","Poseidon's","Hera's","Athena's","Iris","Perun's","Veles's","Svarog's","Stribog's","Zhiva's",
            "Ranger's","Hero's","Sorcerer's","Knight's","Sellsword","Enchanter's",
            "Dragon's", "Golem", "Fae", "Goblin", "Gnommish", "Elfin",
            "Morass","Mire",
            "Burning", "Candle", "Inferno","Kindling", "Ember", "Blazing", "Scalding", "Torrid",
            "Whirlwind", "Tradewind", "Doldrum",
            "Tidal", "Crest", "Wavecrest", "Tsunami",
            "Boulder", "Monument", "Monolith", "Obelisk",
            "Argent", "Silvery",
            "Auric", "Gilded",
            "Alpha","Bravo","Delta","Echo","Foxtrot","Juliett","Kilo","Sierra","Tango","Victor",
            "Protonic","Neutrino","Plasmonic","Thermion","Soliton",
            "Dawn","Sunrise","Noontide","Dusk","Gloaming","Midnight",
            "Tribute"
    };

    public static HashMap<Vector3i, IntOpenHashSet> usedNames = new HashMap<>();

    public String generateZoneName(Vector3i galaxy, ZoneClass zoneClass, long seed) {
        if(zoneClass == ZoneClass.RR_CORE) return "Galactic Core";

        IntOpenHashSet galaxyUsedNames;
        if(usedNames.containsKey(galaxy)) galaxyUsedNames = usedNames.get(galaxy);
        else{
            galaxyUsedNames = new IntOpenHashSet();
            usedNames.put(galaxy,galaxyUsedNames);
        }

        Random rng = new Random(seed);
        String result = "";

        if(new Random(seed).nextFloat() < (NAME_PREFIX_CHANCE/4)){ //this is bad but it doesn't patch over configs by default, and this needs to reduce significantly.... also, used isolated Random so that messing with this will not change subsequent things
            result = prefixes[rng.nextInt(prefixes.length)] + " "; //e.g. Greater
        }

        if(rng.nextFloat() < NAME_GENERIC_ADJECTIVE_CHANCE){
            if(usedNames.size() >= zoneAdjectives.length) usedNames.clear();
            Integer index;
            do index = rng.nextInt(zoneAdjectives.length); while(galaxyUsedNames.contains(index));
            galaxyUsedNames.add(index);
            result += zoneAdjectives[index]; //e.g. Midnight
        }
        else {
            String[] results = zoneClass.adjective();
            if (results.length > 1) result += results[rng.nextInt(results.length)]; //e.g. Glittering
            else result += results[0];
        }

        result += " " + suffixes[rng.nextInt(suffixes.length)]; //e.g. Veil
        return result;
    }
}
