package org.ithirahad.resourcesresourced.universe.terrestrial.factory;

import org.ithirahad.resourcesresourced.universe.terrestrial.TerrestrialPlanetGroundType;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.server.controller.world.factory.WorldCreatorPlanetFactory;
import org.schema.game.server.controller.world.factory.terrain.TerrainDeco;

import javax.vecmath.Vector3f;
import java.util.Random;

public abstract class RRSPlanetFactory extends WorldCreatorPlanetFactory {
    public final TerrestrialPlanetGroundType materials;
    public short caveBottom = 80; //TODO: override getCaveBottom with custom - e.g. ice crystal
    TerrainDeco[] minable;

    //TODO: Some custom terrain gen tweaks for deserts/barrens and such - e.g. noise amp ranges

    public RRSPlanetFactory(long seed, Vector3f[] polys, float radius, TerrestrialPlanetGroundType materials) {
        super(seed, polys, radius);
        this.materials = materials;

        Random rng = new Random(seed);
        this.minable = materials.decos;
        /*
        this.minable[2] = new GeneratorFloraPlugin((short)96, this.getTop(), this.getFiller());
        this.minable[3] = new GeneratorFloraPlugin((short)104, this.getTop(), this.getFiller());
        this.minable[4] = new GeneratorFloraPlugin((short)100, this.getTop(), this.getFiller());
        this.minable[5] = new GeneratorFloraPlugin((short)108, this.getTop(), this.getFiller());
         */
    }

    @Override
    public short getTop() {
        return materials.surface;
    }

    @Override
    public short getFiller() {
        return materials.soil;
    }

    @Override
    public short getSolid() {
        return materials.substrate;
    }

    @Override
    public TerrainDeco[] getGen() {
        return this.minable;
    }

    @Override
    public short getCaveBottom() {
        return materials.caveBottomLiquid;
    }

    @Override
    public void initialize(SegmentController world){
        generator = new RRSTerrainGenerator(((Planet) world).getSeed());
        generator.setWorldCreator(this);
        init(world);

        initialized = true;
    }
}
