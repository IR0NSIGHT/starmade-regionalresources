package org.ithirahad.resourcesresourced.universe.terrestrial.factory.VanillaTweaks;

import org.ithirahad.resourcesresourced.universe.terrestrial.factory.RRSPlanetFactory;
import org.ithirahad.resourcesresourced.universe.terrestrial.factory.RRSTerrainGenerator;
import org.ithirahad.resourcesresourced.universe.terrestrial.TerrestrialPlanetGroundType;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.RequestData;
import org.schema.game.server.controller.RequestDataPlanet;
import org.schema.game.server.controller.world.factory.terrain.*;

import javax.vecmath.Vector3f;

import java.util.Random;

public class VIcePlanetFactory extends RRSPlanetFactory {
    private TerrainDeco[] minable;

    public VIcePlanetFactory(long seed, Vector3f[] polys, float radius) {
        super(seed, polys, radius, TerrestrialPlanetGroundType.VANILLA);
        this.minable = new TerrainDeco[4];
        this.minable[0] = new GeneratorFloraPlugin(1, (short)280, this.getTop(), this.getFiller());
        this.minable[1] = new GeneratorFloraPlugin(1, (short)279, this.getTop(), this.getFiller());
        this.minable[2] = new GeneratorFloraPlugin(1, (short)281, this.getTop(), this.getFiller());
        this.minable[3] = new GeneratorFloraPlugin(1, (short)278, this.getTop(), this.getFiller());
    }

    @Override
    public void createAdditionalRegions(Random r) {

    }

    @Override
    public void createWorld(SegmentController world, Segment w, RequestData requestData) {
        synchronized (this) {
            if (!initialized) {
                generator = new RRSTerrainGenerator(((Planet) world).getSeed()){
                    {
                        this.setFlatness(0.6523427423424416D);
                        this.halfHeightFactor = 0.4D;
                        this.heighNormValue = 48.0D;
                        this.planetEdgeHeight = 36.0F;
                        this.polyMargin = 44.0F;
                        this.hasC = true;
                    }

                    protected void initOctaves(int xPos, int yPos, int zPos, int width, int height, int depth, RequestDataPlanet requestData) {
                        double d = 393.12345431233D;
                        double d1 = 621.5127634345D;
                        double smooth2d = 100.0D;
                        requestData.getR().noise2DMid16 = this.noiseGen6Oct16for2D.make2DOctaves(xPos, zPos, width, depth, smooth2d, smooth2d, requestData.getR().noise2DMid16);
                        double smoothXZ = 40.0D;
                        double smoothY = 180.0D;
                        requestData.getR().noiseSmall8 = this.noiseGen3Oct8.make3DOctaves(xPos, yPos, zPos, width, height, depth, d / smoothXZ, d1 / smoothY, d / smoothXZ, requestData.getR().noiseSmall8);
                        requestData.getR().noise1Big16 = this.noiseGen1Oct16.make3DOctaves(xPos, yPos, zPos, width, height, depth, d * 3.0D, d1 / 1.6D, d * 3.0D, requestData.getR().noise1Big16);
                        requestData.getR().noise2Big16 = this.noiseGen2Oct16.make3DOctaves(xPos, yPos, zPos, width, height, depth, d * 3.0D, d1 / 1.9D, d * 3.0D, requestData.getR().noise2Big16);
                    }

                    protected void initNoises(Random rand) {
                        this.noiseGen1Oct16 = new OctavesGenerator(rand, 20);
                        this.noiseGen2Oct16 = new OctavesGenerator(rand, 19);
                        this.noiseGen3Oct8 = new OctavesGenerator(rand, 7);
                        this.noiseGen4Oct4 = new OctavesGenerator(rand, 5);
                        this.noiseGen6Oct16for2D = new OctavesGenerator(rand, 8);
                    }
                };
                generator.setWorldCreator(this);
                init(world);
                initialized = true;
            }
        }

        try {
            gen(w, (RequestDataPlanet) requestData);
        } catch (SegmentDataWriteException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public short getCaveBottom() {
        return ElementKeyMap.TERRAIN_ICEPLANET_CRYSTAL;
    }

    @Override
    public short getFiller() {
        return ElementKeyMap.TERRAIN_ICE_ID;
    }

    @Override
    public TerrainDeco[] getGen() {
        return minable;
    }

    @Override
    public short getSolid() {
        return ElementKeyMap.TERRAIN_ICEPLANET_ROCK;
    }

    @Override
    public short getTop() {
        return ElementKeyMap.TERRAIN_ICEPLANET_SURFACE;
    }

}
