package org.ithirahad.resourcesresourced.universe.terrestrial.factory.VanillaTweaks;

import org.ithirahad.resourcesresourced.universe.terrestrial.factory.RRSPlanetFactory;
import org.ithirahad.resourcesresourced.universe.terrestrial.TerrestrialPlanetGroundType;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.server.controller.world.factory.terrain.GeneratorFloraPlugin;
import org.schema.game.server.controller.world.factory.terrain.TerrainDeco;

import javax.vecmath.Vector3f;

import java.util.Random;

public class VMarsPlanetFactory extends RRSPlanetFactory {
    private TerrainDeco[] minable;

    public VMarsPlanetFactory(long seed, Vector3f[] polys, float radius) {
        super(seed, polys, radius, TerrestrialPlanetGroundType.VANILLA);
        this.minable = new TerrainDeco[4];
        this.minable[0] = new GeneratorFloraPlugin((short)96, this.getTop(), this.getFiller());
        this.minable[1] = new GeneratorFloraPlugin((short)104, this.getTop(), this.getFiller());
        this.minable[2] = new GeneratorFloraPlugin((short)100, this.getTop(), this.getFiller());
        this.minable[3] = new GeneratorFloraPlugin((short)108, this.getTop(), this.getFiller());
    }

    @Override
    public void createAdditionalRegions(Random r) {
    }

    @Override
    public short getFiller() {
        return ElementKeyMap.TERRAIN_MARS_DIRT;
    }

    @Override
    public TerrainDeco[] getGen() {
        return minable;
    }

    @Override
    public short getSolid() {
        return ElementKeyMap.TERRAIN_ROCK_MARS;
    }

    @Override
    public short getTop() {
        return ElementKeyMap.TERRAIN_MARS_TOP;
    }
}
