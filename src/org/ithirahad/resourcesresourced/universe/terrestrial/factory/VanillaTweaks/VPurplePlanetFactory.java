package org.ithirahad.resourcesresourced.universe.terrestrial.factory.VanillaTweaks;

import org.ithirahad.resourcesresourced.universe.terrestrial.factory.RRSPlanetFactory;
import org.ithirahad.resourcesresourced.universe.terrestrial.factory.RRSTerrainGenerator;
import org.ithirahad.resourcesresourced.universe.terrestrial.TerrestrialPlanetGroundType;
import org.schema.game.common.controller.Planet;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.Segment;
import org.schema.game.common.data.world.SegmentDataWriteException;
import org.schema.game.server.controller.RequestData;
import org.schema.game.server.controller.RequestDataPlanet;
import org.schema.game.server.controller.world.factory.terrain.GeneratorFloraPlugin;
import org.schema.game.server.controller.world.factory.terrain.TerrainDeco;

import javax.vecmath.Vector3f;

import java.util.Random;

public class VPurplePlanetFactory extends RRSPlanetFactory {
    private TerrainDeco[] minable = new TerrainDeco[4];

    public VPurplePlanetFactory(long seed, Vector3f[] polys, float radius) {
        super(seed, polys, radius, TerrestrialPlanetGroundType.VANILLA);
        this.minable[0] = new GeneratorFloraPlugin((short)97, this.getTop(), this.getFiller());
        this.minable[1] = new GeneratorFloraPlugin((short)101, this.getTop(), this.getFiller());
        this.minable[2] = new GeneratorFloraPlugin((short)105, this.getTop(), this.getFiller());
        this.minable[3] = new GeneratorFloraPlugin((short)109, this.getTop(), this.getFiller());
    }

    @Override
    public void createAdditionalRegions(Random r) {

    }

    @Override
    public void createWorld(SegmentController world, Segment w, RequestData requestData) {
        synchronized(this) {
            if (!this.initialized) {
                this.generator = new RRSTerrainGenerator(((Planet)world).getSeed()){
                    {
                        this.setFlatness(0.45D);
                        this.hasColumns = true;
                        this.defaultMax = 14.0F;
                    }
                };
                this.generator.setWorldCreator(this);
                this.init(world);
                this.initialized = true;
            }
        }

        try {
            this.gen(w, (RequestDataPlanet)requestData);
        } catch (SegmentDataWriteException var6) {
            var6.printStackTrace();
        }

    }

    @Override
    public short getFiller() {
        return ElementKeyMap.TERRAIN_PURPLE_ALIEN_VINE;
    }

    @Override
    public TerrainDeco[] getGen() {
        return minable;
    }

    @Override
    public short getSolid() {
        return ElementKeyMap.TERRAIN_PURPLE_ALIEN_ROCK;
    }

    @Override
    public short getTop() {
        return ElementKeyMap.TERRAIN_PURPLE_ALIEN_TOP;
    }
}
