package org.ithirahad.resourcesresourced.universe.terrestrial;

import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.ithirahad.resourcesresourced.industry.PassiveResourceSupplier;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.SectorInformation;

import java.util.Random;

import static org.ithirahad.resourcesresourced.industry.ResourceSourceType.*;
import static org.ithirahad.resourcesresourced.RRSConfiguration.*;
import static org.ithirahad.resourcesresourced.RRSElementInfoManager.elementEntries;
import static org.schema.game.common.data.world.SectorInformation.PlanetType.*;

public enum RRSTerrestrialPlanetType {
    V_RED("Rusty Planet", MARS),
    V_TERRAN("Verdant Plant", EARTH),
    V_SAND("Desert Planet", DESERT),
    V_PURPLE("Xenofungal Planet", PURPLE),
    V_ICE("Ice Pillar Planet", ICE), //Vanilla types
    RRS_ENERGETIC("Thermyn Energized Planet", ICE, TerrestrialPlanetGroundType.ENERGETIC_TYPES), //Thermyn Amalgam planets
    RRS_COLD("Icy Planet", ICE, ElementKeyMap.TERRAIN_ICEPLANET_CRYSTAL,TerrestrialPlanetGroundType.COLD_TYPES),
    RRS_COLD_AEGIUM("Sub-Zero Planet", ICE, ElementKeyMap.TERRAIN_ICEPLANET_CRYSTAL,TerrestrialPlanetGroundType.AEGIUM_TYPES),
    RRS_TEMPERATE("Temperate Planet", EARTH, TerrestrialPlanetGroundType.TEMPERATE_TYPES),
    RRS_DESERT("Desert Planet", DESERT, TerrestrialPlanetGroundType.DESERT_TYPES),
    RRS_BARREN("Barren Planet", DESERT, ElementKeyMap.TERRAIN_ROCK_NORMAL,TerrestrialPlanetGroundType.BARREN_TYPES), //Stone worlds
    RRS_CRYSTAL("Crystalline Planet", DESERT, ElementKeyMap.CRYS_PARSEEN,TerrestrialPlanetGroundType.CRYSTAL_TYPES), //crystal-rich
    RRS_METAL("Metallic Planet", DESERT, TerrestrialPlanetGroundType.METAL_TYPES), //metal-rich
    RRS_MIST("Mistbound Planet", DESERT, TerrestrialPlanetGroundType.MIST), //Parsyne Plasma
    RRS_HOT("Torrid Planet", MARS, TerrestrialPlanetGroundType.HOT_TYPES),
    RRS_EXTRADIMENSIONAL("Enigmatic Planetoid", PURPLE, TerrestrialPlanetGroundType.EXTRADIMENSIONAL);

    private final SectorInformation.PlanetType vanillaEquiv;
    private final TerrestrialPlanetGroundType[] groundTypes;
    public final String descName;

    RRSTerrestrialPlanetType(String labelName,SectorInformation.PlanetType vanillaEquivalent,TerrestrialPlanetGroundType... groundTypes) {
        this.descName = labelName;
        this.vanillaEquiv = vanillaEquivalent;
        this.groundTypes = groundTypes;
    }

    RRSTerrestrialPlanetType(String labelName,SectorInformation.PlanetType vanillaEquivalent,short caveBottomMaterial,TerrestrialPlanetGroundType... groundTypes) { //TODO
        this.descName = labelName;
        this.vanillaEquiv = vanillaEquivalent;
        this.groundTypes = groundTypes;
    }

    public TerrestrialPlanetGroundType[] getGroundTypes() {
        return groundTypes;
    }

    public SectorInformation.PlanetType vanillaEquiv(){
        return vanillaEquiv;
    }

    public PassiveResourceSupplier[] getResources(long seed, Vector3i sector) {
        switch(this){
            case V_RED:
            case RRS_HOT:
                return new PassiveResourceSupplier[]{new PassiveResourceSupplier(2.5f, elementEntries.get("Metallic Ore").id, GROUND, sector)};
            case V_SAND:
            case RRS_DESERT:
                return new PassiveResourceSupplier[]{new PassiveResourceSupplier(2.5f, elementEntries.get("Crystalline Ore").id, GROUND, sector)};
            case V_PURPLE:
                return new PassiveResourceSupplier[]{
                        new PassiveResourceSupplier(0.2f, elementEntries.get("Floral Protomaterial Capsule").id, GROUND, sector),
                        new PassiveResourceSupplier(1, elementEntries.get("Crystalline Ore").id, GROUND, sector)
                };
            case V_TERRAN:
                return new PassiveResourceSupplier[]{
                        new PassiveResourceSupplier(0.2f, elementEntries.get("Floral Protomaterial Capsule").id, GROUND, sector),
                        new PassiveResourceSupplier(1, elementEntries.get("Metallic Ore").id, GROUND, sector)
                };
            case V_ICE:
            case RRS_COLD:
                return new PassiveResourceSupplier[]{
                        new PassiveResourceSupplier(0.5f, ElementKeyMap.WATER, GROUND, sector),
                        new PassiveResourceSupplier(1, elementEntries.get("Crystalline Ore").id, GROUND, sector)
                };
            case RRS_ENERGETIC:
                return new PassiveResourceSupplier[]{
                        new PassiveResourceSupplier(MiscUtils.lerp(RSC_THERMYN_MIN_REGEN_RATE_SEC,RSC_THERMYN_MAX_REGEN_RATE_SEC,new Random(seed).nextFloat()), elementEntries.get("Thermyn Amalgam").id, GROUND, sector)};
            case RRS_COLD_AEGIUM:
                return new PassiveResourceSupplier[]{
                        new PassiveResourceSupplier(5 * new Random(seed).nextFloat(), elementEntries.get("Aegium Ore").id, GROUND, sector),
                        new PassiveResourceSupplier(1, ElementKeyMap.TERRAIN_ICE_ID, GROUND, sector),
                        new PassiveResourceSupplier(1, elementEntries.get("Crystalline Ore").id, GROUND, sector)
                };
                //TODO: Oil? :P
            case RRS_CRYSTAL:
                return new PassiveResourceSupplier[]{new PassiveResourceSupplier(4, elementEntries.get("Crystalline Ore").id, GROUND, sector)};
            case RRS_METAL:
                if(new Random(seed).nextBoolean()) {
                    return new PassiveResourceSupplier[]{
                            new PassiveResourceSupplier(new Random(seed).nextFloat(), elementEntries.get("Metallic Ore").id, GROUND, sector),
                            new PassiveResourceSupplier(5 * new Random(seed).nextFloat(), elementEntries.get("Ferron Ore").id, GROUND, sector)
                    };
                } else return new PassiveResourceSupplier[]{
                        new PassiveResourceSupplier(2 * new Random(seed).nextFloat(), elementEntries.get("Metallic Ore").id, GROUND, sector),
                };
            case RRS_MIST:
                return new PassiveResourceSupplier[]{
                        new PassiveResourceSupplier(RSC_PARSYNE_MIN_REGEN_RATE_SEC * RSC_PARSYNE_PLANET_MOD * 0.5f, elementEntries.get("Parsyne Plasma").id, GROUND, sector),
                        new PassiveResourceSupplier(RSC_PARSYNE_MIN_REGEN_RATE_SEC * RSC_PARSYNE_PLANET_MOD * 0.5f, elementEntries.get("Parsyne Plasma").id, SPACE, sector) //would be harvestable from atmosphere as well
                };
            case RRS_EXTRADIMENSIONAL:
                //eventually...
            default:
                return new PassiveResourceSupplier[]{
                        new PassiveResourceSupplier(new Random(seed).nextFloat(), elementEntries.get("Metallic Ore").id, GROUND, sector),
                        new PassiveResourceSupplier(new Random(seed).nextFloat(), elementEntries.get("Crystalline Ore").id, GROUND, sector)
                };
        }
    }
}
