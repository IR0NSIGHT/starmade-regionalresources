package org.ithirahad.resourcesresourced.universe.terrestrial;

import org.schema.game.server.controller.world.factory.terrain.*;

import java.util.ArrayList;
import java.util.Random;

import static org.ithirahad.resourcesresourced.universe.terrestrial.RRSTerrestrialPlanetType.*;
import static org.schema.game.common.data.element.ElementKeyMap.*;

public enum TerrestrialPlanetGroundType {
    VANILLA("planet",598,598,598, V_TERRAN), //All grey hull. Indicates that the ground type is being asked for when it shouldn't be.

    ENERGETIC_OVERGROWN("energized planet overgrown with strange living mass", TERRAIN_PURPLE_ALIEN_TOP, TERRAIN_ROCK_BLACK, CRYS_VARAT, RRS_ENERGETIC),
    ENERGETIC_STONY("stony, energized planet", TERRAIN_EARTH_TOP_ROCK, TERRAIN_ROCK_NORMAL, CRYS_VARAT, RRS_ENERGETIC, TERRAIN_ROCK_SPRITE, TERRAIN_FAN_FLOWER_ICE_SPRITE),
    ENERGETIC_CRYONIC("chilled, energized planet", TERRAIN_ICEPLANET_ROCK, TERRAIN_ROCK_MARS, TERRAIN_ICEPLANET_CRYSTAL, RRS_ENERGETIC, TERRAIN_ICE_CRAG_SPRITE, TERRAIN_GLOW_TRAP_SPRITE),
    ENERGETIC_DEAD("energized planet", TERRAIN_ROCK_BLACK, TERRAIN_ROCK_MARS, CRYS_VARAT, RRS_ENERGETIC, TERRAIN_ROCK_SPRITE),
    ENERGETIC_ODD("odd energized planet", TERRAIN_ROCK_BLUE, TERRAIN_ROCK_BLUE, CRYS_VARAT, RRS_ENERGETIC, TERRAIN_CORAL_ICE_SPRITE, TERRAIN_FLOWER_FAN_PURPLE_SPRITE),
    ENERGETIC_WATER("odd energized planet", WATER, TERRAIN_ROCK_BLUE, CRYS_VARAT, RRS_ENERGETIC, TERRAIN_WEEDS_PURPLE_SPRITE, TERRAIN_CORAL_RED_SPRITE),

    COLD_BLUE("chilly planet", TERRAIN_ICEPLANET_SURFACE, TERRAIN_ICEPLANET_ROCK, TERRAIN_ROCK_BLUE, TERRAIN_ICE_ID, RRS_COLD, TERRAIN_WEEDS_PURPLE_SPRITE),
    COLD_CRYSTAL("frozen planet", TERRAIN_ICEPLANET_SURFACE, TERRAIN_ICEPLANET_ROCK, TERRAIN_ICE_ID, TERRAIN_ICEPLANET_CRYSTAL, RRS_COLD, TERRAIN_ICE_CRAG_SPRITE, TERRAIN_FAN_FLOWER_ICE_SPRITE),
    COLD_PERMAFROST("permafrost-covered planet", TERRAIN_ICEPLANET_ROCK, TERRAIN_DIRT_ID, TERRAIN_ROCK_WHITE, TERRAIN_ICE_ID, RRS_COLD, TERRAIN_ROCK_SPRITE, TERRAIN_FLOWERS_DESERT_SPRITE),
    COLD_ICE("icy planet", TERRAIN_ICE_ID, TERRAIN_ICEPLANET_ROCK,  TERRAIN_ROCK_WHITE, TERRAIN_ICEPLANET_ROCK, RRS_COLD),
    COLD_GLOW("strange frozen planet", TERRAIN_ICE_ID, CRYS_PARSEEN, TERRAIN_ROCK_WHITE, CRYS_PARSEEN, RRS_COLD), //Snowblindness, but it's a planet.

    AEGIUM_BLUE("frigid world", TERRAIN_ICE_ID, TERRAIN_ICEPLANET_CRYSTAL, TERRAIN_ICEPLANET_ROCK, TERRAIN_ICEPLANET_CRYSTAL, RRS_COLD_AEGIUM, TERRAIN_ICE_CRAG_SPRITE),
    AEGIUM_PURPLE("extremely cold planet", TERRAIN_ICE_ID, CRYS_RAMMET, TERRAIN_ICEPLANET_ROCK, CRYS_RAMMET, RRS_COLD_AEGIUM, TERRAIN_ICE_CRAG_SPRITE, TERRAIN_YHOLE_PURPLE_SPRITE),
    AEGIUM_YELLOW("extremely cold planet", TERRAIN_ICE_ID, CRYS_NOCX, TERRAIN_ICE_ID, CRYS_HATTEL, RRS_COLD_AEGIUM, TERRAIN_ICE_CRAG_SPRITE, TERRAIN_YHOLE_PURPLE_SPRITE),

    TEMPERATE_STONY("temperate, rocky world", TERRAIN_EARTH_TOP_ROCK, TERRAIN_ROCK_NORMAL, TERRAIN_ROCK_WHITE, RRS_TEMPERATE),
    TEMPERATE_DIRT("temperate, dirt-covered planet", TERRAIN_DIRT_ID, TERRAIN_ROCK_YELLOW, TERRAIN_ROCK_NORMAL, RRS_TEMPERATE),
    TEMPERATE_GRASS("verdant planet", TERRAIN_EARTH_TOP_DIRT, TERRAIN_DIRT_ID, TERRAIN_ROCK_NORMAL, RRS_TEMPERATE,
        new TerrainDecoTrees(),
        (short)93,
        (short)98,
        (short)102,
        (short)106),
    TEMPERATE_WEIRD("odd growth-encrusted planet", TERRAIN_PURPLE_ALIEN_VINE, TERRAIN_ROCK_BLACK, TERRAIN_ROCK_NORMAL, RRS_TEMPERATE),

    DESERT_SANDY("desert planet", TERRAIN_SAND_ID, TERRAIN_SAND_ID, TERRAIN_ROCK_YELLOW, RRS_DESERT, TERRAIN_CACTUS_SMALL_SPRITE, TERRAIN_CACTUS_ARCHED_SPRITE),
    DESERT_RED("ruddy desert planet", TERRAIN_MARS_TOP, TERRAIN_MARS_DIRT, TERRAIN_ROCK_MARS, RRS_DESERT, TERRAIN_ROCK_SPRITE, TERRAIN_FUNGAL_GROWTH_SPRITE),
    DESERT_ORANGE("rugged desert planet", TERRAIN_ROCK_ORANGE, TERRAIN_ROCK_WHITE, TERRAIN_ROCK_NORMAL, RRS_DESERT, TERRAIN_FLOWERS_DESERT_SPRITE),
    DESERT_BLACK("dark desert planet", TERRAIN_ROCK_BLACK, TERRAIN_ROCK_MARS, TERRAIN_ROCK_NORMAL, RRS_DESERT, TERRAIN_FLOWERS_DESERT_SPRITE, TERRAIN_FLOWERS_YELLOW_SPRITE),
    DESERT_TAN("dusty desert world", TERRAIN_ROCK_YELLOW, TERRAIN_ROCK_YELLOW, TERRAIN_ROCK_NORMAL, RRS_DESERT, TERRAIN_ROCK_SPRITE, TERRAIN_FLOWERS_DESERT_SPRITE, TERRAIN_CACTUS_SMALL_SPRITE, TERRAIN_CACTUS_ID),

    BARREN_DARK("dark, barren world", TERRAIN_ROCK_MARS, TERRAIN_ROCK_BLACK, TERRAIN_ROCK_BLACK, RRS_BARREN),
    BARREN_WHITE("bleached, barren world", TERRAIN_ROCK_WHITE, TERRAIN_ROCK_NORMAL, CRYS_PARSEEN, RRS_BARREN, TERRAIN_ROCK_SPRITE),
    BARREN_GREEN("strange, barren planet", TERRAIN_ROCK_GREEN, TERRAIN_ROCK_GREEN, TERRAIN_ROCK_NORMAL, RRS_BARREN, TERRAIN_ROCK_SPRITE),
    BARREN_LUNAR("barren planet which may have once been a stray moon", TERRAIN_ROCK_NORMAL, TERRAIN_ROCK_NORMAL, TERRAIN_ROCK_BLACK, RRS_BARREN, TERRAIN_ROCK_SPRITE),

    CRYSTAL_WHITE("gleaming crystal-rich planet", TERRAIN_ROCK_WHITE, CRYS_HATTEL, TERRAIN_ROCK_MARS, CRYS_HATTEL, RRS_CRYSTAL),
    CRYSTAL_BLACK("glowing crystal-rich planet", TERRAIN_ROCK_BLACK, CRYS_MATTISE, TERRAIN_ROCK_NORMAL, CRYS_MATTISE, RRS_CRYSTAL),
    CRYSTAL_PURPLE("glowing crystal-rich planet", TERRAIN_ROCK_PURPLE, CRYS_PARSEEN, TERRAIN_ROCK_BLACK, CRYS_PARSEEN, RRS_CRYSTAL),

    METAL_ORANGE("metal-rich planet", TERRAIN_ROCK_ORANGE, TERRAIN_MARS_DIRT, TERRAIN_ROCK_NORMAL, RRS_METAL),
    METAL_BLACK("metallic planet", TERRAIN_ROCK_BLACK, TERRAIN_ROCK_BLACK, TERRAIN_ROCK_NORMAL, RRS_METAL),

    MIST("mistbound world", TERRAIN_ROCK_WHITE, CRYS_PARSEEN, TERRAIN_ROCK_NORMAL, CRYS_PARSEEN, RRS_MIST),

    HOT_VOLCANIC("scalding hot planet", TERRAIN_ROCK_BLACK, TERRAIN_ROCK_RED, TERRAIN_ROCK_NORMAL, RRS_HOT),
    HOT_LAVA_SEA("world of churning lava", TERRAIN_LAVA_ID, TERRAIN_ROCK_MARS, TERRAIN_ROCK_BLACK, RRS_HOT),
    HOT_RED("burning hot world", TERRAIN_ROCK_RED, TERRAIN_MARS_DIRT, TERRAIN_ROCK_NORMAL, RRS_HOT),

    EXTRADIMENSIONAL("extradimensional planetoid", TERRAIN_ROCK_BLACK, TERRAIN_PURPLE_ALIEN_ROCK, CRYS_HATTEL, RRS_EXTRADIMENSIONAL),
    /*TODO*/PROCEDURAL("[ERROR] Procedural description was not retrieved", 598,598,598, V_TERRAN) //All grey hull. Indicates that the ground type material is being asked for when it shouldn't be.
    ;
    public final String description;
    public final short surface;
    public final short soil;
    public final short substrate; //TODO: Maybe ignore this and use random rock tbh lol
    public final short caveBottomLiquid;
    final RRSTerrestrialPlanetType type;
    public TerrainDeco[] decos;

    public static final TerrestrialPlanetGroundType[] COLD_TYPES = {COLD_BLUE,COLD_CRYSTAL,COLD_GLOW,COLD_ICE,COLD_PERMAFROST};
    public static final TerrestrialPlanetGroundType[] TEMPERATE_TYPES = {TEMPERATE_DIRT,TEMPERATE_GRASS,TEMPERATE_STONY,TEMPERATE_WEIRD};
    public static final TerrestrialPlanetGroundType[] HOT_TYPES = {HOT_VOLCANIC,HOT_LAVA_SEA,HOT_RED};
    public static final TerrestrialPlanetGroundType[] ENERGETIC_TYPES = {ENERGETIC_CRYONIC,ENERGETIC_DEAD,ENERGETIC_ODD,ENERGETIC_OVERGROWN,ENERGETIC_STONY,ENERGETIC_WATER};
    public static final TerrestrialPlanetGroundType[] AEGIUM_TYPES = {AEGIUM_BLUE,AEGIUM_PURPLE,AEGIUM_YELLOW};
    public static final TerrestrialPlanetGroundType[] DESERT_TYPES = {DESERT_BLACK,DESERT_ORANGE,DESERT_RED,DESERT_SANDY,DESERT_TAN};
    public static final TerrestrialPlanetGroundType[] BARREN_TYPES = {BARREN_DARK,BARREN_LUNAR,BARREN_WHITE,BARREN_GREEN};
    public static final TerrestrialPlanetGroundType[] CRYSTAL_TYPES = {CRYSTAL_BLACK,CRYSTAL_WHITE,CRYSTAL_PURPLE};
    public static final TerrestrialPlanetGroundType[] METAL_TYPES = {METAL_ORANGE,METAL_BLACK};

    TerrestrialPlanetGroundType(String description, int surface, int soil, int substrate, RRSTerrestrialPlanetType correspondingType) {
        this.description = description;
        this.surface = (short)surface;
        this.soil = (short)soil;
        this.substrate = (short)substrate;
        this.type = correspondingType;
        this.decos = new GeneratorResourcePlugin[]{};
        this.caveBottomLiquid = TERRAIN_LAVA_ID;
    }

    TerrestrialPlanetGroundType(String description, int surface, int soil, int substrate, int caveBottomLiquid, RRSTerrestrialPlanetType correspondingType) {
        this.description = description;
        this.surface = (short)surface;
        this.soil = (short)soil;
        this.substrate = (short)substrate;
        this.type = correspondingType;
        this.decos = new GeneratorResourcePlugin[]{};
        this.caveBottomLiquid = (short) caveBottomLiquid;
    }

    TerrestrialPlanetGroundType(String description, int surface, int soil, int substrate, RRSTerrestrialPlanetType correspondingType, TerrainDeco... decos) {
        this.description = description;
        this.surface = (short)surface;
        this.soil = (short)soil;
        this.substrate = (short)substrate;
        this.type = correspondingType;
        this.decos = decos;
        this.caveBottomLiquid = TERRAIN_LAVA_ID;
    }

    TerrestrialPlanetGroundType(String description, int surface, int soil, int substrate, RRSTerrestrialPlanetType correspondingType, TerrainDeco deco, short... flora) {
        this.description = description;
        this.surface = (short)surface;
        this.soil = (short)soil;
        this.substrate = (short)substrate;
        this.type = correspondingType;
        TerrainDeco[] decoGen = new TerrainDeco[flora.length + 1];
        for(short i = 0; i < flora.length; i++){
            decoGen[i] = new GeneratorFloraPlugin(flora[i], (short)surface, (short)soil);
        }
        decoGen[flora.length] = deco;
        this.decos = decoGen;
        this.caveBottomLiquid = TERRAIN_LAVA_ID;
    }

    TerrestrialPlanetGroundType(String description, int surface, int soil, int substrate, RRSTerrestrialPlanetType correspondingType, short... flora) {
        this.description = description;
        this.surface = (short)surface;
        this.soil = (short)soil;
        this.substrate = (short)substrate;
        this.type = correspondingType;
        TerrainDeco[] floraGen = new TerrainDeco[flora.length];
        for(short i = 0; i < flora.length; i++){
            floraGen[i] = new GeneratorFloraPlugin(flora[i], (short)surface, (short)soil);
        }
        this.decos = floraGen;
        this.caveBottomLiquid = TERRAIN_LAVA_ID;
    }

    TerrestrialPlanetGroundType(String description, int surface, int soil, int substrate, int caveBottomLiquid, RRSTerrestrialPlanetType correspondingType, short... flora) {
        this.description = description;
        this.surface = (short)surface;
        this.soil = (short)soil;
        this.substrate = (short)substrate;
        this.type = correspondingType;
        TerrainDeco[] floraGen = new TerrainDeco[flora.length];
        for(short i = 0; i < flora.length; i++){
            floraGen[i] = new GeneratorFloraPlugin(flora[i], (short)surface, (short)soil);
        }
        this.decos = floraGen;
        this.caveBottomLiquid = (short)caveBottomLiquid;
    }

    //new GeneratorFloraPlugin(ElementKeyMap.TERRAIN_GLOW_TRAP_SPRITE, this.getTop(), this.getFiller());

    @Deprecated
    public static TerrestrialPlanetGroundType randomFromType(RRSTerrestrialPlanetType type, int seed) {
        if(type == V_ICE || type == V_TERRAN || type == V_PURPLE || type == V_RED || type == V_SAND) return VANILLA;
        Random rng = new Random(seed);
        ArrayList<TerrestrialPlanetGroundType> hat = new ArrayList<>();
        for(TerrestrialPlanetGroundType matsSet : values()) if(matsSet.type == type) hat.add(matsSet);
        return hat.get(rng.nextInt(hat.size()));
    }
}
