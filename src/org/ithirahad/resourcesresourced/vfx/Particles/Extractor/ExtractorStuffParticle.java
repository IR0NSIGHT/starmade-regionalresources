package org.ithirahad.resourcesresourced.vfx.Particles.Extractor;

import api.utils.particle.ModParticle;
import com.bulletphysics.linearmath.MiscUtil;
import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

public class ExtractorStuffParticle extends ModParticle {
    //final long creationTime;
    Vector4f startColor = new Vector4f(1F,1F,1F,0F);
    Vector4f endColor = new Vector4f();
    private final float startSize;
    private final float endSize;
    float tPct;
    private Vector3f startLoc;
    private Vector3f endLoc;
    private float initialRotation = 0;
    private boolean firstUpdate = true;

    //TODO: TEMPORARY
    public ExtractorStuffParticle(float startSize, float endSize) {
        this.startSize = 1f;
        this.endSize = 0.05f;
    }

    @Override
    public void spawn() {
        /*
        startColor.set(colorR,colorG,colorB,0f);
        endColor.set(colorR,colorG,colorB,0.5f);
         */
        /*
        startColor.set(colorR,colorG,colorB,1f);
        endColor.set(colorR,colorG,colorB,1f);
         */
    }

    //TODO: TEMPORARY
    @Override
    public void update(long currentTime) {
        sizeOverTime(this, currentTime, startSize, endSize);
        //colorA =  (byte)(127 * getLifetimePercent(currentTime));
        colorA = (byte) (127 * MiscUtils.smoothstep(0,1,getLifetimePercent(currentTime)));
        //colorOverTime(this, currentTime, startColor, endColor);
    }

    /*
    public ExtractorStuffParticle(float startSize, float endSize) {
        this.startSize = startSize;
        this.endSize = endSize;
        startColor.set(colorR,colorG,colorB,0f);
        endColor.set(colorR,colorG,colorB,colorA);
        //creationTime = System.nanoTime();
    }

    @Override
    public void update(long currentTime) {
        if(firstUpdate){
            this.startLoc = new Vector3f(position);
            this.endLoc = new Vector3f(velocity); //Velocity field is used to carry destination position through modparticle protocol
            Vector4f currColor = startColor;
            colorR = (byte) (255 * currColor.x);
            colorG = (byte) (255 * currColor.y);
            colorB = (byte) (255 * currColor.z);
            colorA = (byte) 0;

            //velocity.set(endLoc);
            //velocity.sub(startLoc); //just in case if matters
            //velocity.scale(1f/lifetimeMs);

            velocity.set(0,0,0); //particle position is managed manually
            firstUpdate = false;
        }
        rotate(this, initialRotation+=0.1F);
        tPct = getLifetimePercent(currentTime);

        //float size = MiscUtils.lerp(startSize, endSize, tPct);
        //sizeX = size;
        //sizeY = size;

        colorA = (byte) MiscUtils.lerp(0,160.0f,tPct);
        sizeOverTime(this, currentTime, startSize, endSize);
        //rotate(this, 1.5F+ticksLived/ (float) Math.random() * 240F);
        position.set(MiscUtils.lerp(startLoc, endLoc, tPct));
    }
    */
}
