package org.ithirahad.resourcesresourced.vfx;

import api.utils.particle.ModParticle;
import api.utils.particle.ModParticleUtil;
import api.utils.particle.ModParticleUtil.LoadEvent;
import org.ithirahad.resourcesresourced.ResourcesReSourced;
import org.ithirahad.resourcesresourced.vfx.Particles.Extractor.ExtractorGasParticle;
import org.ithirahad.resourcesresourced.vfx.Particles.Extractor.ExtractorStuffParticle;

import javax.imageio.ImageIO;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.util.Random;

public final class ParticleEffectsManager {
    public static int TX_EXTRACTOR_STUFF;
    public static int TX_EXTRACTOR_GAS;
    public static int TX_SCANNER_RING;

    public static int FX_EXTRACTOR_SPARKYS;
    public static int FX_EXTRACTOR_GAS;
    public static int FX_SCANNER_RING;

    public static float GAS_FX_START_SIZE = 20.0f;
    public static final float GAS_FX_END_SIZE = 2.0f; //should be the size of a block face, 1m x 1m x 1m
    private static boolean init = false;
    private static Random rng = new Random();

    private ParticleEffectsManager(){
        throw new UnsupportedOperationException();
    };

    public static void init(LoadEvent event, ResourcesReSourced instance){
        try {
            TX_EXTRACTOR_STUFF = event.addParticleSprite(ImageIO.read(instance.getJarResource("org/ithirahad/resourcesresourced/assets/image/particle/softgeneric-1x1-c-.png")), instance); //TODO: Placeholder.
            TX_EXTRACTOR_GAS = event.addParticleSprite(ImageIO.read(instance.getJarResource("org/ithirahad/resourcesresourced/assets/image/particle/softgeneric-1x1-c-.png")), instance); //TODO: Placeholder.
            TX_SCANNER_RING = event.addParticleSprite(ImageIO.read(instance.getJarResource("org/ithirahad/resourcesresourced/assets/image/particle/bigring-1x1-c-.png")), instance);
        }
        catch(Exception ex){
            System.err.println("[MOD][Resources ReSourced][ERROR] Unable to retrieve particle sprites!");
            ex.printStackTrace();
            System.err.println("[MOD][Resources ReSourced][WARNING] Particles may be invisible or break the game.");
            TX_EXTRACTOR_STUFF = 0;
            TX_EXTRACTOR_GAS = 0;
            TX_SCANNER_RING = 0;
        }
    }
    public static void spawnExtractorGas(int sectorId, Vector3f pos, Vector4f color, Vector3f velocity, int lifetimeMs){
        //TODO Unused
        //should be proportional to the other particles' size
        ExtractorGasParticle particle = new ExtractorGasParticle(GAS_FX_END_SIZE, GAS_FX_START_SIZE);
        ModParticleUtil.playClient(sectorId, pos, TX_EXTRACTOR_STUFF, particle);
    }
    public static void spawnExtractorSparkys(int sectorId, Vector3f pos, Vector4f color, Vector3f velocity, int lifetimeMs){
        ExtractorStuffParticle particle = new ExtractorStuffParticle(GAS_FX_START_SIZE, GAS_FX_END_SIZE);
        ModParticle.setColorF(particle, color);
        particle.velocity.set(velocity);
        particle.lifetimeMs = lifetimeMs;
        ModParticleUtil.playClient(sectorId, pos, TX_EXTRACTOR_STUFF, particle);
    }
}
