package org.ithirahad.resourcesresourced.vfx;

import api.common.GameServer;
import api.network.packets.PacketUtil;
import api.utils.particle.ModParticle;
import api.utils.particle.ModParticleUtil;
import org.ithirahad.resourcesresourced.RRUtils.RealTimeDelayedAction;
import org.ithirahad.resourcesresourced.network.ScannerFXRemoteExecuteTemporaryPacket;
import org.ithirahad.resourcesresourced.vfx.Particles.ScannerPulseRingParticle;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import static org.ithirahad.resourcesresourced.vfx.ParticleEffectsManager.TX_SCANNER_RING;

public class ScannerPulseEffect{
    public static Vector4f intelColor = new Vector4f(1f,0.87f,0.63f,0.5f);
    public static Vector4f astroColor = new Vector4f(0.1F,0.6F,0.8F,0F);

    public static void FireEffectClient(final int sectorId, Vector3i sector, final Vector3f position, final Vector3f velocity, int diameterMultiplier, final Vector4f color){
        new RealTimeDelayedAction(250, 2, true){
            @Override
            public void doAction() {
                ScannerPulseRingParticle particle = new ScannerPulseRingParticle(1f, 3500.0f, position);
                particle.velocity.set(velocity);
                ModParticle.setColorF(particle, color);
                ModParticleUtil.playClient(sectorId, position, TX_SCANNER_RING, particle);
            }
        }.startCountdown();
    }

    public static void FireAstroEffectServer(final int sectorId, Vector3i sector, final Vector3f position, final Vector3f velocity, int diameterMultiplier){
        ScannerFXRemoteExecuteTemporaryPacket packet = new ScannerFXRemoteExecuteTemporaryPacket(sectorId, sector,position, velocity, diameterMultiplier, astroColor);
        Vector3i delta = new Vector3i();
        for(PlayerState player : GameServer.getServerState().getPlayerStatesByDbId().values()) if(player != null) {
            delta.set(player.getCurrentSector());
            delta.sub(sector);
            if (delta.lengthSquared() <= 4) PacketUtil.sendPacket(player, packet);
        }
    }

    public static void FireIntelEffectServer(final int sectorId, Vector3i sector, final Vector3f position, final Vector3f velocity, int diameterMultiplier){
        ScannerFXRemoteExecuteTemporaryPacket packet = new ScannerFXRemoteExecuteTemporaryPacket(sectorId, sector,position, velocity, diameterMultiplier, intelColor);
        Vector3i delta = new Vector3i();
        for(PlayerState player : GameServer.getServerState().getPlayerStatesByDbId().values()) if(player != null) {
            delta.set(player.getCurrentSector());
            delta.sub(sector);
            if (delta.lengthSquared() <= 4) PacketUtil.sendPacket(player, packet);
        }
    }
}
