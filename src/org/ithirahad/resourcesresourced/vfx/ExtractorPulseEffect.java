package org.ithirahad.resourcesresourced.vfx;

import api.common.GameServer;
import api.network.packets.PacketUtil;
import org.ithirahad.resourcesresourced.RRUtils.RealTimeDelayedAction;
import org.ithirahad.resourcesresourced.network.ExtractorFXRemoteExecuteTemporaryPacket;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.player.PlayerState;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.util.Random;

public class ExtractorPulseEffect {
    static Random rng = new Random();
    static final float ORIGIN_SPREAD_MAX = 30.0f;
    static final float ORIGIN_DISTANCE = 600.0f;
    public static void fireEffectClient(final int sectorId, Vector3i sector, final Vector3f startPosition, final Vector3f endPosition, final Vector4f color){

        new RealTimeDelayedAction(250, 9, true){
            Vector3f offset;
            Vector3f start;
            /*
            Vector3f startCenter;

            {
                startCenter = new Vector3f(endPosition);
                startCenter.sub(startPosition);
                startCenter.normalize();
                startCenter.scale(ORIGIN_DISTANCE);
                startCenter.add(endPosition);
            }
             */

            @Override
            public void doAction() {
                offset = new Vector3f();
                offset.set(nextSignedFloat(),nextSignedFloat(),nextSignedFloat());
                /*
                offset.scale(rng.nextFloat() * ORIGIN_SPREAD_MAX);
                start = new Vector3f(startCenter);
                start.add(offset);
                builder.setVelocity(endPosition); //not the actual velocity; that's managed by the particle
                builder.setLifetime(1500);
                 */

                //TODO: TEMPORARY
                offset.scale(0.1f);

                start = new Vector3f();
                start.set(endPosition);
                start.y += 2;
                start.add(offset);

                Vector3f vel = new Vector3f(0,-1f,0);
                vel.sub(offset);
                vel.scale(0.05f); //TODO wtf is this conversion factor
                //TODO END TEMPORARY

                ParticleEffectsManager.spawnExtractorSparkys(sectorId, start, color, vel, 650);
            }
        }.startCountdown();
    }

    public static void fireEffectServer(final int sectorId, Vector3i sector, final Vector3f startPosition, final Vector3f endPosition, final Vector4f color){
        ExtractorFXRemoteExecuteTemporaryPacket packet = new ExtractorFXRemoteExecuteTemporaryPacket(sectorId,sector,startPosition,endPosition,color);
        Vector3i delta = new Vector3i();
        for(PlayerState player : GameServer.getServerState().getPlayerStatesByDbId().values()) if(player != null) {
            delta.set(player.getCurrentSector());
            delta.sub(sector);
            if (delta.lengthSquared() <= 4) PacketUtil.sendPacket(player, packet);
        }
    }

    /*
    public static void fireEffectServer(final int sectorId, Vector3i sector, final Vector3f startPosition, final Vector3f endPosition, final Vector4f color){
        final ModParticleUtil.Builder builder = new ModParticleUtil.Builder();
        builder.setColor(new Vector4f(color));
        builder.setVelocity(endPosition);

        new RealTimeDelayedAction(250, 9, true){
            Vector3f offset;
            Vector3f start;

            //Vector3f startCenter;

            //{
            //    startCenter = new Vector3f(endPosition);
            //    startCenter.sub(startPosition);
            //    startCenter.normalize();
            //    startCenter.scale(ORIGIN_DISTANCE);
            //    startCenter.add(endPosition);
            //}


            @Override
            public void doAction() {
                offset = new Vector3f();
                offset.set(nextSignedFloat(),nextSignedFloat(),nextSignedFloat());

                //offset.scale(rng.nextFloat() * ORIGIN_SPREAD_MAX);
                //start = new Vector3f(startCenter);
                //start.add(offset);
                //builder.setVelocity(endPosition); //not the actual velocity; that's managed by the particle
                //builder.setLifetime(1500);


                //TODO: TEMPORARY
                offset.scale(0.1f);

                start = new Vector3f();
                start.set(endPosition);
                start.y += 2;
                start.add(offset);

                Vector3f vel = new Vector3f(0,-1f,0);
                vel.sub(offset);
                vel.scale(0.05f); //TODO wtf is this conversion factor
                builder.setVelocity(vel);
                builder.setLifetime(650);
                //TODO END TEMPORARY

                ModParticleUtil.playServer(sectorId, FX_EXTRACTOR_SPARKYS, start, TX_EXTRACTOR_STUFF, builder);
                //also play other extractor FX component
            }
        }.startCountdown();
    }
    */
    static float nextSignedFloat(){
        return (rng.nextFloat() * 2.0f) - 1.0f;
    }
}
