package org.ithirahad.resourcesresourced;

import org.ithirahad.resourcesresourced.industry.RRSRecipeManager;
import org.ithirahad.resourcesresourced.industry.ResourceSourceType;
import org.ithirahad.resourcesresourced.shipsystems.*;
import api.config.BlockConfig;
import api.utils.element.CustomModRefinery;
import api.utils.registry.UniversalRegistry;
import api.utils.textures.StarLoaderTexture;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.schema.game.client.view.cubes.shapes.BlockStyle;
import org.schema.game.common.data.element.ElementCategory;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;
import java.util.HashMap;

import static api.config.BlockConfig.setElementCategory;
import static org.ithirahad.resourcesresourced.industry.RRSRecipeManager.*;
import static api.config.BlockConfig.setBasicInfo;
import static api.utils.registry.UniversalRegistry.RegistryType.ORE;
import static api.utils.registry.UniversalRegistry.RegistryType.PLAYER_USABLE_ID;
import static org.schema.game.common.data.element.ElementKeyMap.*;

public class RRSElementInfoManager {
    public static boolean elementsInitialized = false;
    //-------ID RESERVATIONS
    public static long COMMON_METAL_ORE;
    //use extant metal mesh

    public static long COMMON_CRYSTAL_ORE;
    //use extant crystal composite

    public static long FERRON_ORE;

    public static long AEGIUM_ORE;

    public static long PARSYNE_GAS;
    public static long PARSYNE_CAPSULE;

    public static long THERMYN_FLUID;
    public static long THERMYN_CAPSULE;

    public static long ANBARIC_VAPOR;
    public static long ANBARIC_CAPSULE;
    //...wait, why do the parsyne/thermyn/anbaric ones exist again? lol

    //-------BLOCKCONFIG
    public static HashMap<String, ImmutablePair<ElementInformation, Integer>> oreEntries = new HashMap<>();
    public static HashMap<String, ElementInformation> elementEntries = new HashMap<String, ElementInformation>(){
        @Override
        public ElementInformation get(Object key) {
            ElementInformation e = super.get(key);
            if(e == null) {
                System.err.println("[MOD][Resources ReSourced][WARNING] ISSUE CODE COLUMBIUM ALEPH");
                System.err.println("[MOD][Resources ReSourced][WARNING] Mod attempted to access nonexistent element name: " + key);
                System.err.println("[MOD[Resources ReSourced][WARNING] Please report this if possible.");
            }
            return e;
        }

        @Override
        public ElementInformation put(String key, ElementInformation value) {
            if(value.price >= 1) {
                value.dynamicPrice = 1L;
                value.calculateDynamicPrice();
            }
            return super.put(key, value);
        }
    };

    //-------IMAGE DICTIONARY
    public static int COMMON_METAL_ORE_ICON;
    public static BufferedImage COMMON_METAL_ORE_OVERLAY;
    //use extant metal mesh material

    public static int COMMON_CRYSTAL_ORE_ICON;
    public static BufferedImage COMMON_CRYSTAL_ORE_OVERLAY;
    //use extant crystal composite material

    public static int FERRON_ORE_ICON;
    public static int FERRON_CAPSULE_ICON;
    public static BufferedImage FERRON_ORE_OVERLAY;

    public static int PARSYNE_GAS_ICON;
    public static int PARSYNE_CAPSULE_ICON;

    public static int THERMYN_FLUID_ICON;
    public static int THERMYN_CAPSULE_ICON;

    public static int ANBARIC_VAPOR_ICON;
    public static int ANBARIC_CAPSULE_ICON;

    public static int MACETINE_AGGREGATE_ICON;
    public static int MACETINE_CAPSULE_ICON;

    public static int AEGIUM_ORE_ICON;
    public static BufferedImage AEGIUM_ORE_OVERLAY;

    public static int T0_PLANTSTUFF_ICON;
    public static int T0_ROCK_DUST_ICON;

    public static int T1_PLACEHOLDER_ICON;
    public static int T1_CAPACITOR_ICON;
    public static int T1_CIRCUIT_ICON;
    public static int T1_FOCUS_ICON;
    public static int T1_FRAME_ICON;
    public static int T1_LIGHT_ICON;
    public static int T1_PLATE_ICON;
    public static int T1_SHEET_ICON;

    public static int T2_PLACEHOLDER_ICON;
    public static int T2_CHARGE_ICON;
    public static int T2_CIRCUIT_ICON;
    public static int T2_COIL_ICON;
    public static int T2_EMITTER_ICON;
    public static int T2_FOCUS_ICON;
    public static int T2_MATRIX_ICON;
    public static int T2_PLATING_ICON;
    public static int T2_PROCESSOR_ICON;
    public static int T2_RAIL_ICON;

    public static int T3_PLACEHOLDER_ICON;
    public static int T3_CIRCUIT_ICON;

    //-----ITEM/BLOCK GLOBALS
    //TODO: Config
    public static int RAW_BASIC_RSC_PRICE = 25;
    public static int RAW_RSC_PRICE = 100;
    public static float RAW_BASIC_ORE_VOLUME = 0.015f; //basic ores
    public static float RAW_ORE_VOLUME = 0.015f; //rare ores
    public static float RAW_FLUID_VOLUME = 0.02f; //gas gas gas
    public static float RAW_ORE_MASS = 0.001f;


    public static int BASE_ORE_CAPSULE_CONVERSION_FACTOR = 10; //how many capsules per raw without efficiency chambers
    public static int BASE_LIQUID_CAPSULE_CONVERSION_FACTOR = 2; //how many capsules per liquid without efficiency chambers
    public static int BASE_GAS_CAPSULE_CONVERSION_FACTOR = 1;
    public static int PERSONAL_CAPSULE_CONVERSION_FACTOR = 5; //starter personal converter
    public static float COMMON_RESOURCE_DEADWEIGHT_FACTOR = 2.0f; //how much extra volume the raw resources take up relative to the equivalent amount of capsules
    public static float RARE_RESOURCE_DEADWEIGHT_FACTOR = 3.0f;
    public static int CAPSULE_PRICE = 15;
    public static float CAPSULE_MASS = 0.05f;
    public static float CAPSULE_VOLUME = 0.001f; //default

    public static int T0_COMPONENT_PRICE = 20; //basically junk
    public static float T0_COMPONENT_MASS = 0.001f; //shouldn't be hard to move these around
    public static float COMMON_COMPONENT_VOLUME = 0.001f; //T0, T1
    public static int T1_COMPONENT_PRICE = 150; //todo: balance
    public static float T1_COMPONENT_MASS = 0.04f;
    public static int T2_COMPONENT_PRICE = 500; //todo: balance
    public static float T2_COMPONENT_MASS = 0.05f;
    public static int T3_COMPONENT_PRICE = 1000;
    public static float T3_COMPONENT_MASS = 0.05f;
    public static float RARE_COMPONENT_VOLUME = 0.0075f; //T2+
    public static short COMPONENT_FACTORY_SIDE;
    public static short COMPONENT_FACTORY_CAPS;
    public static short BLOCK_FACTORY_SIDE;
    public static short BLOCK_FACTORY_CAPS;
    public static short COMPONENT_RECYCLER_SIDE;
    public static short COMPONENT_RECYCLER_CAPS;
    public static short BLOCK_RECYCLER_SIDE;
    public static short BLOCK_RECYCLER_CAPS;
    public static short GAS_MINER_SIDE;
    public static short GAS_MINER_CAPS;
    public static short MANTLE_MINER_SIDE;
    public static short MANTLE_MINER_CAPS;

    public static short[] OLD_METALS =
    {
        RESS_ORE_FERTIKEEN,
        RESS_ORE_HITAL,
        RESS_ORE_JISPER,
        RESS_ORE_NACHT,
        RESS_ORE_PARSTUN,
        RESS_ORE_SERTISE,
        RESS_ORE_THRENS,
        RESS_ORE_ZERCANER,
    };

    public static short[] OLD_CRYSTALS = {
        RESS_CRYS_NOCX,
        RESS_CRYS_BASTYN,
        RESS_CRYS_HATTEL,
        RESS_CRYS_MATTISE,
        RESS_CRYS_PARSEN,
        RESS_CRYS_RAMMET,
        RESS_CRYS_SINTYR,
        RESS_CRYS_VARAT
    };

    public static void registerURVs(ResourcesReSourced mod) {
        COMMON_METAL_ORE   = UniversalRegistry.registerURV(ORE, mod.getSkeleton(), "Metallic Ore");
        COMMON_CRYSTAL_ORE = UniversalRegistry.registerURV(ORE, mod.getSkeleton(), "Crystalline Ore");
        FERRON_ORE         = UniversalRegistry.registerURV(ORE, mod.getSkeleton(), "Ferron Ore");
        AEGIUM_ORE         = UniversalRegistry.registerURV(ORE, mod.getSkeleton(), "Aegium Ore");
        //other ores as stopgap???
        UniversalRegistry.registerURV(PLAYER_USABLE_ID, mod.getSkeleton(), ReconAstrometricScannerAddOn.UID_NAME);
    }

    public static void loadElements(ResourcesReSourced mod, BlockConfig config){
        //todo: read config values

        //------PRE CONFIG HOUSEKEEPING/VANILLA CLEANUP

        ElementCategory manufacturingCategory = ElementKeyMap.getInfo(238).type;
        ElementCategory decoCategory = ElementKeyMap.getInfo(DECORATIVE_PANEL_1).type;
        ElementCategory nonCubicCategory = ElementKeyMap.getInfo(976).type;

        ElementCategory factoryCategory = ElementKeyMap.getInfo(211).type;
        ElementCategory rawMetalCategory = ElementKeyMap.getInfo(RESS_ORE_NACHT).type;
        ElementCategory capsuleCategory = ElementKeyMap.getInfo(150).type;
        ElementCategory componentCategory = BlockConfig.newElementCategory(manufacturingCategory,"Components");

        System.err.println("[MOD][ResourcesReSourced] Cleaning up vanilla block data...");
        for(ElementInformation info : getInfoArray()) {
            if(info != null) { //...huh.
                if (sameCatAs(info, rawMetalCategory)) {
                    getCategoryHirarchy().removeRecursive(info);
                    nameTranslations.remove(info.id);
                    descriptionTranslations.remove(info.id);
                    info.deprecated = true;
                    info.shoppable = false;
                    info.description = "An old chunk of raw material, harvested in such a way as to make it completely unusable for manufacturing or reprocessing by modern methods. Neat-looking, but useless.";
                    //TODO: Change to 'dead' texture and allow reprocessing into alloy/crystal?
                } else if (info.id != 213 && info.id != 212 && sameCatAs(info, factoryCategory) && !info.deprecated) { //all old factories what aren't the capsule refinery or factory enhancer
                    info.canActivate = false;
                    getCategoryHirarchy().removeRecursive(info);
                    setElementCategory(info,decoCategory);
                    nameTranslations.remove(info.id);
                    descriptionTranslations.remove(info.id);
                    info.name = "Defunct Factory (Type " + info.id + ")";
                    info.description = "A piece of old machinery. Appears to have at one point been used in manufacturing various items and modules via some convoluted process. Neat-looking, but useless.";
                    oldFactories.add(info);
                    //TODO: Dedicated subcategory for these semideprecated things to avoid problems on reboot
                } else if (sameCatAs(info, capsuleCategory)) {
                    getCategoryHirarchy().removeRecursive(info);
                    setElementCategory(info,nonCubicCategory);
                    //getCategoryHirarchy().insertRecusrive(info);
                    nameTranslations.remove(info.id);
                    info.name = "Decorative Replica Capsule (" + info.name + ")";
                    descriptionTranslations.remove(info.id);
                    info.description = "A crystal capsule containing a replica of some refined material historically used in manufacturing. Neat-looking, but useless.";
                    oldCapsules.add(info);
                } else if (sameCatAs(info,manufacturingCategory)){
                    descriptionTranslations.remove(info.id);
                    if(info.id != 546 && info.id != 547){
                        getCategoryHirarchy().removeRecursive(info);
                        setElementCategory(info,nonCubicCategory);
                        //getCategoryHirarchy().insertRecusrive(info);
                        nameTranslations.remove(info.id);
                        info.name = "Old Industrial Supplies (Type " + info.id + ")";
                        info.description = "Some supplies for outdated manufacturing processes. Neat-looking, but useless.";
                        oldCapsules.add(info);
                    } else {
                        ElementKeyMap.descriptionTranslations.remove(info.id);
                        info.description = "Scrap from degraded structures. Can be refined into useful material via the Capsule Refinery.";
                    }
                }
            }
        }
        //TODO: Rename metal/crystal ingots/crystals

        //------PREP STUFF

        RAW_BASIC_ORE_VOLUME = CAPSULE_VOLUME * BASE_ORE_CAPSULE_CONVERSION_FACTOR * COMMON_RESOURCE_DEADWEIGHT_FACTOR;
        RAW_ORE_VOLUME = CAPSULE_VOLUME * BASE_ORE_CAPSULE_CONVERSION_FACTOR * RARE_RESOURCE_DEADWEIGHT_FACTOR;
        RAW_FLUID_VOLUME = CAPSULE_VOLUME * BASE_LIQUID_CAPSULE_CONVERSION_FACTOR * RARE_RESOURCE_DEADWEIGHT_FACTOR;

        try { //get images
            COMMON_METAL_ORE_ICON      = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Metal_Ore_Raw.png"))).getTextureId(); //TODO: multicolor rust/grey ore
            COMMON_METAL_ORE_OVERLAY   = ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/ore/Metal_Overlay.png"));
            COMMON_CRYSTAL_ORE_ICON    = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Crystal_Ore_Raw.png"))).getTextureId(); //TODO: crystal cluster icon
            COMMON_CRYSTAL_ORE_OVERLAY = ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/ore/Crystal_Overlay.png"));

            FERRON_ORE_ICON            = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Ferron_Ore_Raw.png"))).getTextureId();
            FERRON_ORE_OVERLAY         = ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/ore/Ferron_Overlay.png"));

            AEGIUM_ORE_ICON            = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Aegium_Shard_Raw.png"))).getTextureId();
            AEGIUM_ORE_OVERLAY         = ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/ore/Rammet_Overlay.png"));

            PARSYNE_GAS_ICON           = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Parsyne_Gas_Raw.png"))).getTextureId();
            ANBARIC_VAPOR_ICON         = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Anbaric_Plasma_Raw.png"))).getTextureId();
            THERMYN_FLUID_ICON         = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Thermyn_Fluid_Raw.png"))).getTextureId();
            MACETINE_AGGREGATE_ICON    = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Macetine_Aggregate_Raw.png"))).getTextureId();

            T0_PLANTSTUFF_ICON         = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_T0_Protomaterial.png"))).getTextureId();
            T0_ROCK_DUST_ICON          = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_T0_Rockdust.png"))).getTextureId();

            T1_PLACEHOLDER_ICON        = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_Placeholder_T1.png"))).getTextureId();
            T1_CAPACITOR_ICON          = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_Placeholder_T1_Cap.png"))).getTextureId();
            T1_CIRCUIT_ICON            = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_Placeholder_T1_Circuit.png"))).getTextureId();
            T1_FOCUS_ICON              = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_Placeholder_T1_Focus.png"))).getTextureId();
            T1_FRAME_ICON              = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_Placeholder_T1_Frame.png"))).getTextureId();
            T1_LIGHT_ICON              = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_Placeholder_T1_Light.png"))).getTextureId();
            T1_PLATE_ICON              = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_Placeholder_T1_Plate_Crs.png"))).getTextureId();
            T1_SHEET_ICON              = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_Placeholder_T1_Plate.png"))).getTextureId();

            T2_PLACEHOLDER_ICON        = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_Placeholder_T2.png"))).getTextureId();
            T2_CHARGE_ICON             = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_T2_Charge.png"))).getTextureId();
            T2_CIRCUIT_ICON            = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_T2_Circuit.png"))).getTextureId();
            T2_COIL_ICON               = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_T2_Coil.png"))).getTextureId();
            T2_EMITTER_ICON            = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_T2_Emitter.png"))).getTextureId();
            T2_FOCUS_ICON              = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_T2_Focus.png"))).getTextureId();
            T2_MATRIX_ICON             = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_T2_Matrix.png"))).getTextureId();
            T2_PLATING_ICON            = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_T2_Plating.png"))).getTextureId();
            T2_PROCESSOR_ICON          = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_T2_Processor.png"))).getTextureId();
            T2_RAIL_ICON               = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_T2_Rail.png"))).getTextureId();

            T3_PLACEHOLDER_ICON        = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_Placeholder_T3.png"))).getTextureId();
            T3_CIRCUIT_ICON            = StarLoaderTexture.newIconTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/item/Component_T3_Circuit.png"))).getTextureId();

            COMPONENT_FACTORY_SIDE     = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/block/component-fabricator-front.png"))).getTextureId();
            COMPONENT_FACTORY_CAPS     = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/block/component-fabricator-caps.png"))).getTextureId();
            BLOCK_FACTORY_SIDE         = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/block/block-assembler-front.png"))).getTextureId();
            BLOCK_FACTORY_CAPS         = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/block/block-assembler-caps.png"))).getTextureId();

            COMPONENT_RECYCLER_SIDE    = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/block/component-recycler-front.png"))).getTextureId();
            COMPONENT_RECYCLER_CAPS    = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/block/component-recycler-caps.png"))).getTextureId();
            BLOCK_RECYCLER_SIDE        = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/block/block-disassembler-front.png"))).getTextureId();
            BLOCK_RECYCLER_CAPS        = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/block/block-disassembler-caps.png"))).getTextureId();

            GAS_MINER_SIDE             = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/block/vapor-siphon-front.png"))).getTextureId();
            GAS_MINER_CAPS             = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/block/vapor-siphon-caps.png"))).getTextureId();
            MANTLE_MINER_SIDE          = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/block/magmatic-extractor-front.png"))).getTextureId();
            MANTLE_MINER_CAPS          = (short)StarLoaderTexture.newBlockTexture(ImageIO.read(mod.getJarResource("org/ithirahad/resourcesresourced/assets/image/block/magmatic-extractor-caps.png"))).getTextureId();


        } catch (Exception e) {
            System.err.println("[MOD][RRS] Error retrieving internal image assets!");
            e.printStackTrace();
        }
        //------CHAMBERS

        ElementInformation astrometricScanner = BlockConfig.newChamber(mod,"Astrometric Scanner Base", REACTOR_CHAMBER_SCANNER);
        astrometricScanner.chamberCapacity = 0.10f;
        astrometricScanner.setTextureId(ElementKeyMap.getInfo(9).getTextureIds());
        astrometricScanner.setDescription("The Astrometric Scanner provides information about your location and surroundings in the universe. Base functionality allows you to scan your current system block, yielding general information about the zone and system your are located in.");
        elementEntries.put("Astrometric Scanner Chamber",astrometricScanner);
        BlockConfig.add(astrometricScanner);

        ElementInformation prospectingScanner = BlockConfig.newChamber(mod, "Sector Prospecting Scanner", astrometricScanner.id);
        prospectingScanner.chamberRoot = REACTOR_CHAMBER_SCANNER;
        prospectingScanner.chamberCapacity = 0.04f;
        prospectingScanner.setTextureId(ElementKeyMap.getInfo(9).getTextureIds());
        prospectingScanner.setDescription("Allows you to use your Astrometric Scanner to survey your current sector for passively extractable resources.");
        elementEntries.put("Sector Prospecting Scanner Chamber", prospectingScanner);
        BlockConfig.add(prospectingScanner);

        ElementInformation planetScanner = BlockConfig.newChamber(mod, "Planetary Scanner", astrometricScanner.id);
        planetScanner.chamberRoot = REACTOR_CHAMBER_SCANNER;
        planetScanner.chamberCapacity = 0.02f;
        planetScanner.setTextureId(ElementKeyMap.getInfo(9).getTextureIds());
        planetScanner.setDescription("Allows your Astrometric Scanner to read the types and resources of all major celestial bodies which are present in a system.");
        elementEntries.put("Planetary Scanner Chamber", planetScanner);
        BlockConfig.add(planetScanner);

        ElementInformation mapScanner = BlockConfig.newChamber(mod, "Stellar Cartography System", astrometricScanner.id);
        mapScanner.chamberRoot = REACTOR_CHAMBER_SCANNER;
        mapScanner.chamberCapacity = 0.06f;
        mapScanner.setTextureId(ElementKeyMap.getInfo(9).getTextureIds());
        mapScanner.setDescription("Allows you to use your Astrometric Scanner to output the local system layout to the map.");
        elementEntries.put("Mapping Scanner Chamber", mapScanner);
        BlockConfig.add(mapScanner);
        //TODO: focal points mechanic for scanning unexplored systems - generate two or three random points outside the heat zone to fly to and scan within a sector or two
        // (then double (or more) the sector count for manual scan)


        //------FACTORIES
        ElementInformation vanillaFactory = getInfo(ElementKeyMap.FACTORY_BASIC_ID);
        short[] siphonTextures        = {GAS_MINER_SIDE,GAS_MINER_SIDE,GAS_MINER_CAPS,GAS_MINER_CAPS,GAS_MINER_SIDE,GAS_MINER_SIDE};
        short[] extractorTextures     = {MANTLE_MINER_SIDE,MANTLE_MINER_SIDE,MANTLE_MINER_CAPS,MANTLE_MINER_CAPS,MANTLE_MINER_SIDE,MANTLE_MINER_SIDE};
        short[] componentFabTextures  = {COMPONENT_FACTORY_SIDE,COMPONENT_FACTORY_SIDE,COMPONENT_FACTORY_CAPS,COMPONENT_FACTORY_CAPS,COMPONENT_FACTORY_SIDE,COMPONENT_FACTORY_SIDE};
        short[] blockAsmTextures      = {BLOCK_FACTORY_SIDE,BLOCK_FACTORY_SIDE,BLOCK_FACTORY_CAPS,BLOCK_FACTORY_CAPS,BLOCK_FACTORY_SIDE,BLOCK_FACTORY_SIDE};
        short[] blockRecyclerTextures = {BLOCK_RECYCLER_SIDE,BLOCK_RECYCLER_SIDE,BLOCK_RECYCLER_CAPS,BLOCK_RECYCLER_CAPS,BLOCK_RECYCLER_SIDE,BLOCK_RECYCLER_SIDE}; //mine layer bay texture? //TODO: piece this together with aux/etc
        short[] compoRecyclerTextures = {COMPONENT_RECYCLER_SIDE,COMPONENT_RECYCLER_SIDE,COMPONENT_RECYCLER_CAPS,COMPONENT_RECYCLER_CAPS,COMPONENT_RECYCLER_SIDE,COMPONENT_RECYCLER_SIDE};

        ElementInformation vaporSiphon = BlockConfig.newFactory(mod, "Vapor Siphon", siphonTextures);
        setBasicInfo(vaporSiphon, "A siphon module capable of absorbing ambient vapors and plasmas from its environment. When placed on a station adjacent to a gas giant, or in the same system as a star emitting Parsyne stellar wind, it will steadily harvest a certain amount of available resources per cycle. You can harvest more per cycle by connecting Factory Enhancers.",
                (int)vanillaFactory.price, vanillaFactory.mass, vanillaFactory.placable, vanillaFactory.canActivate, getInfo(ElementKeyMap.SHIELD_DRAIN_MODULE_ID).getBuildIconNum());
        vaporSiphon.maxHitPointsFull = vanillaFactory.maxHitPointsFull;
        vaporSiphon.volume = vanillaFactory.volume;
        vaporSiphon.shoppable = true;
        vaporSiphon.canActivate = true;
        vaporSiphon.type = factoryCategory;
        //TODO: one per entity?
        //TODO: Station/planet only
        BlockConfig.add(vaporSiphon);
        elementEntries.put("Vapor Siphon",vaporSiphon);
        ResourceSourceType.SPACE.setExtractorBlockID(vaporSiphon.getId());

        ElementInformation magmaticExtractor = BlockConfig.newFactory(mod,"Magmatic Extractor", extractorTextures);
        /*
         -Extracts small amounts metal/crystal of from generic planets
         -Extracts large amounts of metal or crystal from metal or crystal planets
         -Extracts aegium from aegium planets
         -Extracts ferron from ferron planets
         -Extracts thermyn from thermyn planets
         -TODO: Asteroids??
         */
        setBasicInfo(magmaticExtractor, "An extractor that uses short-range teleportation technology to filter raw resources out of a planet's mantle. Capable of harvesting Thermyn Amalgam, as well as metallic and crystalline ores.",
                (int)vanillaFactory.price, vanillaFactory.mass, vanillaFactory.placable, vanillaFactory.canActivate, getInfo(ElementKeyMap.SHIELD_SUPPLY_MODULE_ID).getBuildIconNum());
        magmaticExtractor.maxHitPointsFull = vanillaFactory.maxHitPointsFull;
        magmaticExtractor.volume = vanillaFactory.volume;
        magmaticExtractor.shoppable = true;
        magmaticExtractor.canActivate = true;
        magmaticExtractor.type = factoryCategory;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(magmaticExtractor);
        //TODO: one per entity?
        //TODO: Station/planet only
        BlockConfig.add(magmaticExtractor);
        elementEntries.put("Magmatic Extractor",magmaticExtractor);
        ResourceSourceType.GROUND.setExtractorBlockID(magmaticExtractor.getId());


        ElementInformation componentFab = BlockConfig.newFactory(mod, "Component Fabricator", componentFabTextures);
        setBasicInfo(componentFab, "A factory machine that can convert capsules of refined resources into various Components which are needed to assemble blocks and ships.",
                (int)vanillaFactory.price, vanillaFactory.mass, vanillaFactory.placable, vanillaFactory.canActivate, getInfo(ElementKeyMap.FACTORY_MICRO_ASSEMBLER_ID).getBuildIconNum());
        componentFab.maxHitPointsFull = vanillaFactory.maxHitPointsFull;
        componentFab.volume = vanillaFactory.volume;
        componentFab.shoppable = true;
        componentFab.canActivate = true;
        componentFab.type = factoryCategory;
        //TODO: one per entity?
        //TODO: Station/planet only
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(componentFab);
        BlockConfig.add(componentFab);
        elementEntries.put("Component Fabricator",componentFab);

        ElementInformation blockAssembler = BlockConfig.newFactory(mod, "Block Assembler", blockAsmTextures);
        setBasicInfo(blockAssembler, "A factory machine that can assemble blocks from Components.",
                (int)vanillaFactory.price, vanillaFactory.mass, vanillaFactory.placable, vanillaFactory.canActivate, getInfo(ElementKeyMap.FACTORY_STANDARD_ID).getBuildIconNum());
        blockAssembler.maxHitPointsFull = vanillaFactory.maxHitPointsFull;
        blockAssembler.volume = vanillaFactory.volume;
        blockAssembler.shoppable = true;
        blockAssembler.canActivate = true;
        blockAssembler.type = factoryCategory;
        //TODO: one per entity?
        //TODO: Station/planet only
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(blockAssembler);
        BlockConfig.add(blockAssembler);
        elementEntries.put("Block Assembler",blockAssembler);

        ElementInformation blockDisassembler = BlockConfig.newRefinery(mod, "Block Disassembler", blockRecyclerTextures, new CustomModRefinery(blockDisassemblyRecipes,"Block Disassembler","Disassembling Blocks into Components...",BLOCK_DISASSEMBLY_BAKE_TIME));
        setBasicInfo(blockDisassembler, "A factory machine that will break blocks into their constituent Components.",
                (int)vanillaFactory.price, vanillaFactory.mass, vanillaFactory.placable, vanillaFactory.canActivate, getInfo(363).getBuildIconNum());
        blockDisassembler.maxHitPointsFull = vanillaFactory.maxHitPointsFull;
        blockDisassembler.volume = vanillaFactory.volume;
        blockDisassembler.shoppable = true;
        blockDisassembler.canActivate = true;
        blockDisassembler.type = factoryCategory;
        //TODO: one per entity?
        //TODO: Station/planet only
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(blockDisassembler);
        BlockConfig.add(blockDisassembler);
        elementEntries.put("Block Disassembler",blockDisassembler);

        ElementInformation componentRecycler = BlockConfig.newRefinery(mod, "Component Recycler", compoRecyclerTextures, new CustomModRefinery(componentRecyclingRecipes, "Component Recycler", "Recycling Components for raw resources...", COMPONENT_RECYCLE_BAKE_TIME));
        setBasicInfo(componentRecycler, "A factory machine that disintegrates Components, reducing them to raw materials. This process is not perfectly efficient, and will sometimes yield common scrap rather than all of the component's materials.",
                (int)vanillaFactory.price, vanillaFactory.mass, vanillaFactory.placable, vanillaFactory.canActivate, getInfo(978).getBuildIconNum());
        componentRecycler.maxHitPointsFull = vanillaFactory.maxHitPointsFull;
        componentRecycler.volume = vanillaFactory.volume;
        componentRecycler.shoppable = true;
        componentRecycler.canActivate = true;
        componentRecycler.type = factoryCategory;
        //TODO: one per entity?
        //TODO: Station/planet only
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(componentRecycler);
        BlockConfig.add(componentRecycler);
        elementEntries.put("Component Recycler",componentRecycler);
        //TODO: Migrate these to the new custom refinery system

        ElementInformation vaporPlaceholder = BlockConfig.newElement(mod,"Gas Giant Planets & Stars",(short)1);
        vaporPlaceholder.volume = 0.00001f;
        vaporPlaceholder.shoppable = false;
        vaporPlaceholder.deprecated = true;
        vaporPlaceholder.placable = false;
        vaporPlaceholder.buildIconNum = 0;
        BlockConfig.add(vaporPlaceholder);
        ElementInformation magmaticPlaceholder = BlockConfig.newElement(mod, "Terrestrial Planets",(short)1); //& asteroids?
        magmaticPlaceholder.volume = 0.00001f;
        magmaticPlaceholder.shoppable = false;
        magmaticPlaceholder.deprecated = true;
        magmaticPlaceholder.placable = false;
        magmaticPlaceholder.buildIconNum = 0;
        BlockConfig.add(magmaticPlaceholder);
        ElementInformation vOutputPlaceholder = BlockConfig.newElement(mod, "Extract Gaseous Resources",(short)1);
        vOutputPlaceholder.volume = 0.00001f;
        vOutputPlaceholder.shoppable = false;
        vOutputPlaceholder.deprecated = true;
        vOutputPlaceholder.placable = false;
        vOutputPlaceholder.buildIconNum = 0;
        BlockConfig.add(vOutputPlaceholder);
        ElementInformation mOutputPlaceholder = BlockConfig.newElement(mod, "Extract Terrestrial Resources",(short)1);
        mOutputPlaceholder.volume = 0.00001f;
        mOutputPlaceholder.shoppable = false;
        mOutputPlaceholder.deprecated = true;
        mOutputPlaceholder.placable = false;
        mOutputPlaceholder.buildIconNum = 0;
        BlockConfig.add(mOutputPlaceholder); //I hate to waste ID's but the factory system made me do it :(
        //TODO: Just override the factory recipe dialogue windows somehow, instead of doing this D:

        BlockConfig.addRecipe(vOutputPlaceholder, BlockConfig.customFactories.get(vaporSiphon.id), HARVESTER_EXTRACTION_CYCLES, fr(1, vaporPlaceholder.id));
        BlockConfig.addRecipe(mOutputPlaceholder, BlockConfig.customFactories.get(magmaticExtractor.id), HARVESTER_EXTRACTION_CYCLES, fr(1, magmaticPlaceholder.id));

        //------ORES AND RESOURCES

        short vanillaBlueMetalCap   = (short) getInfo(142).buildIconNum; //hylat
        short vanillaGoldBar        = (short) getInfo(343).buildIconNum; //gold bar

        //TODO: category assignment for capsules

        ImmutablePair<ElementInformation, Integer> metalOre = BlockConfig.newOre(mod, "Metallic Ore", (short) COMMON_METAL_ORE_ICON, COMMON_METAL_ORE_OVERLAY);
        setBasicInfo(metalOre.left, "A common metallic ore, which can be refined into useful metal mesh.",
                RAW_BASIC_RSC_PRICE, RAW_ORE_MASS, false, false, (short) COMMON_METAL_ORE_ICON);
        metalOre.left.volume = RAW_BASIC_ORE_VOLUME;
        metalOre.left.type = rawMetalCategory;
        metalOre.left.shoppable = true;
        metalOre.left.setBlockStyle(BlockStyle.SPRITE.id);
        metalOre.left.blended = true;
        ElementKeyMap.getCategoryHirarchy().insertRecusrive(metalOre.left);
        BlockConfig.add(metalOre.left);
        oreEntries.put("Metallic Ore", metalOre);
        elementEntries.put("Metallic Ore", metalOre.left);

        ElementInformation metalMesh = getInfo(ElementKeyMap.METAL_MESH);
        //ElementKeyMap.nameTranslations.remove(metalMesh.id);
        descriptionTranslations.remove(metalMesh.id);
        metalMesh.description = "A common alloyed metal mesh. Extremely useful in manufacturing most types of mechanical components.";

        ImmutablePair<ElementInformation, Integer> crystalOre = BlockConfig.newOre(mod, "Crystalline Ore", (short) COMMON_CRYSTAL_ORE_ICON, COMMON_CRYSTAL_ORE_OVERLAY); //TODO: images
        setBasicInfo(crystalOre.left, "A common type of impure crystal, which can be refined into useful crystal composite.",
                RAW_BASIC_RSC_PRICE, RAW_ORE_MASS, false, false, (short) COMMON_CRYSTAL_ORE_ICON);
        crystalOre.left.volume = RAW_BASIC_ORE_VOLUME;
        crystalOre.left.type = rawMetalCategory;
        crystalOre.left.shoppable = true;
        crystalOre.left.setBlockStyle(BlockStyle.SPRITE.id);
        crystalOre.left.blended = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(crystalOre.left);
        BlockConfig.add(crystalOre.left);
        oreEntries.put("Crystalline Ore", crystalOre);
        elementEntries.put("Crystalline Ore", crystalOre.left);

        ElementInformation crystalComp = getInfo(ElementKeyMap.CRYSTAL_CRIRCUITS);
        //ElementKeyMap.nameTranslations.remove(crystalComp.id);
        descriptionTranslations.remove(crystalComp.id);
        crystalComp.description = "A common bulk crystalline composite. Extremely useful in manufacturing most types of technological components.";

        ImmutablePair<ElementInformation, Integer> ferronOre = BlockConfig.newOre(mod, "Ferron Ore", (short) FERRON_ORE_ICON, FERRON_ORE_OVERLAY);
        setBasicInfo(ferronOre.left, "An uncommon ore, formed at extremely high temperatures in certain star systems. Can be refined into pure Ferron metal, used in advanced sensors.",
                RAW_RSC_PRICE, RAW_ORE_MASS, false, false, (short) FERRON_ORE_ICON);
        ferronOre.left.volume = RAW_ORE_VOLUME;
        ferronOre.left.type = rawMetalCategory;
        ferronOre.left.shoppable = true;
        ferronOre.left.setBlockStyle(BlockStyle.SPRITE.id);
        ferronOre.left.blended = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(ferronOre.left);
        BlockConfig.add(ferronOre.left);
        oreEntries.put("Ferron Ore", ferronOre);
        elementEntries.put("Ferron Ore", ferronOre.left);

        ElementInformation ferronCapsule = BlockConfig.newElement(mod, "Ferron Capsule", getInfo(150).getTextureIds());
        setCapsuleInfo(ferronCapsule, "A pure metal with a high melting point, capable of conducting exotic energies. Used in stealth and sensor systems.", (short)150);
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(ferronCapsule);

        ImmutablePair<ElementInformation, Integer> aegiumOre = BlockConfig.newOre(mod, "Aegium Ore", (short) AEGIUM_ORE_ICON, AEGIUM_ORE_OVERLAY);
        setBasicInfo(aegiumOre.left,"Impure shards of an uncommon crystal that magnifies energy fields passing through it. Can be refined into pure Aegium, used in many advanced energy field technologies.",
                RAW_RSC_PRICE, RAW_ORE_MASS,false,false, (short) AEGIUM_ORE_ICON);
        aegiumOre.left.name = "Aegium Shards";
        aegiumOre.left.volume = RAW_ORE_VOLUME;
        aegiumOre.left.type = rawMetalCategory;
        aegiumOre.left.shoppable = true;
        aegiumOre.left.setBlockStyle(BlockStyle.SPRITE.id);
        aegiumOre.left.blended = true;
        oreEntries.put("Aegium Ore", aegiumOre);
        elementEntries.put("Aegium Ore", aegiumOre.left);
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(aegiumOre.left);
        BlockConfig.add(aegiumOre.left);

        ElementInformation aegiumCapsule = BlockConfig.newElement(mod, "Aegium Capsule", getInfo(174).getTextureIds());
        setCapsuleInfo(aegiumCapsule, "A pure crystalline material with the ability to tune, shape, and amplify energy fields. Useful in shielding, teleporters, and other devices that affect things from a distance.", (short)174);
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(aegiumCapsule);

        ElementInformation thermynRaw = BlockConfig.newElement(mod, "Thermyn Amalgam", (short) THERMYN_FLUID);
        setBasicInfo(thermynRaw, "An uncommon raw substance with slightly volatile properties. Can be refined into pure Thermyn, used in highly energetic devices.",
                RAW_RSC_PRICE, RAW_ORE_MASS, false, false, THERMYN_FLUID_ICON);
        thermynRaw.volume = RAW_FLUID_VOLUME;
        thermynRaw.type = rawMetalCategory;
        thermynRaw.shoppable = true;
        thermynRaw.setBlockStyle(BlockStyle.SPRITE.id);
        thermynRaw.blended = true;
        elementEntries.put("Thermyn Amalgam", thermynRaw);
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(thermynRaw);
        BlockConfig.add(thermynRaw);

        ElementInformation thermynCapsule = BlockConfig.newElement(mod, "Thermyn Capsule", (short)THERMYN_CAPSULE);
        setCapsuleInfo(thermynCapsule, "A purified, volatile liquid that seems almost organic. Used in highly energetic devices.", (short)142);

        ElementInformation parsyneRaw = BlockConfig.newElement(mod, "Parsyne Plasma", (short) PARSYNE_GAS);
        setBasicInfo(parsyneRaw,"An uncommon, very dense gas condensed from stellar wind. Can be refined into pure parsyne, useful in matter- and energy- manipulating beams.",
                RAW_RSC_PRICE, RAW_ORE_MASS,false,false,PARSYNE_GAS_ICON);
        parsyneRaw.volume = RAW_FLUID_VOLUME;
        parsyneRaw.type = rawMetalCategory;
        parsyneRaw.shoppable = true;
        parsyneRaw.setBlockStyle(BlockStyle.SPRITE.id);
        parsyneRaw.blended = true;
        elementEntries.put("Parsyne Plasma", parsyneRaw);
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(parsyneRaw);
        BlockConfig.add(parsyneRaw);

        ElementInformation parsyneCapsule = BlockConfig.newElement(mod, "Parsyne Capsule", (short) PARSYNE_CAPSULE);
        setCapsuleInfo(parsyneCapsule, "An pure crystalline condensate, refined from Parsyne Plasma. Has matter and energy-focusing properties that make it useful for advanced beams.", (short)158);
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(parsyneCapsule);

        ElementInformation anbaricRaw = BlockConfig.newElement(mod, "Anbaric Vapor", (short) ANBARIC_VAPOR);
        setBasicInfo(anbaricRaw,"An uncommon form of matter that seems to defy the laws of physics, increasing the weight of any container holding it but yet repelling against gravity when allowed to float free. Can be refined into a pure form, which is used to create devices that manipulate spacetime and gravity.",
                RAW_RSC_PRICE, RAW_ORE_MASS,false,false, ANBARIC_VAPOR_ICON);
        anbaricRaw.volume = RAW_FLUID_VOLUME;
        anbaricRaw.type = rawMetalCategory;
        anbaricRaw.shoppable = true;
        anbaricRaw.setBlockStyle(BlockStyle.SPRITE.id);
        anbaricRaw.blended = true;
        elementEntries.put("Anbaric Vapor",anbaricRaw);
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(anbaricRaw);
        BlockConfig.add(anbaricRaw);

        ElementInformation anbaricCapsule = BlockConfig.newElement(mod,"Anbaric Capsule", (short) ANBARIC_CAPSULE);
        setCapsuleInfo(anbaricCapsule, "An purified plasma that has unique effects on space and time. Useful in gravity and momentum manipulation, teleportation, and faster-than-light travel.", (short)194);
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(anbaricCapsule);

        ElementInformation macetineRaw = BlockConfig.newElement(mod, "Macetine Aggregate", getInfo(ElementKeyMap.RESS_ORE_NACHT).getTextureIds());
                //getInfo(ElementKeyMap.RESS_ORE_NACHT); //Macet Ore
        //macetRaw.name = "Macet Aggregate";
        setBasicInfo(macetineRaw, "A black, slightly luminescent substance generated as a byproduct when refining most raw resources. Can be refined into pure Macetine metal, used in armor.",
                RAW_RSC_PRICE, RAW_ORE_MASS, false, false, MACETINE_AGGREGATE_ICON);
        macetineRaw.volume = RAW_ORE_VOLUME;
        macetineRaw.type = rawMetalCategory;
        macetineRaw.shoppable = true;
        macetineRaw.setBlockStyle(BlockStyle.SPRITE.id);
        macetineRaw.blended = true;
        elementEntries.put("Macetine Aggregate", macetineRaw);
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(macetineRaw);
        BlockConfig.add(macetineRaw);

        ElementInformation macetineCapsule = BlockConfig.newElement(mod,"Macetine Capsule", getInfo(202).getTextureIds());
                //getInfo(202);
        setCapsuleInfo(macetineCapsule, "An unusually light but strong black metal which can be refined from industrial byproducts and is highly resistant to damage. Used in armor plating.", (short)202);
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(macetineCapsule);

        ElementInformation thrensRaw = BlockConfig.newElement(mod, "Threns Relic", getInfo(343).getTextureIds());
        setBasicInfo(thrensRaw, "A relic of a past civilization, made from precious and energy-dense Threns metal. It is impure, but if refined, the metal could be useful in creating astronaut gear.",
                RAW_RSC_PRICE * 2, RAW_ORE_MASS, false, false, vanillaGoldBar);
        thrensRaw.volume = RAW_ORE_VOLUME;
        thrensRaw.type = rawMetalCategory;
        thrensRaw.setBlockStyle(BlockStyle.SPRITE.id);
        thrensRaw.blended = true;
        //thrensRaw.shoppable = true;
        elementEntries.put("Threns Relic", thrensRaw);
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(thrensRaw);
        BlockConfig.add(thrensRaw);

        ElementInformation thrensCapsule = BlockConfig.newElement(mod,"Threns Capsule", getInfo(154).getTextureIds());
                //getInfo(154);
        setCapsuleInfo(thrensCapsule, "A shiny, golden metal alloy with exceptional energy density. Threns can be found in the artifacts of long-lost civilizations, and is stockpiled by the various factions of the galaxy, but the method to produce this metallic material from scratch are long forgotten. Used in personal gear technology.", (short)154);
        //thrensCapsule.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(thrensCapsule);

        elementEntries.put("Ferron Capsule", ferronCapsule); //TODO: substitute custom image
        BlockConfig.add(ferronCapsule);

        elementEntries.put("Aegium Capsule", aegiumCapsule);
        BlockConfig.add(aegiumCapsule);

        elementEntries.put("Thermyn Capsule", thermynCapsule); //TODO: substitute permanent image
        BlockConfig.add(thermynCapsule);

        elementEntries.put("Parsyne Capsule",parsyneCapsule); //TODO: substitute permanent image
        BlockConfig.add(parsyneCapsule);

        elementEntries.put("Anbaric Capsule",anbaricCapsule); //TODO: substitute permanent image
        BlockConfig.add(anbaricCapsule);

        elementEntries.put("Macetine Capsule", macetineCapsule);
        BlockConfig.add(macetineCapsule);

        elementEntries.put("Threns Capsule", thrensCapsule);
        BlockConfig.add(thrensCapsule);

        //   ---Floral Protomaterial (made of any plants) (used for making other plants or maybe bioship hull)
        ElementInformation floralProto = BlockConfig.newElement(mod, "Floral Protomaterial Capsule", getInfo(180).getTextureIds()); //Varis capsule texture
        setCapsuleInfo(floralProto,"",(short)180);
        setBasicInfo(floralProto, "Mixed plant material and genetic fragments. Can be used to reconstitute various types of common plant and fungal life in the Block Assembler.",
                T0_COMPONENT_PRICE, CAPSULE_MASS, true, false, getInfo(180).getBuildIconNum());
        floralProto.volume = CAPSULE_VOLUME;
        floralProto.lowHpSetting = true;
        floralProto.type = componentCategory;
        floralProto.shoppable = true;
        floralProto.blockStyle = BlockStyle.SPRITE;
        floralProto.blended = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(floralProto);

        //   ---Rock Dust Capsule (made of any rocks) (used with paint to make any type of rock)
        ElementInformation rockDust = BlockConfig.newElement(mod, "Rock Dust Capsule", getInfo(204).getTextureIds()); //Tekt capsule texture
        setCapsuleInfo(rockDust,"",(short)204);
        setBasicInfo(rockDust, "Pulverized rocks. Can be used along with Omnichrome Paint to reconstitute various types of common stone and mineral.",
                T0_COMPONENT_PRICE, CAPSULE_MASS, true, false, getInfo(204).getBuildIconNum());
        rockDust.volume = CAPSULE_VOLUME;
        rockDust.lowHpSetting = true;
        rockDust.type = componentCategory; //Even though these are capsules, they get used directly in the block assembler, so they're effectively Components
        rockDust.shoppable = true;
        floralProto.blockStyle = BlockStyle.SPRITE;
        floralProto.blended = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(rockDust);

        //------COMPONENTS

        short[] placeholderComponentTexture = getInfo(342).getTextureIds(); //silver bar. probably don't need to change this since these can't be placed anyway.

        //   -T1

        //   ---Omnichrome Paint (crystal composite, metal mesh) (used for coloured things, incl. rocks)
        ElementInformation omniPaint = BlockConfig.newElement(mod, "Component: Omnichrome Paint", getInfo(245).getTextureIds()); //white paint texture //TODO: rainbow paint texture
        setBasicInfo(omniPaint, "Space-grade paint that incorporates nanotechnology. By changing its nanoscale surface features to absorb and reflect different spectra of light, this paint can appear as a variety of colors.",
                T1_COMPONENT_PRICE, T1_COMPONENT_MASS, true, false, getInfo(245).getBuildIconNum());
        omniPaint.volume = COMMON_COMPONENT_VOLUME;
        omniPaint.type = componentCategory;
        omniPaint.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(omniPaint);

        //   ---Crystal Panel (crystal composite) (used for transparent things and computers)
        ElementInformation crystalPanel = BlockConfig.newElement(mod, "Component: Crystal Panel", placeholderComponentTexture);
        setBasicInfo(crystalPanel, "Transparent panels made from common crystal. Used in computers, windows, light fixtures, and more.",
                T1_COMPONENT_PRICE, T1_COMPONENT_MASS, false, false, T1_PLATE_ICON);
        crystalPanel.volume = COMMON_COMPONENT_VOLUME;
        crystalPanel.type = componentCategory;
        crystalPanel.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(crystalPanel);

        //   ---Crystal Light Source (crystal composite) (used for lights obv)
        ElementInformation crystalLightSource = BlockConfig.newElement(mod, "Component: Crystal Light Source", placeholderComponentTexture);
        setBasicInfo(crystalLightSource, "A compact and efficient light source made from common crystals. Used in most forms of lighting.",
                T1_COMPONENT_PRICE, T1_COMPONENT_MASS, false, false, T1_LIGHT_ICON);
        crystalLightSource.volume = COMMON_COMPONENT_VOLUME;
        crystalLightSource.type = componentCategory;
        crystalLightSource.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(crystalLightSource);

        //   ---Crystal Energy Focus (crystal composite) (used for salvage beams & damage beams)
        ElementInformation crystalEnergyFocus = BlockConfig.newElement(mod,"Component: Crystal Energy Focus", placeholderComponentTexture);
        setBasicInfo(crystalEnergyFocus, "An energy-focusing lens made from common crystals. Used in standard beam techologies for harvesting and destruction.",
                T1_COMPONENT_PRICE, T1_COMPONENT_MASS, false, false, T1_FOCUS_ICON);
        crystalEnergyFocus.volume = COMMON_COMPONENT_VOLUME;
        crystalEnergyFocus.type = componentCategory;
        crystalEnergyFocus.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(crystalEnergyFocus);

        //   ---Standard Circuitry (crystal composite) (logic, salvage/shipyard/weapon computers)
        ElementInformation standardCircuitry = BlockConfig.newElement(mod, "Component: Standard Circuitry", getInfo(547).getTextureIds()); //scrap composite
        setBasicInfo(standardCircuitry, "Photonic circuitry made from common crystals and metals. Used in standard computers.",
                T1_COMPONENT_PRICE, T1_COMPONENT_MASS, false, false, T1_CIRCUIT_ICON); //TODO: maybe edit scrap composite icon for final texture
        standardCircuitry.volume = COMMON_COMPONENT_VOLUME;
        standardCircuitry.type = componentCategory;
        standardCircuitry.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(standardCircuitry);

        //   ---Energy Cell (crystal composite) (shield capacitors, power?)
        ElementInformation energyCell = BlockConfig.newElement(mod, "Component: Energy Cell", placeholderComponentTexture);
        setBasicInfo(energyCell,"A device capable of storing various types of energy. Used in power and shielding systems.",
                T1_COMPONENT_PRICE, T1_COMPONENT_MASS, false, false, T1_CAPACITOR_ICON);
        energyCell.volume = COMMON_COMPONENT_VOLUME;
        energyCell.type = componentCategory;
        energyCell.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(energyCell);

        //   ---Metal Frame (metal mesh) (used for all system blocks and computers)
        ElementInformation metalFrame = BlockConfig.newElement(mod, "Component: Metal Frame", placeholderComponentTexture);
        setBasicInfo(metalFrame, "Structural framing segments and fasteners made from common metal. Used in the manufacture of most computers, machines, and system modules.",
                T1_COMPONENT_PRICE, T1_COMPONENT_MASS, false, false, T1_FRAME_ICON);
        metalFrame.volume = COMMON_COMPONENT_VOLUME;
        metalFrame.type = componentCategory;
        metalFrame.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(metalFrame);

        //   ---Metal Sheet (metal mesh) (used for all basic computers, all chambers, and basic armor)
        ElementInformation metalSheet = BlockConfig.newElement(mod, "Component: Metal Sheet", placeholderComponentTexture);
        setBasicInfo(metalSheet, "Sturdy sheets made from common metal, perfect for cutting, bending, or press-forming. Used in the manufacture of ship hulls, most computers, weapons, system modules, and more.",
                T1_COMPONENT_PRICE,T1_COMPONENT_MASS, false, false, T1_SHEET_ICON);
        metalSheet.volume = COMMON_COMPONENT_VOLUME;
        metalSheet.type = componentCategory;
        metalSheet.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(metalSheet);

        //   -T2

        //   ---Parsyne Amplifying Focus (parsyne/composite) (used for tractor beams, astrotech, shield supply/drain, etc)
        ElementInformation parsyneFocus = BlockConfig.newElement(mod, "Component: Parsyne Amplifying Focus", placeholderComponentTexture);
        setBasicInfo(parsyneFocus, "An advanced energy-focusing lens made with condensed parsyne plasma. Capable of amplifying and transforming energy passing through it. Used in the manufacture of advanced beam systems.",
                T2_COMPONENT_PRICE,T2_COMPONENT_MASS, false, false, T2_FOCUS_ICON); //TODO: a white lens with wavy patterns?
        parsyneFocus.volume = RARE_COMPONENT_VOLUME;
        parsyneFocus.type = componentCategory;
        parsyneFocus.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(parsyneCapsule);

        //  ---Parsyne Holographic Processor (parsyne/mesh) (used for certain computers)
        ElementInformation parsyneProcessor = BlockConfig.newElement(mod,"Component: Parsyne Holographic Processor", placeholderComponentTexture);
        setBasicInfo(parsyneProcessor, "A powerful three-dimensional core processor. Used in computers that manage intensive processes, such as warp jumps.",
                T2_COMPONENT_PRICE,T2_COMPONENT_MASS,false,false,T2_PROCESSOR_ICON);
        parsyneProcessor.volume = RARE_COMPONENT_VOLUME;
        parsyneProcessor.type = componentCategory;
        parsyneProcessor.shoppable = true;

        //   ---Macetine Alloy Plating (Macet/composite/mesh) (replacement for armor hardener - maybe armor hardener becomes a consumable for station defenses, made from anbaric. lol)
        ElementInformation macetinePlate = BlockConfig.newElement(mod, "Component: Macetine Alloy Plating", placeholderComponentTexture);
        setBasicInfo(macetinePlate, "An extremely durable plate manufactured from macetine alloy. Has a microscale internal structure optimized for dissipating kinetic energy. Used to reinforce armor.",
                T2_COMPONENT_PRICE,T2_COMPONENT_MASS, false, false, T2_PLATING_ICON);
        macetinePlate.volume = RARE_COMPONENT_VOLUME;
        macetinePlate.type = componentCategory;
        macetinePlate.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(macetinePlate);

        //   ---Macetine Magnetic Rail  (macet/mesh) (used for rails, cannons, kinetic effect mods)
        ElementInformation macetineRail = BlockConfig.newElement(mod, "Component: Macetine Magnetic Rail", placeholderComponentTexture);
        setBasicInfo(macetineRail, "A superconducting electromagnet rail made of macetine metal. Capable of controlling and moving plasma or metallic objects, potentially at very high speeds.",
            T2_COMPONENT_PRICE,T2_COMPONENT_MASS,false,false, T2_RAIL_ICON);
        macetineRail.volume = RARE_COMPONENT_VOLUME;
        macetineRail.type = componentCategory;
        macetineRail.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(macetineRail);

        //   ---Ferron Circuitry (ferron/composite) (used for stealth chambers, mine stealth modules, mine stealth/IFF modules)
        ElementInformation ferronCircuit = BlockConfig.newElement(mod, "Component: Ferron Resonant Circuitry", placeholderComponentTexture);
        setBasicInfo(ferronCircuit, "Advanced resonant circuitry made from ferron and crystal, combining electronic and photonic components. Useful in data transmission and synchronization, scanning, and stealth technologies.",
                T2_COMPONENT_PRICE, T2_COMPONENT_MASS, false, false, T2_CIRCUIT_ICON);
        ferronCircuit.volume = RARE_COMPONENT_VOLUME;
        ferronCircuit.type = componentCategory;
        ferronCircuit.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(ferronCircuit);

        //   ---Aegium Field Emitter (aegium/mesh) (used for shield blocks, defense chambers, logistics chambers, station aura devices of any kind, tractor beams, transporters...)
        ElementInformation aegiumEmitter = BlockConfig.newElement(mod, "Component: Aegium Field Emitter", placeholderComponentTexture);
        setBasicInfo(aegiumEmitter, "A device incorporating aegium crystals, capable of emitting and manipulating energy fields. Can be used to create various devices and modules for defensive purposes, or systems for manipulating and affecting objects from a distance.",
                T2_COMPONENT_PRICE, T2_COMPONENT_MASS, false, false, T2_EMITTER_ICON);
        aegiumEmitter.volume = RARE_COMPONENT_VOLUME;
        aegiumEmitter.type = componentCategory;
        aegiumEmitter.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(aegiumEmitter);

        //   ---Anbaric Distortion Coil (anbaric/mesh) (used for FTL, hover, etc)
        ElementInformation anbaricCoil = BlockConfig.newElement(mod, "Component: Anbaric Distortion Coil", placeholderComponentTexture);
        setBasicInfo(anbaricCoil, "A hollow metal coil with distilled, condensed anbaric flowing through it. Capable of creating and controlling spatial distortions. Used for faster-than-light travel, gravity manipulation, and other applications that contradict the conventional laws of physics.",
                T2_COMPONENT_PRICE, T2_COMPONENT_MASS, false, false, T2_COIL_ICON);
        anbaricCoil.volume = RARE_COMPONENT_VOLUME;
        anbaricCoil.type = componentCategory;
        anbaricCoil.shoppable = true;

        //   ---Thermyn Power Charge (thermyn/composite) (used for Mine Layers, Power Chambers, Missile Capacity mods, Mine damage modules, Warheads, heat effect mods, anything fancy that goes buzz and/or boom)
        ElementInformation thermynCharge = BlockConfig.newElement(mod, "Component: Thermyn Power Charge", placeholderComponentTexture);
        setBasicInfo(thermynCharge, "A sample of purified and condensed Thermyn held within a crystal casing. Generates incredible amounts of energy, which may be easily released in a controlled or uncontrolled manner. Handle with care!",
                T2_COMPONENT_PRICE,T2_COMPONENT_MASS,false, false, T2_CHARGE_ICON);
        thermynCharge.volume = RARE_COMPONENT_VOLUME;
        thermynCharge.type = componentCategory;
        thermynCharge.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(thermynCharge);

        //   ---Threns Wire Matrix (threns/mesh) (used for astro gear)
        ElementInformation thrensMatrix = BlockConfig.newElement(mod, "Component: Threns Wire Matrix", placeholderComponentTexture);
        setBasicInfo(thrensMatrix, "A delicate matrix of Threns wires. While not powerful enough for a starship, these matrices of mysterious energetic alloy can be manipulated to function as computers and compact power sources all at once. This makes them very useful for handheld gear.",
                T2_COMPONENT_PRICE,T2_COMPONENT_MASS,false,false, T2_MATRIX_ICON);
        thrensMatrix.volume = RARE_COMPONENT_VOLUME;
        thrensMatrix.type = componentCategory;
        //thrensMatrix.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(thrensMatrix);
        //   .
        //   -T3
        //   ---Prismatic Circuitry (ferron, aegium, parsyne, threns, composite) (used for tertiary effect computers, and anything else sufficiently fancy)
        ElementInformation prismaCircuit = BlockConfig.newElement(mod, "Component: Prismatic Circuit", placeholderComponentTexture);
        setBasicInfo(prismaCircuit, "A complex, intricate, and powerful quantum circuit. Used in highly advanced technologies.",
                T3_COMPONENT_PRICE, T3_COMPONENT_MASS, false, false, T3_CIRCUIT_ICON);
        prismaCircuit.volume = RARE_COMPONENT_VOLUME;
        prismaCircuit.type = componentCategory;
        prismaCircuit.shoppable = true;
        //ElementKeyMap.getCategoryHirarchy().insertRecusrive(prismaCircuit);

        elementEntries.put("Floral Protomaterial Capsule", floralProto);
        BlockConfig.add(floralProto);

        elementEntries.put("Rock Dust Capsule", rockDust);
        BlockConfig.add(rockDust);

        elementEntries.put("Component: Omnichrome Paint", omniPaint);
        BlockConfig.add(omniPaint);

        elementEntries.put("Component: Crystal Panel", crystalPanel);
        BlockConfig.add(crystalPanel);

        elementEntries.put("Component: Crystal Light Source", crystalLightSource);
        BlockConfig.add(crystalLightSource);

        elementEntries.put("Component: Crystal Energy Focus", crystalEnergyFocus);
        BlockConfig.add(crystalEnergyFocus);

        elementEntries.put("Component: Standard Circuitry", standardCircuitry);
        BlockConfig.add(standardCircuitry);

        elementEntries.put("Component: Energy Cell", energyCell);
        BlockConfig.add(energyCell);

        elementEntries.put("Component: Metal Frame", metalFrame);
        BlockConfig.add(metalFrame);

        elementEntries.put("Component: Metal Sheet", metalSheet);
        BlockConfig.add(metalSheet);

        elementEntries.put("Component: Parsyne Amplifying Focus", parsyneFocus);
        BlockConfig.add(parsyneFocus);

        elementEntries.put("Component: Parsyne Holographic Processor", parsyneProcessor);
        BlockConfig.add(parsyneProcessor);

        elementEntries.put("Component: Macetine Alloy Plating", macetinePlate);
        BlockConfig.add(macetinePlate);

        elementEntries.put("Component: Macetine Magnetic Rail", macetineRail);
        BlockConfig.add(macetineRail);

        elementEntries.put("Component: Ferron Resonant Circuitry", ferronCircuit);
        BlockConfig.add(ferronCircuit);

        elementEntries.put("Component: Aegium Field Emitter", aegiumEmitter);
        BlockConfig.add(aegiumEmitter);

        elementEntries.put("Component: Anbaric Distortion Coil", anbaricCoil);
        BlockConfig.add(anbaricCoil);

        elementEntries.put("Component: Thermyn Power Charge", thermynCharge);
        BlockConfig.add(thermynCharge);

        elementEntries.put("Component: Threns Wire Matrix", thrensMatrix);
        BlockConfig.add(thrensMatrix);

        elementEntries.put("Component: Prismatic Circuit", prismaCircuit);
        BlockConfig.add(prismaCircuit);

        //------FINAL ACTION(S)
        RRSRecipeManager.addRecipes(componentFab, blockAssembler);

        for(ElementInformation info : elementEntries.values()){
            info.dynamicPrice = info.price;
            info.calculateDynamicPrice();
        }
        //This has to be run TWICE (once upon addition in the element entries anon, once later) because of strange dynamic price logic - only on second run will it properly capture consistence based prices
        //good thing I added in hierarchical order or this would simply never work and I'd probably have to do the price calculations in recipe addition or something
        //TODO: look into icon bake

        elementsInitialized = true;
    }

    public static boolean sameCatAs(ElementInformation info, String category){
        try{return info.type.getCategory().equals(category);}
        catch(Exception ignore){return false;}
    }
    public static boolean sameCatAs(ElementInformation info, ElementCategory category){
        try{return info.type.getCategory().equals(category.getCategory());}
        catch(Exception ignore){return false; /*CatAsTrophe!*/}
    }

    public static boolean isSiblingCatTo(ElementInformation info, String category){
        try{
            ElementCategory infoParent = getParentCat(info.type);
            for(ElementCategory kitten : infoParent.getChildren()) if(kitten.getCategory().equals(category)) return true;
            return false;
        }
        catch(Exception ignore){return false;}
    }

    public static boolean isParentCat(String category, ElementInformation info){
        try{
            ElementCategory infoParent = getParentCat(info.type);
            assert infoParent != null;
            return infoParent.getCategory().equals(category);
        }
        catch(Exception ignore){return false;}
    }

    public static boolean isParentCat(ElementCategory category, ElementInformation info){
        return isParentCat(getParentCat(category).getCategory(), info);
    }

    public static ElementCategory getParentCat(ElementCategory type) {
        try {
            Field fParent = ElementCategory.class.getDeclaredField("parent");
            fParent.setAccessible(true);
            ElementCategory result = (ElementCategory)fParent.get(type);
            fParent.setAccessible(false);
            return result;
        } catch (Exception e) {
            System.err.println("[MOD][Resources ReSourced] Could not retrieve parent category for " + type.getCategory());
            return null;
        }
    }

    public static void setCapsuleInfo(ElementInformation info, String description, short vanillaCapsuleToMimic){
        ElementCategory capsuleCategory = ElementKeyMap.getCategoryHirarchy().find("Capsules");
        ElementInformation original = getInfo(vanillaCapsuleToMimic);

        setBasicInfo(info, description, CAPSULE_PRICE, CAPSULE_MASS, true, false, original.buildIconNum);
        info.setTextureId(original.getTextureIds());
        info.drawOnlyInBuildMode = original.drawOnlyInBuildMode;
        info.volume = CAPSULE_VOLUME;
        info.maxHitPointsFull = original.maxHitPointsFull;
        info.lowHpSetting = true;
        info.type = capsuleCategory;
        info.shoppable = true;
        info.dynamicPrice = original.dynamicPrice;
        info.canActivate = false;
        info.setBlockStyle(BlockStyle.SPRITE.id);
        info.blockResourceType = original.blockResourceType;
        info.lodShapeString = original.lodShapeString;
        info.lodShapeStringActive = original.lodShapeStringActive;
        info.lodShapeStyle = original.lodShapeStyle;
        info.lodActivationAnimationStyle = original.lodActivationAnimationStyle;
        info.lodCollision = original.lodCollision;
        info.lodCollisionPhysical = original.lodCollisionPhysical;
        info.lodDetailCollision = original.lodDetailCollision;
        info.lodUseDetailCollision = original.lodUseDetailCollision;
        info.hasActivationTexure = false;
        info.blended = true;
        info.animated = original.animated;
    }
}
