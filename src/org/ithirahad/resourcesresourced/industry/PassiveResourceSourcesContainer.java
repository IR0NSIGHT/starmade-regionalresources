package org.ithirahad.resourcesresourced.industry;

import org.apache.commons.lang3.ArrayUtils;
import org.ithirahad.resourcesresourced.RRSElementInfoManager;
import org.ithirahad.resourcesresourced.ResourcesReSourced;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.graphicsengine.movie.craterstudio.data.tuples.Pair;

import javax.vecmath.Vector4f;
import java.util.*;

import static org.ithirahad.resourcesresourced.ResourcesReSourced.*;
import static org.ithirahad.resourcesresourced.listeners.BlockRemovalLogic.getStarResourceSector;

public class PassiveResourceSourcesContainer {
    private static final PassiveResourceSupplier[] nothing = new PassiveResourceSupplier[]{};
    private final HashMap<Vector3i, PassiveResourceSupplier[]> resourceMap = new HashMap<>();

    public HashMap<Vector3i, PassiveResourceSupplier[]> getMap(){
        return resourceMap;
    }

    public void copyFrom(PassiveResourceSourcesContainer origin){
        resourceMap.clear();
        resourceMap.putAll(origin.getMap());
    }

    /**
     * clones active resource wells into this container from the origin
     * @param origin
     */
    public void copyOnlyActiveFrom(PassiveResourceSourcesContainer origin){
        resourceMap.clear();

        HashMap<Vector3i, PassiveResourceSupplier[]> originMap = origin.getMap();

        for(Vector3i key : originMap.keySet()) {//TODO O gods, is it shifting the keys after they're added again
            boolean hasAnyActive = false;
            for(PassiveResourceSupplier well : originMap.get(key)){
                //TODO well.bustGhosts(); //prevent the buildup of BS records
                if (!well.isEmpty()) {
                    hasAnyActive = true;
                    break;
                }
            }
            if(hasAnyActive) resourceMap.put(new Vector3i(key), originMap.get(key));
        }
    }

    public void beforeSerialize() {
        for (PassiveResourceSupplier[] ps: resourceMap.values()) {
            for (PassiveResourceSupplier p: ps)
                p.beforeSerialize();
        }
    }
    public void afterDeserialize() {
        for (PassiveResourceSupplier[] ps: resourceMap.values()) {
            for (PassiveResourceSupplier p: ps)
                p.afterDeserialize();
        }
    }

    public PassiveResourceSupplier[] getPassiveSources(Vector3i sector){
        return resourceMap.containsKey(sector) ? resourceMap.get(sector) : nothing;
    }

    //FIXME this is wacky and needs more clear and documented logic. when is old stuff kept, when overwritten, when deleted.
    public PassiveResourceSupplier[] setPassiveSources(Vector3i sector, PassiveResourceSupplier[] newValues){
        boolean duplicate = true;
        //Are we trying to set the same types of passive sources as already present?
        // (should not matter, but just in case RNG acts strange again and decides to make different choices inside the source body gen than last time)
        PassiveResourceSupplier[] extant = resourceMap.get(sector);
        if(extant != null) {
            for(PassiveResourceSupplier orig : extant){
                for(PassiveResourceSupplier newRsc : newValues){
                    if (orig.resourceTypeId != newRsc.resourceTypeId) {
                        duplicate = false;
                        break;
                    }
                }
            }
        } else duplicate = false;

        if(!duplicate && extant != null){
            //In this case, the generation probably changed. Perhaps something was added, or removed.
            //Either way, we need to try to hand any extant harvesters over if applicable.
            for(PassiveResourceSupplier oldSource : extant){
                for (PassiveResourceSupplier newSource : newValues) {
                    if(newSource.type == oldSource.type) newSource.copyFrom(oldSource);
                }
            }
            System.err.println("[MOD][Resources ReSourced][WARNING] Replaced extant resource source profile. This may be due to a galaxy generation change.");
        }
        //TODO: This duplicate checking logic might be sus.
        return duplicate ? extant : resourceMap.put(new Vector3i(sector),newValues);
    }

    public void resetPassiveSources(Vector3i sector){
        resourceMap.remove(sector);
    }

    public PassiveResourceSupplier[] addPassiveSources(Vector3i sector, PassiveResourceSupplier[]... rsc){
        return resourceMap.put(sector, (PassiveResourceSupplier[]) ArrayUtils.addAll(getPassiveSources(sector),rsc));
    }

    public Collection<PassiveResourceSupplier[]> getAllSources(){return resourceMap.values();}

    public PassiveResourceSupplier[] getAllAvailableSourcesInArea(Vector3i sector, Vector3i system) {
        PassiveResourceSupplier[][] allSources = {
                container.getPassiveSources(sector),
                container.getPassiveSources(new Vector3i(sector.x + 1, sector.y, sector.z)),
                container.getPassiveSources(new Vector3i(sector.x - 1, sector.y, sector.z)),
                container.getPassiveSources(new Vector3i(sector.x, sector.y + 1, sector.z)),
                container.getPassiveSources(new Vector3i(sector.x, sector.y - 1, sector.z)),
                container.getPassiveSources(new Vector3i(sector.x, sector.y, sector.z + 1)),
                container.getPassiveSources(new Vector3i(sector.x, sector.y, sector.z - 1)),
                container.getPassiveSources(getStarResourceSector(system))
        };
        ArrayList<PassiveResourceSupplier> results = new ArrayList<>();
        for(PassiveResourceSupplier[] rsc : allSources) Collections.addAll(results, rsc);
        return results.toArray(new PassiveResourceSupplier[]{});
    } //macro

    public PassiveResourceSupplier[] getAllAvailableSourcesInArea(SegmentController controller) {
        return getAllAvailableSourcesInArea(controller.getSector(new Vector3i()),controller.getSystem(new Vector3i()));
    }

    public List<Pair<Vector3i, Vector4f>> getAvailableSpaceSourcesForDisplay(SegmentController controller){
        ArrayList<Pair<Vector3i, Vector4f>> result = new ArrayList<>();

        Vector3i extractorSector = new Vector3i();
        controller.getSector(extractorSector);

        Vector3i system = new Vector3i();
        controller.getSystem(system);

        ArrayList<Vector3i> adjacentSourceSetsPresentInContainer = new ArrayList<>();
        Vector3i sec;
        sec = extractorSector;
        HashMap<Vector3i, PassiveResourceSupplier[]> map = getMap();
        if(map.containsKey(sec)) adjacentSourceSetsPresentInContainer.add(sec);
        sec = new Vector3i(extractorSector.x + 1, extractorSector.y, extractorSector.z);
        if(map.containsKey(sec)) adjacentSourceSetsPresentInContainer.add(sec);
        sec = new Vector3i(extractorSector.x - 1, extractorSector.y, extractorSector.z);
        if(map.containsKey(sec)) adjacentSourceSetsPresentInContainer.add(sec);
        sec = new Vector3i(extractorSector.x, extractorSector.y + 1, extractorSector.z);
        if(map.containsKey(sec)) adjacentSourceSetsPresentInContainer.add(sec);
        sec = new Vector3i(extractorSector.x, extractorSector.y - 1, extractorSector.z);
        if(map.containsKey(sec)) adjacentSourceSetsPresentInContainer.add(sec);
        sec = new Vector3i(extractorSector.x, extractorSector.y, extractorSector.z + 1);
        if(map.containsKey(sec)) adjacentSourceSetsPresentInContainer.add(sec);
        sec = new Vector3i(extractorSector.x, extractorSector.y, extractorSector.z - 1);
        if(map.containsKey(sec)) adjacentSourceSetsPresentInContainer.add(sec);
        sec = getStarResourceSector(system);
        if(map.containsKey(sec)) adjacentSourceSetsPresentInContainer.add(sec); //In hindsight, there ought to have been a function that returns the list of adjacent sectors.

        Vector4f color;
        PassiveResourceSupplier[] sources;
        short parsyne = RRSElementInfoManager.elementEntries.get("Parsyne Plasma").id;
        short anbaric = RRSElementInfoManager.elementEntries.get("Anbaric Vapor").id;
        short castellium = -1; //TODO: Bastion Initiative
        if(ResourcesReSourced.bastionInitiativeIsPresentAndActive) castellium = RRSElementInfoManager.elementEntries.get("Castellium Condensate").id; //TODO: Bastion Initiative

        for(Vector3i key : adjacentSourceSetsPresentInContainer){
            //we check if there is any special resource to give a colour, else give a generic one.
            color = null;
            sources = resourceMap.get(key);
            for(PassiveResourceSupplier source : sources){
                if(source.type == ResourceSourceType.SPACE){
                    color = new Vector4f(0.8f,0.8f,0.83f,1f); //generic colour; will be replaced if there is anything special in here
                    if(source.resourceTypeId == anbaric) color.set(0.79f,0.12f,0.06f,1f);
                    else if(source.resourceTypeId == parsyne) color.set(0.95f,0.98f,0.99f,1f);
                    else if(source.resourceTypeId == castellium) color.set(0.06f, 0.86f, 0.97f, 1f);
                }
            }
            if(color != null) result.add(new Pair<>(key,color));
            //this is a stupid way of determining validity but it's either this or a pointless boolean...
        }
        return result;
    }
}

