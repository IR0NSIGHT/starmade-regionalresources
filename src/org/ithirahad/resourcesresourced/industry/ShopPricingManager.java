package org.ithirahad.resourcesresourced.industry;

import org.ithirahad.resourcesresourced.RRUtils.MiscUtils;
import org.ithirahad.resourcesresourced.universe.galaxy.ZoneClass;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.*;
import org.schema.game.common.data.element.ElementCategory;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.data.world.StellarSystem;
import org.schema.game.network.objects.TradePrice;
import org.schema.game.network.objects.TradePriceInterface;
import org.schema.game.server.data.GameServerState;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.ithirahad.resourcesresourced.ResourcesReSourced.getSystemSheet;
import static org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType.*;
import static org.schema.game.common.data.world.SimpleTransformableSendableObject.EntityType.PLANET_ICO;

public class ShopPricingManager {
    public static float PRICE_FACTOR_PER_NATIVE_ITEM_RARE = 0.99f;
    public static float PRICE_FACTOR_PER_NATIVE_ITEM_COMMON = 0.999f;
    public static float PRICE_FACTOR_NATIVE_MATERIAL = 0.5f; //price factor for capsules or ores native to the region
    //TODO: balance these - they should be noticeable but not insane
    public static float CORE_MARKUP_FACTOR = 1.2f; //Ratio of Trading Guild sale prices to players vs. base value in the Galactic Core
    public static float CORE_CONVENIENCE_FACTOR = 0.9f; //Ratio of Trading Guild buy prices to players vs. base value in the Galactic Core
    public static float ZONES_MARKUP_FACTOR = 1.1f; //Ratio of Trading Guild sale prices to players vs. base value in resource zones
    public static float BARRENS_MARKUP_FACTOR = 1.5f; //Ratio of Trading Guild buy prices to players vs. base value in resource zones
    public static float BARRENS_CONVENIENCE_FACTOR = 0.6f; //Ratio of Trading Guild buy prices to players vs. base value in resource zones

    public static int IMPORT_FEE_PER_CAPSULE = 20; //TODO: maybe base this on distance to the nearest corresponding zone

    public static void setNPShopPricesIfApplicable(SegmentController entity){
        ZoneClass zoneClass;
        Vector3i secPos = entity.getTagSectorId();
        try {
            StellarSystem system = GameServerState.instance.getUniverse().getStellarSystemFromSecPos(secPos);
            zoneClass = getSystemSheet(system.getPos()).zone.zoneClass;
        } catch (IOException ioException) {
            ioException.printStackTrace();
            return;
        }

        if (entity.getFactionId() > -1 && entity.getType() != SHOP) return; //We don't want to mess with player stuff
        if (entity.getType() == SHOP || entity.getType() == SPACE_STATION || entity.getType() == PLANET_SEGMENT || entity.getType() == PLANET_ICO) {
            ShoppingAddOn shop = null;
            try {
                switch (entity.getType()) {
                    case SHOP:
                        ShopSpaceStation sticc = (ShopSpaceStation) entity;
                        shop = sticc.getShoppingAddOn();
                        break;
                    case SPACE_STATION:
                        SpaceStation station = (SpaceStation) entity;
                        shop = station.getShoppingAddOn();
                        break;
                    case PLANET_SEGMENT:
                        Planet seg = (Planet) entity;
                        shop = seg.getManagerContainer().getShoppingAddOn();
                        break;
                    case PLANET_ICO:
                        PlanetIco newseg = (PlanetIco) entity;
                        shop = newseg.getManagerContainer().getShoppingAddOn();
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                return;
            }

            if (shop != null) {
                List<TradePriceInterface> prices = new ArrayList<>();
                ShopInterface ishop = (ShopInterface) MiscUtils.readPrivateField("shop", shop);

                final ElementCategory capsuleCategory = ElementKeyMap.getCategoryHirarchy().find("Capsules");
                final ElementCategory rawRscCategory = ElementKeyMap.getCategoryHirarchy().find("Raw");

                if (ishop != null) for (ElementInformation item : ElementKeyMap.getInfoArray())
                    if (item != null && item.shoppable && !item.deprecated) {
                        TradePriceInterface oldBuy = shop.getPrice(item.id, true);
                        TradePriceInterface oldSell = shop.getPrice(item.id, false);

                        int shopBuysAtPrice = (int)item.dynamicPrice; //This is the price the SHOP buys for; the price the PLAYER sells back to them at.
                        int buyQuant = oldBuy == null? 0 : oldBuy.getAmount();
                        int shopSellsAtPrice = (int)item.dynamicPrice; //This is the price the PLAYER buys for; the price the SHOP sells at.
                        int sellQuant = oldBuy == null? 0 : oldSell.getAmount();

                        if(buyQuant != 0 || sellQuant != 0) {
                            if (zoneClass == ZoneClass.RR_CORE) {
                                shopBuysAtPrice = Math.max(1,(int) (shopBuysAtPrice * CORE_CONVENIENCE_FACTOR)); //TODO: Should this just be 1?
                                shopSellsAtPrice = Math.max(1,(int) (shopSellsAtPrice * CORE_MARKUP_FACTOR));
                            } else if(zoneClass == ZoneClass.NONE){
                                shopBuysAtPrice = Math.max(1,(int) (shopBuysAtPrice * BARRENS_CONVENIENCE_FACTOR));
                                shopSellsAtPrice = Math.max(1,(int) (shopSellsAtPrice * BARRENS_MARKUP_FACTOR)); //TG Ripoff :D
                            } else {
                                float discountFactor;
                                int materialImportFee = 0;
                                boolean isCommonMatZone = (zoneClass == ZoneClass.RR_CRYSTAL_DENSE || zoneClass == ZoneClass.RR_METAL_DENSE);
                                if(isCommonMatZone) discountFactor = PRICE_FACTOR_PER_NATIVE_ITEM_COMMON;
                                else discountFactor = PRICE_FACTOR_PER_NATIVE_ITEM_RARE;

                                float priceMultiplier = 1.0f;
                                List<FactoryResource> blockMaterials = item.getRawConsistence();
                                ElementInformation[] nativeMaterials = zoneClass.principalRefinedResource();
                                System.currentTimeMillis(); //NOP for breakpoint

                                boolean isNative = false;
                                if(item.type.equals(capsuleCategory)){
                                    for(ElementInformation e : nativeMaterials)
                                        if (e != null && e.id == item.id) {
                                            priceMultiplier = PRICE_FACTOR_NATIVE_MATERIAL;
                                            break;
                                        }
                                }
                                else if(item.type.equals(rawRscCategory)){
                                    nativeMaterials = zoneClass.principalResources();
                                    for(ElementInformation e : nativeMaterials)
                                        if (e != null && e.id == item.id) {
                                            priceMultiplier = PRICE_FACTOR_NATIVE_MATERIAL;
                                            break;
                                        }
                                }
                                else for(FactoryResource ingredient : blockMaterials){
                                    for(ElementInformation mat : nativeMaterials){
                                        if(mat.id == ingredient.type) {
                                            isNative = true; //ingredient.count times
                                            break;
                                        }
                                    }
                                    if(isNative) {
                                        priceMultiplier *= (StrictMath.pow(discountFactor,ingredient.count));
                                    }
                                    else if(ElementKeyMap.getInfo(ingredient.type).type == capsuleCategory) {
                                        materialImportFee += IMPORT_FEE_PER_CAPSULE * ingredient.count;
                                    } //else it's mesh/crystal/plants/etc. and we don't care
                                }

                                shopBuysAtPrice = Math.max(1,(int)(shopBuysAtPrice * priceMultiplier));
                                shopSellsAtPrice = Math.max(1,(int)(shopSellsAtPrice * priceMultiplier * ZONES_MARKUP_FACTOR) + materialImportFee);
                            }
                            //else double availability count, adjust inventory and buy price based on zone resource present or no.

                            // TradePrice initializer: (short type, int amount, int price, int limit, boolean sell)
                            TradePrice newBuy = new TradePrice(item.id, oldBuy.getAmount(), shopBuysAtPrice, oldBuy.getLimit(), false);
                            TradePrice newSell = new TradePrice(item.id, sellQuant, shopSellsAtPrice, oldSell.getLimit(), true);
                            prices.add(newBuy);
                            prices.add(newSell);
                        }
                    }
                shop.putAllPrices(prices);
            }
        }
    }
}
