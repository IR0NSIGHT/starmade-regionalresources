package org.ithirahad.resourcesresourced.industry;

import org.ithirahad.resourcesresourced.RRSConfiguration;
import org.ithirahad.resourcesresourced.RRSElementInfoManager;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import static org.ithirahad.resourcesresourced.RRSConfiguration.DEBUG_LOGGING;

public class Extractor implements Serializable {
    String UID;
    float strength;
    int capacity; //amount of units that the extractor can hold.
    float collected;
    Vector3i sector;
    String segmentController;
    Vector3i block;
    ResourceSourceType type;
    short oreId;
    private long lastWithdraw; //last time the extractor was emptied

    public static String generateUID(String segmentController, Vector3i sector, Vector3i block) {
        return segmentController+"_"+sector+"_"+block;
    }

    private transient PassiveResourceSupplier supplier;

    public static void main(String[] args) {
        /*
        PassiveResourceSupplier debugResourceSupplier = new PassiveResourceSupplier(25,(short)420,ResourceSourceType.SPACE,new Vector3i());
        Extractor ex = debugResourceSupplier.addHarvester("DEBUG_01",ResourceSourceType.SPACE,new Vector3i(69,0,420));
        ex.setCapacity(543); ex.setStrength(10);

        Extractor ex2 = debugResourceSupplier.addHarvester("DEBUG_02",ResourceSourceType.SPACE,new Vector3i(69,0,420));
        ex2.setCapacity(543); ex2.setStrength(20);

        print(ex.toString()); print(ex2.toString());
        debugResourceSupplier.update(false,-1);
        print(ex.toString()); print(ex2.toString());

        int gotten = ex.withdrawCollected(-1);
        System.out.println("[MOD][DEBUG][Resources ReSourced] got "+gotten+ " units of ore.");
        System.out.println(ex);
         */
    }

    private static void print(String s) {
        System.out.println(s);
    }

    public Extractor(String segmentController, Vector3i block, ResourceSourceType type, Vector3i sector, short OreId) {
        this.UID = generateUID(segmentController,sector,block);
        this.block = block;
        this. segmentController = segmentController;
        this.type = type;
        this.sector = sector;
        this.oreId = OreId;
    }

    public boolean isLoaded() {
        return GameServerState.instance.getUniverse().isSectorLoaded(sector);
    }

    /**
     * requires that extractor is loaded.
     * @return
     */
    public boolean existsBlock() {
        if (!GameServerState.instance.getSegmentControllersByName().containsKey(segmentController))
            return false;
        SegmentController sc = GameServerState.instance.getSegmentControllersByName().get(segmentController);
        assert sc != null;
        //access the block by its absolute position on the loaded segmentcontroller. check if its a siphon/extractor
        SegmentPiece extractorBlock = sc.getSegmentBuffer().getPointUnsave(block); //TODO make sure this actually gets the intended block.
        assert extractorBlock != null; //what happens when the block doesnt exist? is it null?
        return type.getExtractorBlockID() == extractorBlock.getType();
    }

    public void setSupplier(PassiveResourceSupplier supplier) {
        this.supplier = supplier;
    }

    protected void incCollected(float amount) {
        //collected += amount;
        collected = Math.min(capacity,collected+amount);
        if(DEBUG_LOGGING)System.out.println("[MOD][Resources ReSourced] Increased extractor "+getUID()+" collected by "+amount+" to "+collected);
    }

    /**
     * Withdraw up to max units of collected items. Withdraw amount is subtracted from collected quantity logged within object.
     * Collected gets rounded down; rounding errors remain as "collected" fractional item logged in the object.
     * @param max -1 for unlimited
     * @return quantity that is withdrawn
     */
    protected int withdrawCollected(double max) {
        int out;
        if (max!=-1)
            out = (int) Math.min(max,collected);
        else
            out = (int) collected;
        collected -= out; //keep fractions, return rounded down amount
        long untouched = (System.currentTimeMillis()-lastWithdraw)/1000;
        lastWithdraw = System.currentTimeMillis();
        if(DEBUG_LOGGING) print("[MOD][Resources ReSourced] Withdrew "+out+" from extractor "+getUID() + " after being untouched for "+ untouched/60+ "m"+untouched%60+ "seconds");
        return out;
    }

    public void setCapacityVolume(double cargoVolume) {
        float capacityPerItem = ElementKeyMap.getInfo(oreId).getVolume();
        capacity = (int)(cargoVolume/capacityPerItem);
    }

    public String getUID() {
        return UID;
    }

    public float getStrength() {
        return strength;
    }

    public int getCapacity() {
        return capacity;
    }

    public float getCollected() {
        return collected;
    }

    public Vector3i getSector() {
        return sector;
    }

    public ResourceSourceType getType() {
        return type;
    }

    public void setStrength(float strength) {
        if(DEBUG_LOGGING)System.out.println("[MOD][Resources ReSourced] Set strength of extractor " + getUID() + " from " + this.strength + " to "+ strength);
        float old = this.strength;
        this.strength = strength;

        if (strength!=old && supplier != null) //FIXME supplier is null
            supplier.onExtractorPowerChanged(this, old);
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getSegmentController() {
        return segmentController;
    }

    public Vector3i getBlock() {
        return block;
    }

    @Override
    public String toString() {
        Date date=new Date(lastWithdraw);
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM HH:mm:ss (z)");
        return "Extractor{" +
                "UID='" + UID + '\'' +
                ", strength=" + strength +
                ", capacity=" + capacity +
                ", collected=" + collected +
                ", sector=" + sector +
                ", segmentController='" + segmentController + '\'' +
                ", block=" + block +
                ", type=" + type +
                ", oreId=" + oreId +
                ", lastWithdraw=" + sdf.format(date) +
                '}';
    }
}
