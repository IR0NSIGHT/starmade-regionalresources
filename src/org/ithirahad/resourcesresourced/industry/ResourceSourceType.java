package org.ithirahad.resourcesresourced.industry;

public enum ResourceSourceType {
    SPACE,
    GROUND;
    private short id = -1;
    public short getExtractorBlockID() {
        assert id != -1;
        return id;
    }
    public void setExtractorBlockID(short id) {
        this.id = id;
    }
}
