package org.ithirahad.resourcesresourced.industry;

import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.data.element.ElementInformation;

import java.util.*;

import static org.ithirahad.resourcesresourced.RRSConfiguration.*;

/**
 * STARMADE MOD
 * Creator: Ithirahad Ivrar'kiim
 * Date: 1 Nov. 2021
 * Time: Running out
 *
 * This class represents something that produced ore that can be extracted: a gasgiant or terrestial planet etc.
 * extractors can be added to it to farm the ore.
 */
//@SuppressWarnings("all")
public class PassiveResourceSupplier {
    //Each supply entry contains regenRate, amount collected, and a list of stations that have extractors
    public float regenRate; //per second
    public short resourceTypeId;
    public ResourceSourceType type;
    private float totalExPower;
    private transient long lastUpdate;
    private Vector3i sector;
    private Vector3i system; //necessary?

    private final ArrayList<Extractor> extractors = new ArrayList<>();
    private transient HashMap<String,Integer> extractorUID_to_idx = new HashMap<>();
    public PassiveResourceSupplier(float regenRate, short resourceTypeId, ResourceSourceType type, Vector3i sector){
        this.regenRate = (float) (Math.round(regenRate * 1000.0) / 1000.0); //rounded number
        this.resourceTypeId = resourceTypeId;
        this.type = type;
        this.sector = sector;
    }

    //TODO dont save if no extractors exist for this resource well
    //TODO ensure extractors are no double entries.

    public void beforeSerialize() {
        if(extractors.size() > 0) recalcExtractionPower();
        //last update time is not saved, so that time while server is offline doesn't count into the resource gathering.
        update(true, System.currentTimeMillis());
    }

    public void afterDeserialize() {
        //rebuild hashmap
        extractorUID_to_idx = new HashMap<>(extractors.size());
        for (int i = 0; i < extractors.size(); i++) {
            assert extractors.get(i)!=null;
            extractorUID_to_idx.put(extractors.get(i).getUID(),i);
        }

        //rebuild references that weren't saved bc circular reference.
        for (Extractor ex : extractors) {
            ex.setSupplier(this);
        }

        //force initial update
        update(true, System.currentTimeMillis());
    }

    public Extractor addExtractor(String segmentControllerUID, Vector3i block, ResourceSourceType type, Vector3i sector){
        String uid = Extractor.generateUID(segmentControllerUID,sector,block);
        //make sure harvester doesnt exist yet with this UID
        if (existsExtractor(uid))
            return getExtractor(uid);

        Extractor ex = new Extractor(segmentControllerUID, block, type,  sector, resourceTypeId);
        extractors.add(ex);
        extractorUID_to_idx.put(uid, extractors.indexOf(ex));
        ex.setSupplier(this);
        return ex;
    }

    public boolean existsExtractor(String uid) {
        return extractorUID_to_idx.containsKey(uid);
    }

    public void removeExtractor(String uid){
        if (!existsExtractor(uid))
            return;
        Integer idx = extractorUID_to_idx.get(uid);
        assert idx != null && uid.equals(extractors.get(idx).UID);
        extractors.remove((int)idx);
        extractorUID_to_idx.remove(uid);
    }

    public void clearExtractors(){
        extractors.clear();
        extractorUID_to_idx.clear();
    }

    public Extractor getExtractor(String uid) {
        if (!existsExtractor(uid))
            return null;

        Integer idx = extractorUID_to_idx.get(uid);
        Extractor out = extractors.get(idx);
        assert out!= null && uid.equals(out.UID);
        return out;
    }

    public Collection<Extractor> getAllExtractors() {
        return extractors;
    }

    /**
     'Macro' function.
     Retrieves as much resource as can be fit into the specified capacity, subtracting it from the corresponding record, divded by however many resources there are.
     */
    public int claimCollectedResources(String uid, double maxCapacity) {
        Extractor ex = getExtractor(uid);
        if (ex != null)
            return ex.withdrawCollected(maxCapacity);
        else
            return 0;
    }

    /**
     * Will update the resource well [TODO: if it's loaded?]
     * Equals one ore-generation-update, distributed generated ore proportionally to connected extractors.
     * @param force force update, regardless if loaded or not.
     * @param currentTime
     */
    public void update(boolean force, long currentTime){
        if (force || isLoaded()) {
            if(DEBUG_LOGGING) System.out.println("[MOD][Resources ReSourced] --------------- Force/Loaded Update for "+sector+ ElementInformation.getKeyId(resourceTypeId));
            //TODO: note from ir0nsight: there is no need to bruteforce all extractors galaxy wide every 5 seconds. update only needs to happen when loaded/before saving, since time is logged and harvested value calculated based on time elapsed.
            long millisSinceLastUpdate = currentTime-lastUpdate;
            boolean nullUpdate = (lastUpdate<=0);
            lastUpdate = currentTime; //for
            if (nullUpdate) //Initial update after server start; don't do anything here. -> Time the server was offline doesn't count into passive generation.
                return;

            float regenRateTimed = (regenRate*millisSinceLastUpdate)/(1000f); //millis -> seconds -> sum of regen rate for x seconds
            if(DEBUG_LOGGING) System.out.println("[MOD][Resources ReSourced] Resource well update ("+millisSinceLastUpdate/1000+"sec) with regen/sec="+regenRate+" => "+regenRateTimed);
            boolean oversubscribed = (totalExPower>regenRate);
            if(DEBUG_LOGGING) System.out.println(String.format("[MOD][Resources ReSourced] Resource supplier update: "+(oversubscribed?"[oversubscribed]":"[underutilized/nominal]")+": regen=%s, power_total= %s",regenRate,totalExPower));
            System.out.println(this);
            LinkedList<Extractor> deleted = new LinkedList<>();
            for (Extractor ex: extractors) {
                //TODO existsBlock method needs impleementation
                if (ex.isLoaded() && !ex.existsBlock()) {
                    deleted.add(ex);
                    continue;
                }

                if (!oversubscribed)
                    ex.incCollected((ex.getStrength()*millisSinceLastUpdate)/1000f); //exctraction power adjusted for time
                else {
                    ex.incCollected(regenRateTimed* (ex.getStrength()/(float)totalExPower) ); //proportial extraction power adjusted for time
                }
                if(DEBUG_LOGGING) System.out.println(ex);
            }
            for (Extractor ex: deleted) {
                removeExtractor(ex.getUID());
            }
        }
    }

    /**
     * used for the factory to call when its ticking.
     */
    public void updateLoaded() {
        //recalcExtractionPower(); //TODO turn on if needed, backup in case the event system fails.
    }

    private boolean isLoaded() {
  //      if (sector != null && !GameServerState.instance.getUniverse().isSectorLoaded(sector))
            //TODO consider this case: supplier is far away from loaded extractor->supplier unloaded&& extractor loaded=>extractor doens't receive harvested stuff.
//            return false;
        return true; //TODO
    }

    public boolean contains(String uid){
        return extractorUID_to_idx.containsKey(uid);
    }

    /**
     * event system: resource well is notified that a connected extractor now has a different pulling strength
     * @param ex
     * @param oldPower
     */
    public void onExtractorPowerChanged(Extractor ex, float oldPower) {
        //adjust total power to correctly represent sum of extractors.
        totalExPower = Math.max(0,totalExPower-oldPower);
        totalExPower += ex.getStrength();
        if(DEBUG_LOGGING) System.out.println(String.format("[MOD][Resources ReSourced] extractor for supplier changed extraction power from %s to %s, total ex power now %s",oldPower,ex.getStrength(),totalExPower));
    }

    public void copyFrom(PassiveResourceSupplier source) {
        clearExtractors();
        //clone listed extractors into own list with same values but new objects
        for (Extractor ex : source.extractors)
            if (ex.getType() == type) {
                Extractor newEx = addExtractor(ex.getSegmentController(),ex.getBlock(), ex.getType(), ex.getSector());
                newEx.setCapacity(ex.getCapacity());
                newEx.setStrength(ex.getStrength());
            }
    } //somehow, I foresee that trouble this way comes...

    public boolean isEmpty() {
        return extractors.isEmpty();
    }

    public float getRegenRate() {
        return regenRate;
    }

    public short getResourceTypeId() {
        return resourceTypeId;
    }

    public ResourceSourceType getType() {
        return type;
    }

    public float getTotalExPower() {
        return totalExPower;
    }

    @Override
    public String toString() {
        return "PassiveResourceSupplier{" +
                "regenRate=" + regenRate +
                ", resourceTypeId=" + resourceTypeId +
                ", type=" + type +
                ", totalExPower=" + totalExPower +
                ", amountExts="+getExtractorCount()+
                '}';
    }

    public int getExtractorCount() {
        return extractors.size();
    }

    public void recalcExtractionPower() {
        totalExPower = 0;
        for(Extractor ex : extractors){
            totalExPower += ex.strength;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PassiveResourceSupplier that = (PassiveResourceSupplier) o;
        return Objects.equals(sector, that.sector);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sector);
    }


}
