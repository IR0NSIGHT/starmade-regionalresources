package org.ithirahad.resourcesresourced.industry;

import api.utils.StarRunnable;
import org.schema.game.server.data.GameServerState;

import java.util.*;

import static org.ithirahad.resourcesresourced.RRSConfiguration.DEBUG_LOGGING;
import static org.ithirahad.resourcesresourced.RRSConfiguration.PASSIVE_POOL_UPDATE_INTERVAL_MS;
import static org.ithirahad.resourcesresourced.ResourcesReSourced.*;

public class PassiveResourceUpdater extends StarRunnable {
    boolean firstRun = true;
    boolean doTick = false;
    final Timer timer = new Timer();
    final Random rng = new Random();

    //TODO: This could and maybe should be a RealTimeDelayedAction.

    @Override
    public void run() {
        if (GameServerState.instance == null) return;
        if (firstRun) { //TODO: and is server or singleplayer, else just doTick n number of times depending on how many times the server's done it since last client update
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    doTick = true;
                }
            }, 0, PASSIVE_POOL_UPDATE_INTERVAL_MS); //TODO: attempt to synch with server, not that it matters that much
        }
        firstRun = false;

        if (doTick && (GameServerState.instance != null)) { //do not do this on MP clients
            if(DEBUG_LOGGING) System.out.println("###########EXTRACTOR UPDATE TICK#############");
            Collection<PassiveResourceSupplier[]> sourceEntries = container.getAllSources();

            for (PassiveResourceSupplier[] resourceWells : sourceEntries) {
                if (resourceWells.length == 0) continue;
                for (PassiveResourceSupplier resourceWell : resourceWells) {
                    if (!resourceWell.isEmpty()) resourceWell.update(false,System.currentTimeMillis());
                }
            }
            doTick = false;
        }
    }
}