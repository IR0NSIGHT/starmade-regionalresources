package org.ithirahad.resourcesresourced.gui;

import api.DebugFile;
import api.listener.Listener;
import api.listener.events.input.MousePressEvent;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.utils.StarRunnable;
import libpackage.drawer.MapDrawer;
import libpackage.drawer.SpriteLoader;
import libpackage.markers.ClickableMapMarker;
import libpackage.markers.SimpleMapMarker;
import libpackage.text.MapText;
import org.apache.commons.lang3.text.WordUtils;
import org.ithirahad.resourcesresourced.ResourcesReSourced;
import org.ithirahad.resourcesresourced.universe.galaxy.ResourceZone;
import org.ithirahad.resourcesresourced.universe.galaxy.ZoneClass;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.gamemap.GameMapDrawer;
import org.schema.game.common.data.world.VoidSystem;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.util.*;

/**
 * STARMADE MOD
 * CREATOR: Max1M
 * DATE: 20.10.2021
 * TIME: 17:34
 * drawer class specifically to draw stuff for marking out resource zones. handles its own sprite loading and the marker generation.
 * instantiate, loadsprites, generate zones. drawer does the rest.
 */
public class ZoneMapDrawer extends MapDrawer {
    private final ArrayList<SimpleMapMarker> starZoneMarkers = new ArrayList<>(); //stores the zone marker for each star
    private final Map<Vector3i, ClickableMapMarker> childToParentMarkerMap = new HashMap<>(); //TODO: per galaxy (or just make these zonemapdrawers per galaxy and swap them idk)
    private Sprite zoneSprite;
    private ClickableMapMarker hoverNow;
    private ClickableMapMarker selectedZone;
    private static GameMapDrawer currentDrawer;
    public ZoneMapDrawer(StarMod mod) {
        super(mod);
        DebugFile.log("CREATED ZONE MAP DRAWER");

        new StarRunnable(){
            @Override
            public void run() {
                if (GameClientState.instance == null || GameClientState.instance.getPlayer() == null) {
                    return;
                }
                generateZoneMarkers();
                cancel();
            }
        }.runTimer(mod,100);

        StarLoader.registerListener(MousePressEvent.class, new Listener<MousePressEvent>() {
            @Override
            public void onEvent(MousePressEvent event) {
                if (event.getRawEvent().pressedLeftMouse()) {
                    if (selectedZone != null && selectedZone.equals(hoverNow)) {
                        selectedZone = null;
                    } else {
                        selectedZone = hoverNow;
                    }
                }
            }
        },mod);
    }

    public void loadSprite(StarMod mod) {
        SpriteLoader msw = new SpriteLoader("org/ithirahad/resourcesresourced/assets/image/map/","zoneMarker.png",1000,1000,1,1);
        if (msw.loadSprite(mod)) {
            zoneSprite = msw.getSprite();
        }
        DebugFile.log("LOADED ZONE SPRITE",mod);
    }

    public void generateZoneMarkers() {
        DebugFile.log("[RRS][MAP] Generating zone markers...");
        assert ResourcesReSourced.zoneMaps != null;
        assert GameClientState.instance != null;
        int i = 0;
        //FIXME entries in zone.getStars are SYSTEMS not sectors! adjust position.
        Object o = ResourcesReSourced.zoneMaps.get(GameClientState.instance.getCurrentGalaxy().galaxyPos);
        for (ResourceZone zone: ResourcesReSourced.zoneMaps.get(GameClientState.instance.getCurrentGalaxy().galaxyPos).values()) {
            if (zone.zoneClass.equals(ZoneClass.NONE))
               continue;
            System.out.println("############# zone "+ zone.name + zone.zoneClass + " has " + zone.getStars().size() +" stars. ");
            //collect all stars connected to all origins in this zone
            Vector3i zoneCore = zone.getCenterSystem();
            LinkedList<Vector3i> starsInZone = new LinkedList<>(zone.getStars());

            //done collecting all stars
            final Vector3i corePosIn = new Vector3i(starsInZone.getFirst());
            //corePosIn.sub(64,64,64); //galaxy half size //TODO why?
            final ClickableMapMarker originMarker = new ClickableMapMarker(zoneSprite,
                    0,
                    getColorByType(zone.zoneClass),
                    posFromSystemPos(zoneCore)) {
                float baseScale = getScale();

                @Override
                public void preDraw(GameMapDrawer drawer) {
                    if(currentDrawer != null && corePosIn.equals(currentDrawer.getGameMapPosition().getCurrentSysPos())) setScale(baseScale * 2);
                    else if(hoverNow != null && hoverNow.equals(this)) setScale(baseScale * 1.2f);
                    else setScale(baseScale);
                    super.preDraw(drawer);
                }

                @Override
                public void onSelect(float v) {
                    super.onSelect(v);
                    hoverNow = this;
                }

                @Override
                public void onUnSelect() {
                    super.onUnSelect();
                    if (this.equals(hoverNow))
                        hoverNow = null;
                }
            };
            addMarker(originMarker);

            //generate help text for focused zone
            String focusedText = zone.name.toUpperCase() + "\r\n(" + WordUtils.capitalize(zone.zoneClass.getDescriptor()) + " Region)";
            MapText selectedZoneText = new MapText(GameClientState.instance,
                    this,
                    zone.name.length() * 25,
                    50,
                    posFromSystemPos(zoneCore),
                    focusedText,
                    new Vector3f(zone.name.length() * 7.5f,-100,0), //Blender Pro Heavy is not monospace so this will never be perfect
                    FontLibrary.getBlenderProHeavy20(),
                    false){
                final ClickableMapMarker parent = originMarker;
                @Override
                public void draw() {
                    boolean isViewPosition = false;
                    if(currentDrawer != null) isViewPosition = parent.equals(getOriginMarker(currentDrawer.getGameMapPosition().getCurrentSysPos()));
                    if (parent.equals(selectedZone) || isViewPosition ||  (hoverNow != null && hoverNow.equals(parent))) super.draw();
                }
            };

            selectedZoneText.setBoxColor(new Vector4f(0,0,0,0));
            //otherZoneText.setBoxColor(new Vector4f(0,0,0,0));
            generateZone(starsInZone,zone.zoneClass,originMarker);
        }
    }

    private Vector4f getColorByType(ZoneClass type) {
        return type.ZoneMapColor();
    }

    /**
     * generate markers for all stars in this zone. get slaved to the origin marker automatically
     * @param zoneClass
     */
    private void generateZone(Collection<Vector3i> systems, ZoneClass zoneClass, final ClickableMapMarker master) {
        //DebugFile.log("generating zone type " + zoneClass.name());

        for (final Vector3i star : systems) {
            SimpleMapMarker starMarker = new ClickableMapMarker(
                    zoneSprite,
                    0,
                    getColorByType(zoneClass),
                    posFromSystemPos(star)
            ){
                Vector3i gPos = new Vector3i(star);
                float baseScale = getScale();

                @Override
                public void preDraw(GameMapDrawer drawer) {
                    if(currentDrawer != null && gPos.equals(currentDrawer.getGameMapPosition().getCurrentSysPos())) setScale(baseScale * 2);
                    else if(hoverNow != null && hoverNow.equals(this)) setScale(baseScale * 1.2f);
                    else setScale(baseScale);
                    super.preDraw(drawer);
                }

                @Override
                public boolean canDraw() {
                    boolean isViewPosition = false;
                    if(currentDrawer != null) isViewPosition = master.equals(getOriginMarker(currentDrawer.getGameMapPosition().getCurrentSysPos()));
                    return (master.equals(selectedZone) || isViewPosition || (hoverNow != null && hoverNow.equals(master)));
                }
            };
            childToParentMarkerMap.put(star,master);
            addMarker(starMarker);
        }
    }

    private static Vector3f posFromSystemPos(Vector3i system) {
        Vector3i in = new Vector3i(system);
        in.scale(VoidSystem.SYSTEM_SIZE);
        in.add(VoidSystem.SYSTEM_SIZE_HALF,VoidSystem.SYSTEM_SIZE_HALF,VoidSystem.SYSTEM_SIZE_HALF);
        return posFromSector(in,true);
    }

    protected ClickableMapMarker getOriginMarker(Vector3i sub){
        return childToParentMarkerMap.get(sub);
    }

    @Override
    public void galaxy_DrawSprites(GameMapDrawer gameMapDrawer) {
        super.galaxy_DrawSprites(gameMapDrawer);
        currentDrawer = gameMapDrawer;
    }
}
