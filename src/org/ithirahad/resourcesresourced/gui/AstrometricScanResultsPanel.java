package org.ithirahad.resourcesresourced.gui;

import api.utils.gui.GUIMenuPanel;
import org.ithirahad.resourcesresourced.network.SystemScanInfo;
import org.schema.game.client.data.GameClientState;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.forms.font.FontLibrary;
import org.schema.schine.graphicsengine.forms.gui.*;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AstrometricScanResultsPanel extends GUIMenuPanel {
    SystemScanInfo source = null;
    public boolean isInit = false;
    GUIContentPane overviewTab = null;
    private final GUIColoredRectangle overviewBG;
    private final GUITextOverlay overviewText;
    //private final GUIAncor overviewAnchor;

    GUIContentPane celestialsTab = null;
    private final GUIColoredRectangle celestialsBG;
    private final GUITextOverlay celestialsText;
    private final GUIScrollablePanel celestialScroll;
    //private final GUIAncor celestialsAnchor;

    GUIContentPane resourcesTab = null;
    private final GUIColoredRectangle resourcesBG;
    private final GUITextOverlay resourcesText;
    //private final GUIAncor resourcesAnchor;

    public AstrometricScanResultsPanel(GameClientState inputState, SystemScanInfo info) {
        super(inputState, "AstrometricScanResultsPanel", 500, 800);
        source = info;

        overviewBG = new GUIColoredRectangle(getState(), 540, 30, new Vector4f(0, 0.26f, 0.3f, 0.5f));
        //overviewAnchor = new GUIAncor(inputState, 500, 800);
        overviewText = new GUITextOverlay(300,300, FontLibrary.getBlenderProBook16(), inputState);
        overviewText.wrapSimple = true;
        overviewBG.attach(overviewText);
        //overviewAnchor.attach(overviewText);
        //overviewBG.attach(overviewAnchor);

        celestialsBG = new GUIColoredRectangle(getState(), 540, 30, new Vector4f(0, 0.26f, 0.3f, 0.5f));
        //celestialsAnchor = new GUIAncor(inputState, 500, 800);
        celestialScroll = new GUIScrollablePanel(500,800, inputState);
        celestialsText = new GUITextOverlay(500,800, FontLibrary.getBlenderProBook16(), inputState);
        celestialsText.wrapSimple = true;
        celestialScroll.setContent(celestialsText);
        celestialsBG.attach(celestialScroll);
        //celestialsAnchor.attach(celestialsScroll);
        //celestialsBG.attach(celestialsAnchor);

        resourcesBG = new GUIColoredRectangle(getState(), 540, 30, new Vector4f(0, 0.26f, 0.3f, 0.5f));
        //resourcesAnchor = new GUIAncor(inputState, 500, 800);
        resourcesText = new GUITextOverlay(300,300, FontLibrary.getBlenderProBook16(), inputState);
        resourcesText.wrapSimple = true;
        //resourcesAnchor.attach(resourcesText);
        resourcesBG.attach(resourcesText);
    }

    @Override
    public void recreateTabs() {
        guiWindow.clearTabs();
        overviewTab = guiWindow.addTab("OVERVIEW");
        //TODO: explore structure while paused, look for discrepancies that may cause this text offset
        overviewTab.setTextBoxHeightLast((int) (GLFrame.getHeight() / 1.5));
        //overviewTab.attach(overviewAnchor);
        overviewTab.attach(overviewBG);

        overviewTab.getTextboxes().get(0).setContent(overviewBG);
        overviewBG.attach(overviewText);
        overviewText.autoWrapOn = overviewTab.getTextboxes().get(0);

        celestialsTab = guiWindow.addTab("CELESTIAL BODIES");
        celestialsTab.setTextBoxHeightLast((int) (GLFrame.getHeight() / 1.5));
        celestialsTab.attach(celestialsBG);
        celestialsTab.getTextboxes().get(0).setContent(celestialsBG);
        celestialScroll.setHeight(this.getHeight() - (celestialsTab.getTabNameText().getHeight() * 10));
        celestialScroll.setWidth(celestialsTab.getTextboxes().get(0).getWidth());
        celestialScroll.setClipHeight(); //???
        celestialsText.autoWrapOn = celestialScroll;
        celestialsText.autoHeight = true;
        //TODO: fancy map?

        resourcesTab = guiWindow.addTab("SECTOR RESOURCES");
        resourcesTab.setTextBoxHeightLast((int) (GLFrame.getHeight() / 1.5));
        resourcesTab.attach(resourcesBG);
        resourcesTab.getTextboxes().get(0).setContent(resourcesBG);
        resourcesText.autoWrapOn = resourcesTab.getTextboxes().get(0);

        Vector3f hardPos = new Vector3f(overviewTab.getTabNameText().getPos());
        hardPos.x += overviewTab.getWidth()/2;;
        overviewTab.getTabNameText().setPos(hardPos);
        isInit = true;
    }

    public void updateText(){ //requires initialization and new source loaded
        try {
            List<Object> overview = new ArrayList<Object>();
            overview.addAll(Arrays.asList(source.scanOverview.split("\r\n")));
            overviewText.setText(overview);

            List<Object> celestials = new ArrayList<Object>();
            if (source.hasRemoteCelestialInfo) celestials.addAll(source.celestialEntries);
            else celestials.add(Lng.str("NO INFO: Celestial Information Chamber Not Installed")); //TODO: hide tab
            celestialsText.setText(celestials);

            List<Object> resources = new ArrayList<>();
            if (source.hasSecResourceInfo) resources.addAll(Arrays.asList(source.sectorResourceList.split("\r\n")));
            else resources.add(Lng.str("NO INFO: Resource Information Chamber Not Installed")); //TODO: hide tab
            resourcesText.setText(resources);
        } catch (Exception e) {
            System.err.println("[MOD][Resources ReSourced] Could not fill data!");
            e.printStackTrace();
        }
    }

    @Override
    public void draw() {
        if(celestialsTab != null){
            celestialScroll.setHeight(this.getHeight() - (celestialsTab.getTabNameText().getHeight() * 10));
            celestialScroll.setWidth(celestialsTab.getTextboxes().get(0).getWidth());
        }
        super.draw();
    }
}
