package org.ithirahad.resourcesresourced.gui;

import api.utils.gui.GUIControlManager;
import api.utils.gui.GUIMenuPanel;
import org.ithirahad.resourcesresourced.network.SystemScanInfo;
import org.schema.game.client.data.GameClientState;

public class AstrometricScanControlManager extends GUIControlManager {
    public static SystemScanInfo lastInfo;
    public AstrometricScanResultsPanel currentPanel = null;

    public AstrometricScanControlManager(GameClientState gameClientState) {
        super(gameClientState);
    }

    public void setInfo(SystemScanInfo info){
        lastInfo = info;
        try {
            if(currentPanel != null && currentPanel.isInit){
                currentPanel.source = info;
                currentPanel.updateText();
            }
        }catch (Exception e){
            System.err.println("[MOD][RRS][ERROR] Could not update scanner panel.");
            e.printStackTrace();
        }
    }

    @Override
    public GUIMenuPanel createMenuPanel() {
        currentPanel = new AstrometricScanResultsPanel(getState(), lastInfo);
        currentPanel.updateText();
        return currentPanel;
    }

    @Override
    public GUIMenuPanel getMenuPanel() {
        createMenuPanel();
        return super.getMenuPanel();
    }
}
