package org.ithirahad.resourcesresourced.gui;


import api.listener.Listener;
import api.listener.events.input.MousePressEvent;
import api.mod.StarLoader;
import api.mod.StarMod;
import com.bulletphysics.linearmath.Transform;
import libpackage.drawer.MapDrawer;
import libpackage.drawer.SpriteLoader;
import libpackage.markers.ClickableMapMarker;
import libpackage.markers.SimpleMapMarker;
import org.apache.commons.lang3.text.WordUtils;
import org.ithirahad.resourcesresourced.RRUtils.SerializableVector3i;
import org.ithirahad.resourcesresourced.RRUtils.SpaceGridMap;
import org.ithirahad.resourcesresourced.ResourcesReSourced;
import org.ithirahad.resourcesresourced.universe.gasplanet.GasGiantSheet;
import org.ithirahad.resourcesresourced.universe.starsystem.SystemSheet;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.view.effects.ConstantIndication;
import org.schema.game.client.view.gamemap.GameMapDrawer;
import org.schema.game.client.view.gui.shiphud.HudIndicatorOverlay;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.forms.Sprite;

import javax.vecmath.Vector4f;
import java.util.ArrayList;

/**
 * STARMADE MOD
 * CREATOR: Max1M
 * DATE: 21.10.2021
 * TIME: 16:49
 */
public class GasPlanetMapDrawer extends MapDrawer {
    ArrayList<Vector3i> exploredSystems = new ArrayList<>();
    private Vector3i lastMapPos;
    private Sprite gasGiantSprite;
    private ClickableMapMarker lastHover;
    private boolean hoveringOnGasGiant;
    private static GameMapDrawer currentDrawer;
    private StarMod mod;

    static Vector4f white = new Vector4f(1f,1f,1f,1f);

    public GasPlanetMapDrawer(StarMod mod) {
        super(mod);
        this.mod = mod;

        /*
        StarLoader.registerListener(MousePressEvent.class, new Listener<MousePressEvent>() {
            @Override
            public void onEvent(MousePressEvent event) {
                if (event.getRawEvent().pressedLeftMouse()) {
                    if (hoveringOnGasGiant && currentDrawer != null) SET CURRENT SELECTED SECTOR TO POSITION; //TODO: How to do this?
                }
            }
        },mod);
         */
    }

    public void loadSprites() {
        String folder = "org/ithirahad/resourcesresourced/assets/image/map/";

        //load sprite
        SpriteLoader msl = new SpriteLoader(folder,"gasgiant-2x1.png",50,50,2,1);
        msl.loadSprite(mod);
        gasGiantSprite = msl.getSprite();
    }

    private void collectGasGiants(GameMapDrawer drawer) {
        if (gasGiantSprite == null) {
            new NullPointerException("sprites for gasgiant drawer dont exist.").printStackTrace();
            return;
        }

        Vector3i currentSystem = drawer.getGameMapPosition().getCurrentSysPos();
        if (currentSystem.equals(lastMapPos))
            return;
        clearMarkers(); //TODO super ineffienct, rebuilding 3 times per frame.
        rebuildInternalList();
        /*
        for (int x = -1; x < 2; x++) {
            for (int y = -1; y < 2; y++) {
                for (int z = -1; z < 2; z++) {

         */
        Vector3i tempSys = new Vector3i(currentSystem);
                    //tempSys.add(x,y,z);
        /*
                    if (!isVisibleSystem(tempSys))
                        continue;

         */
        if (!isVisibleSystem(tempSys)) return;
        SystemSheet sheet = ResourcesReSourced.getSystemSheet(tempSys);
        if (sheet == null)
            return;

        final SpaceGridMap<GasGiantSheet> giants = sheet.getGiants(new SerializableVector3i(tempSys));
        for (final Vector3i giant : giants.keySet()) {
            Vector4f planetColor = new Vector4f(giants.get(giant).brightColor);
            planetColor.interpolate(giants.get(giant).darkColor, 0.5f);

            addMarker( //ball
                    new ClickableMapMarker(
                            gasGiantSprite,
                            0,
                            planetColor,
                            posFromSector(giant,true))
                    {
                        String text = WordUtils.capitalize("Gas Giant Planet (Type: " + WordUtils.capitalize(giants.get(giant).type.typeName()) + ")");
                        Transform labelPosition = new Transform();
                        ConstantIndication label = new ConstantIndication(labelPosition, "");
                        //boolean textNotDrawnYet = true;
                        boolean drawLabel = false;
                        {
                            label.setColor(new Vector4f(1,1,1,1));
                            labelPosition.setIdentity();
                            labelPosition.origin.set(posFromSector(giant,true));
                        }

                        public void drawText(String text) {
                            label.setText(Lng.str(text));
                            //label = new ConstantIndication(labelPosition, Lng.str(labelText));
                            //if(textNotDrawnYet) {
                            HudIndicatorOverlay.toDrawMapTexts.add(label);
                            //    textNotDrawnYet = false;
                            //}
                        }

                        @Override
                        public void preDraw(GameMapDrawer drawer) {
                            super.preDraw(drawer);
                            drawText(text);
                        }

                        @Override
                        public void onSelect(float v) {
                            label.setText(Lng.str(text));
                            lastHover = this;
                            hoveringOnGasGiant = true;
                        }

                        @Override
                        public void onUnSelect() {
                            label.setText(Lng.str(""));
                            hoveringOnGasGiant = false;
                        }
                    }
            );

            addMarker( //ring
                    new SimpleMapMarker(
                           gasGiantSprite,
                            1,
                            white,
                            posFromSector(giant,true))
            );
/*
                    }
                }
            }
 */
        }
    }

    @Override
    public void galaxy_DrawSprites(GameMapDrawer gameMapDrawer) {
        collectGasGiants(gameMapDrawer);
        rebuildInternalList();
        super.galaxy_DrawSprites(gameMapDrawer);
        currentDrawer = gameMapDrawer;
    }

    @Override
    public void system_PostDraw(GameMapDrawer gameMapDrawer, Vector3i systemPos, boolean explored) {
        super.system_PostDraw(gameMapDrawer, systemPos, explored);
    }

    private boolean isVisibleSystem(Vector3i system) {
        return !GameClientState.instance.getGameState().isFow() || GameClientState.instance.getController().getClientChannel().getGalaxyManagerClient().isSystemVisiblyClient(system);
    }
}
